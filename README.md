CEENBoT API 324 v2.21
======================

General
-------
CEENBoT API's are compiled from this source code and the resulting static library are then incorporated into user programs by calls to the API. To add features and enable additional functions of the robot, this source must be modified and compiled appropriately.

License
--
Copyright (C) 2014 The Board of Regents of the University of Nebraska. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

- Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Conventions
------------
Coding conventions are based on observations of the code before the GIT repository existed. This is primarily Jose Santos' code. Observations are as follows:

*   All #DEFINEs must be capitalizing all letters (I.E: #DEFINE THIS_IS_MY_DEFINE 0)
*   All spaces that would typically be in a phrase are replaced with underscores. (I.E: this_is_my_function())
*   All public functions should capitalize the system name followed by lower case function name (I.E: LCD_open())
*   All private functions should have two underscores before the system name. (IE: __LCD_close())
*   Header files are split into two files. __SYSTEM324v221.h for private functions, SYSTEM324v221.h for public functions.

First time
---------
The first time you check this out and need to compile, you will need to run the respective "build-dir-struct" script. The windows version is marked "win-build-dir-struct.bat" and the Mac/Linux version is a bash script called "build-dir-struct". It is recomended to run this from the respective command prompt (Terminal on Mac/Linux, Command Prompt on Windows). You can then run the "build-static-lib" script to compile the API with any changes. Output is set within the lib folder, which the contents can be distributed to test your changes (libcapi324v221.a and lib-includes directory are both needed).

Credits
--------
Original code written by Jose Santos, inspired by the factory CEENBoT firmware. Thanks goes to him and the original CEENBoT developers and creators.