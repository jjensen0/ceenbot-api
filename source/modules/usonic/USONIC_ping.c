/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'USONIC_ping()' function.

#include "usonic324v221.h"
#include "__usonic324v221.h"

SWTIME USONIC_ping( void )
{

    SWTIME rval = 0;

    if ( SYS_get_state( SUBSYS_USONIC ) == SUBSYS_OPEN )
    {

        // Reset the stopwatch to begin a new measurement.
        STOPWATCH_reset();

        // Start the pulse.
        SBV( __USONIC_PIN, PORTA );

        // Wait 5us or so...
        DELAY_us( 5 );

        // Clear the pluse.
        CBV( __USONIC_PIN, PORTA );

        // Switch the output 'trigger' pin to an INPUT so we can listen
        // for the 'echo' pulse.
        SBD( A, __USONIC_PIN, INPIN );

        // Now we 'busy-wait' for the 'echo pulse' to arrive.
        // We're looking for the 0->1 transition..., so we'll busy wait while
        // it remains LOW.
        while( ! ( GBV( __USONIC_PIN, PINA ) ) );

        DELAY_us( 10 );

        if ( GBV( __USONIC_PIN, PINA ) )
        {

            // As soon as we detect the 0->1 transition, we start the 
            // stop-watch.
            STOPWATCH_start();

            // Next, we 'busy-wait' again for the 1->0 transition, telling us
            // the echo has either arrived or never returned (maximum 'wait' 
            // time on the 'echo'.).  We busy wait while the pulse is HIGH.
            while( GBV( __USONIC_PIN, PINA ) );

        } // end if()

        // Stop the stopwatch.
        STOPWATCH_stop();

        // Switch the input trigger pin back to an OUTPUT for the next
        // 'ping' occurrence.
        SBD( A, __USONIC_PIN, OUTPIN );

        // ...and make sure it stays low.
        CBV( __USONIC_PIN, PORTA );

        // Return the stopwatch's time.
        rval = STOPWATCH_get_ticks();

    } // end if()

    return rval;

} // end USONIC_ping_and_wait()
