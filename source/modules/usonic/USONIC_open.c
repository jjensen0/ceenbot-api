/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'USONIC_open()' function.

#include "usonic324v221.h"
#include "__usonic324v221.h"

SUBSYS_OPENSTAT USONIC_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Set default values to assume there is no error.
    rval.subsys = SUBSYS_USONIC;
    rval.state  = SUBSYS_OPEN;

    // We need to make sure the stopwatch service is open.
    if ( SYS_get_state( SUBSYS_SWATCH ) == SUBSYS_CLOSED )
    {

        // Set 'rval' to indicate the needed subsystem is closed.
        rval.subsys = SUBSYS_SWATCH;
        rval.state  = SUBSYS_CLOSED;

    } // end if()

    // Only 'OPEN' if the module is closed.
    else if ( SYS_get_state( SUBSYS_USONIC ) == SUBSYS_CLOSED )
    {

        ATTINY_power_cycle_IR( PWRCYCLE_OFF );

        // Wait a bit to let the power cycling stabilize.
        DELAY_ms( 100 );

        // Do MCU-specific initialization.
        __USONIC_init();

        // Tell the system 'USONIC' is open for business.
        SYS_set_state( SUBSYS_USONIC, SUBSYS_OPEN );

    } // end else-if()

    return rval;

} // end USONIC_open()
