/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'SPKR_note()' function.

#include "spkr324v221.h"
#include "__spkr324v221.h"

void SPKR_note( SPKR_NOTE note, SPKR_OCTV octave, signed short int transp )
{

    unsigned short int freq_entry;  // Stores the 'entry' to be accessed in
                                    // the 'Freq LUT'.

    SPKR_FREQ   note_freq = 0;                                    

    // Compute the frequency entry to be accessed.
    freq_entry = 12 * octave + note + transp;

    // Do bounds check on the transposition.
    if ( transp > MAX_TRANSPOSE_VALUE )

        transp = MAX_TRANSPOSE_VALUE;

    else if ( transp < MIN_TRANSPOSE_VALUE )

        transp = MIN_TRANSPOSE_VALUE;

    // Do bounds check for the octave.
    if ( octave > MAX_OCTAVE_VALUE )

        octave = MAX_OCTAVE_VALUE;


    
    // Decide on what to do.. play a note or discontinue a note.

    if ( note != SPKR_NOTE_NONE )
    {

        // Determine if we're within bounds.
        if ( ( freq_entry >= 0 ) && ( freq_entry <= SEMITONES_FROM_ROOT_C0 ) )

            // Get the frequency value corresonding to the note being
            // requested for play.
            note_freq = p_Freq_LUT[ freq_entry ];

    } // end if()

    // Play the note requested (or shut it off if out of bounds, or simply
    // requested by the user.  Note that 'note_freq = 0' by default, so 
    // this variable will only be overriden if there's a valid note to play and
    // the user did not explicitly invoke 'SPKR_NOTE_NONE', etc.

    SPKR_tone( note_freq );

    // That's it.        

} // end SPKR_note()
