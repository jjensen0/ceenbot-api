/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'SPKR_play_note()' function.

#include "spkr324v221.h"
#include "__spkr324v221.h"

void SPKR_play_note( SPKR_NOTE note, SPKR_OCTV octave,
                     signed short int transp,
                     SPKR_TIME duration_ms, unsigned short int len )
{

    unsigned long int long_time = 0;

    if ( len > 0 )
    {

        // First make sure that 'len' parameter <= 100.
        if ( len > 100 )

            len = 100;

        SPKR_TIME note_ON_time;
        SPKR_TIME note_OFF_time;

        // Scale the 'duration' so that we can perform computations using 
        // integer arithmetic.
        long_time = ( unsigned long int ) duration_ms;
        long_time *= ( unsigned long int ) len;
        long_time /= 100;

        note_ON_time = ( SPKR_TIME ) long_time;

        note_OFF_time = duration_ms - note_ON_time;

        // Play the specified note at the given octave/transposition.
        SPKR_note( note, octave, transp );

        // Wait for 'note_ON_time' duration.
        TMRSRVC_delay( note_ON_time );

        // Turn it off.
        SPKR_stop_tone();

        // Wait before exiting.
        TMRSRVC_delay( note_OFF_time );

    } // end if()

    // If we have some non-zero duration (with zero note length), then
    // we should just respect the 'moment of silence'.
    else if ( duration_ms != 0 )

        TMRSRVC_delay( duration_ms );

} // end SPKR_play_note()
