/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private file contains global variables.

#include "spkr324v221.h"
#include "__spkr324v221.h"

// ========================== private globals =============================== //
volatile BOOL tone_mode_active = FALSE;      // 'TRUE' if 'TONE' mode active.
volatile BOOL beep_mode_active = FALSE;      // 'TRUE' if 'BEEP' mode active.

volatile signed long int spkr_accum32 = 0;   // 32-bit accumulator used for DDS
                                             // tone generation synthesis.

volatile signed long int spkr_freq = 0; // Contains the frequency at which the 
                                        // tone will be generated * 10.

volatile signed short int beep_accum16 = 0; // 16-bit accumulator used for DDS
                                            // beep generation synthesis.
volatile signed short int beep_freq = 0;    // Contains the frequency at which
                                            // the beep will be generated in
                                            // Hz.

// Contains the chromatic scale for 12-tones relative to Octave 3.
// All other octaves can be obtained from this one with multiplying by 
// the appropriate factor.
const SPKR_FREQ note_LUT[ 12 ] = {

    SPKR_TONE_C3,
    SPKR_TONE_C3_S,
    SPKR_TONE_D3,
    SPKR_TONE_D3_S,
    SPKR_TONE_E3,
    SPKR_TONE_F3,
    SPKR_TONE_F3_S,
    SPKR_TONE_G3,
    SPKR_TONE_G3_S,
    SPKR_TONE_A3,
    SPKR_TONE_A3_S,
    SPKR_TONE_B3

};    
   

// The following look-up table is used to convert from a chromatic table
// to diatonic.  This look up table is used to look into the 'note_LUT' above.
// So it acts as a look-up-table of a look-up-table.
const unsigned char diatonic_LUT[ 8 ] = {

    0,  //  0 = Note C
    2,  //  1 = Note D
    4,  //  2 = Note E
    5,  //  3 = Note F
    7,  //  4 = Note G
    9,  //  5 = Note A
    11  //  6 = Note B

};

// The following look-up-table contains the frequency table, dynamically
// generated and stored into an array.  This allows for easy transposition
// of not only 'octaves' but also in 'semitones' for greater flexibility.
// It is dynamically allocated when 'SPKR_open()' is invoked, and deallocated
// via 'SPKR_close()', etc.
//
// The table contains frequecies starting from note C0 = 16.35 Hz up to
// B5 = ~987.77 Hz with a relative error (at LEAST) of %0.06.
SPKR_FREQ *p_Freq_LUT = NULL;

