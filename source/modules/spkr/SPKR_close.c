/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'SPKR_close()' function.

#include "spkr324v221.h"
#include "__spkr324v221.h"

// =========================== private prototypes =========================== //
inline void __SPKR_close_tone_mode( void );
inline void __SPKR_close_beep_mode( void );

// ============================= functions ================================== //
void SPKR_close( SPKR_MODE spkr_mode )
{

    switch( spkr_mode )
    {

        case SPKR_TONE_MODE:

            __SPKR_close_tone_mode();

        break;

        case SPKR_BEEP_MODE:

            __SPKR_close_beep_mode();

        break;

        default: /* Do Nothing. */ ;

    } // end switch()

} // end SPKR_close()
// -------------------------------------------------------------------------- //
void __SPKR_close_tone_mode( void )
{

    // Only close IF the service is currently OPEN.
    if ( SYS_get_state( SUBSYS_SPKR ) == SUBSYS_OPEN )
    {

        if ( tone_mode_active )

            // Stop the timer (just in case).
            __SPKR_stop();

        // Do any MCU-specific release.
        __SPKR_close( SPKR_TONE_MODE );

        // Release any previously allocated memory.
        if ( p_Freq_LUT != NULL )
        {

            // Release.
            free( p_Freq_LUT );

            // Safety first.
            p_Freq_LUT = NULL;

        } // end if()

        // Note 'TONE' mode is no longer active.
        tone_mode_active = FALSE;

        // Update system variable to indicate so.
        SYS_set_state( SUBSYS_SPKR, SUBSYS_CLOSED );

    } // end if()

} // end __SPKR_close_tone_mode()
// -------------------------------------------------------------------------- //
void __SPKR_close_beep_mode( void )
{

    // Only close IF the service is currently open.
    if ( SYS_get_state( SUBSYS_BEEP ) == SUBSYS_OPEN )
    {

        // Do any MCU-specific release.
        __SPKR_close( SPKR_BEEP_MODE );

        // Note 'BEEP' mode is no longer active.
        beep_mode_active = FALSE;

        // Note we're closed for business.
        SYS_set_state( SUBSYS_BEEP, SUBSYS_CLOSED );

    } // end if()

} // end __SPKR_close_beep_mode()
