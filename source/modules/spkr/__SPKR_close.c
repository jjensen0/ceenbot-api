/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for '__SPRK_close()' function.

#include "spkr324v221.h"
#include "__spkr324v221.h"

void __SPKR_close( SPKR_MODE spkr_mode )
{

    if ( spkr_mode == SPKR_TONE_MODE )
    {

        // Set the clock divider to 0.
        CBV( CS10, TCCR1B );

    } // end if()

    // Do this ONLY if only one subsystem is active, but NOT both because
    // the other may still need it!

    if ( ( tone_mode_active == TRUE ) && ( beep_mode_active == TRUE ) )
    {

        // Do nothing.
        __asm__ __volatile__ ( "nop" );  // Just in case the optimizer gets
                                        // 'smart' on us.

    } // end if()

    // Otherwise...
    else if ( ( tone_mode_active == TRUE ) || ( beep_mode_active == TRUE ) )
    {

        // Set the speaker pin to LOW.
        CBV( __SPKR_OUT_PIN, __SPKR_OUT_PORT );

        // Set it to input.
        SBD( D, __SPKR_OUT_PIN, INPIN );

    } // end if()

} // end __SPKR_close()
