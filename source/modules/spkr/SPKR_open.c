/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'SPKR_open()' function.

#include "spkr324v221.h"
#include "__spkr324v221.h"

// ========================== private prototypes ============================ //
// Desc: Function takes care of setting up the frequency LUT.
inline BOOL __SPKR_set_up_freq_LUT( void );
inline SUBSYS_OPENSTAT __SPKR_open_tone_mode( void );
inline SUBSYS_OPENSTAT __SPKR_open_beep_mode( void );

// ============================= functions ================================== //
SUBSYS_OPENSTAT SPKR_open( SPKR_MODE spkr_mode )
{

    SUBSYS_OPENSTAT rval;

    rval.subsys = SUBSYS_SPKR;
    rval.state  = SUBSYS_CLOSED;

    // Open depending on the 'speaker mode'.
    switch( spkr_mode )
    {

        case SPKR_BEEP_MODE:

            rval = __SPKR_open_beep_mode(); 

        break;

        case SPKR_TONE_MODE:

            rval = __SPKR_open_tone_mode();

        break;

        default: /* Do nothing. */;

    } // end switch()

    return rval;

} // end SPKR_open()
// -------------------------------------------------------------------------- //
SUBSYS_OPENSTAT __SPKR_open_tone_mode( void )
{

    SUBSYS_OPENSTAT rval;

    // Set up default values to assume there's no error.
    rval.subsys = SUBSYS_SPKR;
    rval.state  = SUBSYS_OPEN;

    // First check if the STOPWATCH is already open, because the 
    // STOPWATCH also uses the 16-bit timer!  If it is OPEN, we can't use this.
    // However, if the 'SPKR_MODE' is 'BEEP MODE', then we don't care.
    if( SYS_get_state( SUBSYS_SWATCH ) == SUBSYS_CLOSED )
    {

        // If the stopwatch is closed (i.e., not being used by something else, 
        // then open the SPKR module ONLY if presently closed.
        if ( SYS_get_state( SUBSYS_SPKR ) == SUBSYS_CLOSED )
        {

            // Set up the frequency LUT table.
            if ( __SPKR_set_up_freq_LUT() == TRUE )
            {

                // Reset the DDS accumulator.
                spkr_accum32 = 0;

                // Attach the ISR to timer1.
                ISR_attach( ISR_TIMER1_COMPA_VECT, __SPKR_TIMER1_COMPA_vect );

                // Initialize MCU-specific hardware based on the speaker mode.
                __SPKR_init( SPKR_TONE_MODE );

                // Denote the 'beep' mechanism is now active.
                tone_mode_active = TRUE;

                // NOTE:  We don't enable timer interrupts until the user 
                //        requests tones to be played by invoking the appro-
                //        priate function.  There's no reason to have the timer
                //        running for no reason while nothing is happening.

                // Let the system know it is open.
                SYS_set_state( SUBSYS_SPKR, SUBSYS_OPEN );

            } // end if()

        } // end if()

    } // endif()

    else
    {

        rval.state = SUBSYS_CLOSED;

    } // end else.

    return rval;

} // end __SPKR_open_tone_mode()
// -------------------------------------------------------------------------- //
SUBSYS_OPENSTAT __SPKR_open_beep_mode( void )
{

    SUBSYS_OPENSTAT rval;

    // Set up default values to assume there's no error.
    rval.subsys = SUBSYS_BEEP;
    rval.state  = SUBSYS_OPEN;

    // Check if the BEEP subsystem isn't already open...
    if ( SYS_get_state( SUBSYS_BEEP ) == SUBSYS_CLOSED )
    {

        // Just reset the DDS accumulator for the 'beep' subsystem.
        beep_accum16 = 0;

        // Do MCU-specific initialization.
        __SPKR_init( SPKR_BEEP_MODE );

        // Denote the 'BEEP' mechanism is now active.
        beep_mode_active = TRUE;

        // Note that the 'BEEP' subsystem is now open.
        SYS_set_state( SUBSYS_BEEP, SUBSYS_OPEN );

        // That's it.

    } // end if()

    return rval;

} // end __SPKR_open_beep_mode()
// -------------------------------------------------------------------------- //
BOOL __SPKR_set_up_freq_LUT( void )
{

    BOOL rval = FALSE;          // Assume this will fail.

    float note_freq = 0;
    float exponent  = 0;

    SPKR_FREQ temp = 0;

    unsigned short int i = 0;   // Used with 'for()' below.

    // See if we can allocate memory for the frequency look up
    // table... we can't proceed unless we get this step out
    // of the way.
    p_Freq_LUT = 
        ( SPKR_FREQ * )malloc( 

                ( sizeof( SPKR_FREQ ) * SEMITONES_FROM_ROOT_C0 )

                );

    if ( p_Freq_LUT != NULL )
    {

        // Populate the table with frequency data.
        for( i = 0; i < SEMITONES_FROM_ROOT_C0; ++i )
        {

            // Compute the exponent.
            exponent = ( float ) i;

            exponent /= ( float )( 20 );

            // Multiply times the semitone constant.
            exponent *= SEMITONE_CONSTANT;

            // Compute preliminary note frequency.
            note_freq = pow( 10.0, ( double ) exponent );

            // Multipley times the root note.
            note_freq *= ROOT_NOTE_FREQ;

            // Down convert.
            temp = ( SPKR_FREQ ) lrint( note_freq );

            // Store it in the frequency LUT after we multiply by
            // 10.
            p_Freq_LUT[ i ] = ( ( temp ) * 10L );

        } // end for()

        // Note that the procedure was successful.
        rval = TRUE;

    } // end if()

    return rval;

} // end __SPKR_set_up_freq_LUT()
