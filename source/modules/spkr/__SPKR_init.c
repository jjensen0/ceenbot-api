/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for '__SPKR_init()' function.

#include "spkr324v221.h"
#include "__spkr324v221.h"

void __SPKR_init( SPKR_MODE spkr_mode )
{

    // Initialize necessary pins if neither 'BEEP' or 'TONE' modes are active.
    if ( ( tone_mode_active == FALSE ) && ( beep_mode_active == FALSE ) )
    {

        // Set up the speaker pin as output.
        SBD( D, __SPKR_OUT_PIN, OUTPIN );

        // Set it to LOW.
        CBV( __SPKR_OUT_PIN, __SPKR_OUT_PORT );

    } // end if()

    // Next set up the timer -- we're going to use timer 1 -- if 
    // not in beep mode, and if not already initialized.
    if ( ( tone_mode_active == FALSE ) && ( spkr_mode == SPKR_TONE_MODE ) )
    {

        TCCR1A = 0x00;      // Reset previous mode.
        TCCR1B = 0x00;      // Reset previous mode.

        SBV( WGM12, TCCR1B ); // Enable 'CTC' mode.
        SBV( CS10,  TCCR1B ); // Set clock-divider to 'divide-by-1'.

        // Reset the counter regsiter.
        TCNT1 = 0x0000;

        // Set the compare-match value needed to generate ~20us interrupt rate
        // (or 50KHz rate).  Note that 0x0190 = 400 (decimal).
        OCR1AH = 0x01;
        OCR1AL = 0x90;

    } // end if()

    // That's it.

} // end __SPKR_init()
