/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: File contains private global variables for the ISR subsystem module.

#include "isr324v221.h"
#include "__isr324v221.h"

// ========================== private globals =============================== //
// Desc: Following contains the vector table of functions that will be called
//       upon an interrupt.  These are MCU-specific and have the following 
//       assignments for the '324 MCU.
//          
//          0 - RESET   ( Not Assignable )
//          1 - INT0:   External Interrupt Request 0
//          2 - INT1:   External Interrupt Request 1
//          3 - INT2:   External Interrupt Request 2
//          4 - PCINT0: Pin Change Interrupt Request 0
//          5 - PCINT1: Pin Change Interrupt Request 1
//          6 - PCINT2: Pin Change Interrupt Request 2
//          7 - PCINT3: Pin Change Interrupt Request 3
//          8 - WDT:    Watchdog Time-out Interrupt
//          9 - TIMER2_COMPA: Timer/Counter 2 Compare Match A
//         10 - TIMER2_COMPB: Timer/Counter 2 Compare Match B
//         11 - TIMER2_OVF:   Timer/Counter 2 Overflow
//         12 - TIMER1_CAPT:  Timer/Counter 1 Capture Event
//         13 - TIMER1_COMPA: Timer/Counter 1 Compare Match A
//         14 - TIMER1_COMPB: Timer/Counter 1 Compare Match B
//         15 - TIMER1_OVF:   Timer/Counter 1 Overflow
//         16 - TIMER0_COMPA: Timer/Counter 0 Compare Match A
//         17 - TIMER0_COMPB: Timer/Counter 0 Compare Match B
//         18 - TIMER0_OVF:   Timer/Counter 0 Overflow
//         19 - SPI_STC:      SPI Serial Transfer Complete
//         20 - USART0_RX:    USART0 Rx Complete
//         21 - USART0_UDRE:  USART0 Data Register Empty
//         22 - USART0_TX:    USART0 Tx Complete
//         23 - ANALOG_COMP:  Analog Comparator
//         24 - ADC:          ADC Conversion Complete
//         25 - EE_READY:     EEPROM Ready
//         26 - TWI:          2-Wire Serial Interface
//         27 - SPM_READY:    Store Program Memory Ready
//         28 - USART1_RX:    USART1 Rx Complete
//         29 - USART1_UDRE:  USART1 Data Register Empty
//         30 - USART1_TX:    USART1 Tx Complete
//
CBOT_ISR_FUNC_PTR CBOT_ISR_vtable[ 32 ] = { 0 };
