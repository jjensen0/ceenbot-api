/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'GPI_open()' function.

#include "gpi324v221.h"

void GPI_open( GPI_MODULES *pModules )
{

    SUBSYS_OPENSTAT opstat; // Mainly for monitoring while debugging.

    if ( pModules != NULL )
    {

        if ( pModules->GPI_psxc   == TRUE ) opstat = PSXC_open();
        if ( pModules->GPI_step   == TRUE ) opstat = STEPPER_open();
        if ( pModules->GPI_lcd    == TRUE ) opstat = LCD_open();
        if ( pModules->GPI_led    == TRUE ) opstat = LED_open();

        // You cannot open this if the SPKR tone mode is active.
        if ( pModules->GPI_spkr != TRUE )
        {

            if ( pModules->GPI_swatch == TRUE ) 

                opstat = STOPWATCH_open();

        } // end if()
        if ( pModules->GPI_usonic == TRUE ) opstat = USONIC_open();

        // You cannot open this if the stopwatch is also open.
        if ( pModules->GPI_swatch != TRUE )
        {

            if ( pModules->GPI_spkr   == TRUE ) 

                opstat = SPKR_open( SPKR_TONE_MODE );

        } // end if()            

        if ( pModules->GPI_beep   == TRUE ) 

            opstat = SPKR_open( SPKR_BEEP_MODE );

    } // end if()

} // end GPI_open()
