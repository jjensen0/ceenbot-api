/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'GPI_ping()' function.

#include "gpi324v221.h"

#define __CM_PER_uSec   0.01724f
#define __FT_PER_uSec   0.00056562f
#define __IN_PER_uSec   0.0067874f

GPI_PINGDIST GPI_ping( GPI_PINGUNITS ping_units )
{

    unsigned long int time_us = 0;  // Holds time in us.

    SWTIME stopwatch_ticks    = 0;  // Holds stopwatch ticks in 10us/tick.

    GPI_PINGDIST rval = 0;          // Holds the ping distance once computed.

    // Ping regardless.
    stopwatch_ticks = USONIC_ping();

    // Compute time in microseconds -- each stopwatch 'tick' = 10us.
    time_us = (( unsigned long int ) stopwatch_ticks ) * 10;

    // What happens next depends on the currently selected 'ping_units'.
    switch( ping_units )
    {

        case GPI_PINGUNITS_CM:

            rval =  ( GPI_PINGDIST )((( double ) time_us ) * __CM_PER_uSec );

        break;

        case GPI_PINGUNITS_FT:

            rval =  ( GPI_PINGDIST )((( double ) time_us ) * __FT_PER_uSec );

        break;

        case GPI_PINGUNITS_IN:

            rval =  ( GPI_PINGDIST )((( double ) time_us ) * __IN_PER_uSec );

        break;

    } // end switch()

    return rval;

} // end GPI_ping()
