/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation for 'LCD_open()' function.

#include "lcd324v221.h"
#include "__lcd324v221.h"

SUBSYS_OPENSTAT LCD_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Set up default values.
    rval.state  = SUBSYS_OPEN;
    rval.subsys = SUBSYS_LCD;

    // Check the SPI is open.
    if ( SYS_get_state( SUBSYS_SPI ) == SUBSYS_CLOSED )
    {

        // Set 'rval' to appropriate error codes.
        rval.state  = SUBSYS_CLOSED;
        rval.subsys = SUBSYS_SPI;

    } // end if()

    // Check if the ATTINY is open ( for backlight level control ).
    else if ( SYS_get_state( SUBSYS_CPU1 ) == SUBSYS_CLOSED )
    {

        // Set 'rval' to appropriate error codes.
        rval.state  = SUBSYS_CLOSED;
        rval.subsys = SUBSYS_CPU1;

    } // end else-if()


    // Otherwise, if no errors, we can proceed to initialize the LCD.
    else
    {

        // DO any MCU-specific initialization first.
        __LCD_init();

        // Update the system variable, to let everyone know
        // the LCD is ready for use.
        SYS_set_state( SUBSYS_LCD, SUBSYS_OPEN );

        LCD_write_cmd( 0xAE );     // Display OFF (while we clear it).

        // Configure the LCD.
        LCD_write_cmd( 0xA2 );     // LCD bias set.
        LCD_write_cmd( 0xA0 );     // ADC select.

        DELAY_ms( 10 );

        LCD_write_cmd( 0xC0 );     // Common output mode select -->normal.
        LCD_write_cmd( 0x2F );     // Power control vol. reg. ckt ON.

        // Electronic Volume set ('Vo' voltage).
        LCD_write_cmd( 0x81 );     // Electronic MODE set -- vol control.
        LCD_write_cmd( 0x16 );     // Electronic vol register set.

        // Power control setting.
        LCD_write_cmd( 0x22 );     // Voltage regulator internal Resistor
                                   // ratio set.

        DELAY_ms( 10 );

        LCD_write_cmd( 0xAF );     // Turn display ON.

        LCD_write_cmd( 0xA6 );     // Sets LCD to normal display
                                   // ( Reverse = 0xA7 ).

        LCD_write_cmd( 0xA5 );     // Display all points ON.
        
        // Wait ~one second -- (pixel inspection time).
        for ( unsigned int i = 0; i < 1000; ++i )
        {

            DELAY_ms( 1 );

        } // end for()

        LCD_write_cmd( 0xB3 );     // Set the page to start displaying at
                                   // the top one.

        // Set the display RAM to start from the beginning.
        LCD_write_cmd( LCDCMD_DRAM_START_ADDR );

        // Set the page to start displaying at the top one, and set the 
        // column addresses at the beginning.
        LCD_set_PGC_addr( 3, 0 );

        // Update the internal variables to reflect this.
        LCD_set_next_PGC( 3, 0 );

        // Initialize the LCD parameter structure to a known state.
        LCD_params.lcd_change_notify = FALSE;
        LCD_params.p_change_notify_func = NULL;

        // Redirect 'stdout' to LCD display.
        stdout = &LCD_stdout;

        // Set the backlight.
        LCD_set_backlight( 24 );

        // Clear the LCD.
        LCD_clear();

        // Display all points normally.
        LCD_write_cmd( 0xA4 );

    } // end else.

    return rval;

} // end LCD_open()
