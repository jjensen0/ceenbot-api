// Auth: Jose Santos
// Desc: Implementation for '__LCD_init()' function.

#include "lcd324v221.h"
#include "__lcd324v221.h"

void __LCD_init( void )
{

    // The only pin needed by the LCD is PB3 to control 'A0' (command/data)
    // mode on the LCD.  The other pins are SPI-related and are handled
    // by the SPI subsystem.
    SBD( B, 3, OUTPIN );

} // end __LCD_init()
