/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'LCD_set_backlight()' function.

#include "lcd324v221.h"

void LCD_set_backlight( unsigned char BL_level )
{

    if ( SYS_get_state( SUBSYS_LCD ) == SUBSYS_OPEN )
    {

        // Check limits.
        if ( BL_level > 32 )

            BL_level = 32;                  // Let backlight FLASH.

        SPI_set_slave_addr( SPI_ADDR_NA );  // Set to non-assigned address
        // to segregate a unique message.

        // Send attnetion byte to ATtiny.
        SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_ATTN );

        // Send LCD backlight adjust command.
        SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_BL_ADJ );

        // Send backlight level -- this tells the ATtiny to adjust
        // the PWM rate to achieve appropriate light intensity.
        SPI_transmit( SPI_ADDR_ATTINY0, BL_level );

        DELAY_us( 35 );                 // Wait before deactivating slave.

        SPI_set_slave_addr( SPI_ADDR_NA );

    } // end if()

} // end LCD_set_backlight()
