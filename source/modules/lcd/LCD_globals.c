/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: File encapsulates all the 'global' data without publicly exposing it
//       through the header files.

#include "lcd324v221.h"
#include "__lcd324v221.h"

// ========================== private globals =============================== //
// Structure holds LCD-module internal parameters.
LCD_PARAMS LCD_params;

// Redirection used to support usage of the 'printf()' function.
FILE LCD_stdout = 
        FDEV_SETUP_STREAM( __LCD_putchar, NULL, _FDEV_SETUP_WRITE );

// -------------------------- font data ------------------------------------- //
//
// Table for 5x7 fixed-width font data.  Numerical values represent their 
// assigned hexadecimal ASCII assignment.  Basically, each five hex-digit line
// corresponds to a VERTICAL pixel column.  Consequently, and collectively,
// five of these make up the 'bit pattern' for a single character.  Therefore,
// 'char_bitmap' contains the 'bit-pattern' for generating the fonts used
// in a pixel-oriented display (as opposed to character-oriented).
const char char_bitmap [] PROGMEM = {
   
    0x16, 0x05, 0x01,       // Maximum number of characters per line 
                            // in this font.

    0x00, 0x00, 0x00, 0x00, 0x00,   // 0x20
    0x00, 0x00, 0xFA, 0x00, 0x00,   // 0x21 !
    0x00, 0xE0, 0x00, 0xE0, 0x00,   // 0x22 "
    0x28, 0xFE, 0x28, 0xFE, 0x28,   // 0x23 #
    0x24, 0x54, 0xFE, 0x54, 0x48,   // 0x24 $
    0xC4, 0xC8, 0x10, 0x26, 0x46,   // 0x25 %
    0x6C, 0x92, 0xAA, 0x44, 0xA0,   // 0x26 &
    0x00, 0xA0, 0xC0, 0x00, 0x00,   // 0x27 '
    0x00, 0x38, 0x44, 0x82, 0x00,   // 0x28 (
    0x00, 0x82, 0x44, 0x38, 0x00,   // 0x29 )
    0x28, 0x10, 0x7C, 0x10, 0x28,   // 0x2A *
    0x10, 0x10, 0x7C, 0x10, 0x10,   // 0x2B +
    0x00, 0x0A, 0x0C, 0x00, 0x00,   // 0x2C ,
    0x10, 0x10, 0x10, 0x10, 0x10,   // 0x2D -
    0x00, 0x06, 0x06, 0x00, 0x00,   // 0x2E .
    0x04, 0x08, 0x10, 0x20, 0x40,   // 0x2F /
    0x7C, 0x8A, 0x92, 0xA2, 0x7C,   // 0x30 0
    0x00, 0x42, 0xFE, 0x02, 0x00,   // 0x31 1
    0x42, 0x86, 0x8A, 0x92, 0x62,   // 0x32 2
    0x84, 0x82, 0xA2, 0xD2, 0x8C,   // 0x33 3
    0x18, 0x28, 0x48, 0xFE, 0x08,   // 0x34 4
    0xE4, 0xA2, 0xA2, 0xA2, 0x9C,   // 0x35 5
    0x3C, 0x52, 0x92, 0x92, 0x0C,   // 0x36 6
    0x80, 0x8E, 0x90, 0xA0, 0xC0,   // 0x37 7
    0x6C, 0x92, 0x92, 0x92, 0x6C,   // 0x38 8
    0x60, 0x92, 0x92, 0x94, 0x78,   // 0x39 9
    0x00, 0x6C, 0x6C, 0x00, 0x00,   // 0x3A :
    0x00, 0x6A, 0x6C, 0x00, 0x00,   // 0x3B ;
    0x10, 0x28, 0x44, 0x82, 0x00,   // 0x3C <
    0x28, 0x28, 0x28, 0x28, 0x28,   // 0x3D =
    0x00, 0x82, 0x44, 0x28, 0x10,   // 0x3E >
    0x40, 0x80, 0x8A, 0x90, 0x60,   // 0x3F ?
    0x4C, 0x92, 0x9E, 0x82, 0x7C,   // 0x40 @
    0x7E, 0x88, 0x88, 0x88, 0x7E,   // 0x41 A
    0xFE, 0x92, 0x92, 0x92, 0x6C,   // 0x42 B
    0x7C, 0x82, 0x82, 0x82, 0x44,   // 0x43 C
    0xFE, 0x82, 0x82, 0x44, 0x38,   // 0x44 D
    0xFE, 0x92, 0x92, 0x92, 0x82,   // 0x45 E
    0xFE, 0x90, 0x90, 0x90, 0x80,   // 0x46 F
    0x7C, 0x82, 0x92, 0x92, 0x5E,   // 0x47 G
    0xFE, 0x10, 0x10, 0x10, 0xFE,   // 0x48 H
    0x00, 0x82, 0xFE, 0x82, 0x00,   // 0x49 I
    0x04, 0x02, 0x82, 0xFC, 0x80,   // 0x4A J
    0xFE, 0x10, 0x28, 0x44, 0x82,   // 0x4B K
    0xFE, 0x02, 0x02, 0x02, 0x02,   // 0x4C L
    0xFE, 0x40, 0x30, 0x40, 0xFE,   // 0x4D M
    0xFE, 0x20, 0x10, 0x08, 0xFE,   // 0x4E N
    0x7C, 0x82, 0x82, 0x82, 0x7C,   // 0x4F O
    0xFE, 0x90, 0x90, 0x90, 0x60,   // 0x50 P
    0x7C, 0x82, 0x8A, 0x84, 0x7A,   // 0x51 Q
    0xFE, 0x90, 0x98, 0x94, 0x62,   // 0x52 R
    0x62, 0x92, 0x92, 0x92, 0x8C,   // 0x53 S
    0x80, 0x80, 0xFE, 0x80, 0x80,   // 0x54 T
    0xFC, 0x02, 0x02, 0x02, 0xFC,   // 0x55 U
    0xF8, 0x04, 0x02, 0x04, 0xF8,   // 0x56 V
    0xFC, 0x02, 0x1C, 0x02, 0xFC,   // 0x57 W
    0xC6, 0x28, 0x10, 0x28, 0xC6,   // 0x58 X
    0xE0, 0x10, 0x0E, 0x10, 0xE0,   // 0x59 Y
    0x86, 0x8A, 0x92, 0xA2, 0xC2,   // 0x5A Z
    0x00, 0xFE, 0x82, 0x82, 0x00,   // 0x5B [
    0x40, 0x20, 0x10, 0x08, 0x04,   // 0x5C backslash
    0x00, 0x82, 0x82, 0xFE, 0x00,   // 0x5D ]
    0x20, 0x40, 0x80, 0x40, 0x20,   // 0x5E ^
    0x02, 0x02, 0x02, 0x02, 0x02,   // 0x5F _
    0x00, 0x80, 0x40, 0x20, 0x00,   // 0x60 `
    0x04, 0x2A, 0x2A, 0x2A, 0x1E,   // 0x61 a
    0xFE, 0x12, 0x22, 0x22, 0x1C,   // 0x62 b
    0x1C, 0x22, 0x22, 0x22, 0x04,   // 0x63 c
    0x1C, 0x22, 0x22, 0x12, 0xFE,   // 0x64 d
    0x1C, 0x2A, 0x2A, 0x2A, 0x18,   // 0x65 e
    0x10, 0x7E, 0x90, 0x80, 0x40,   // 0x66 f
    0x30, 0x4A, 0x4A, 0x4A, 0x7C,   // 0x67 g
    0xFE, 0x10, 0x20, 0x20, 0x1E,   // 0x68 h
    0x00, 0x22, 0xBE, 0x02, 0x00,   // 0x69 i
    0x04, 0x02, 0x22, 0xBC, 0x00,   // 0x6A j
    0xFE, 0x08, 0x14, 0x22, 0x00,   // 0x6B k
    0x00, 0x82, 0xFE, 0x02, 0x00,   // 0x6C l
    0x3E, 0x20, 0x18, 0x20, 0x1E,   // 0x6D m
    0x3E, 0x10, 0x20, 0x20, 0x1E,   // 0x6E n
    0x1C, 0x22, 0x22, 0x22, 0x1C,   // 0x6F o
    0x3E, 0x28, 0x28, 0x28, 0x10,   // 0x70 p
    0x10, 0x28, 0x28, 0x18, 0x3E,   // 0x71 q
    0x3E, 0x10, 0x20, 0x20, 0x10,   // 0x72 r
    0x12, 0x2A, 0x2A, 0x2A, 0x04,   // 0x73 s
    0x20, 0xFC, 0x22, 0x02, 0x04,   // 0x74 t
    0x3C, 0x02, 0x02, 0x04, 0x3E,   // 0x75 u
    0x38, 0x04, 0x02, 0x04, 0x38,   // 0x76 v
    0x3C, 0x02, 0x0C, 0x02, 0x3C,   // 0x77 w
    0x22, 0x14, 0x08, 0x14, 0x22,   // 0x78 x
    0x30, 0x0A, 0x0A, 0x0A, 0x3C,   // 0x79 y
    0x22, 0x26, 0x2A, 0x32, 0x22,   // 0x7A z
    0x00, 0x10, 0x6C, 0x82, 0x00,   // 0x7B {
    0x00, 0x00, 0xFE, 0x00, 0x00,   // 0x7C |
    0x00, 0x82, 0x6C, 0x10, 0x00,   // 0x7D }
    0x08, 0x10, 0x10, 0x08, 0x10,   // 0x7E ~
    0x00, 0x00, 0x00, 0x00, 0x00,   // 0x7F space 
    0x7f, 0x81, 0x81, 0x81, 0x7f,   // 0x80 battery 0%
    0x7f, 0x83, 0x83, 0x83, 0x7f,   // 0x81 battery 17%
    0x7f, 0x87, 0x87, 0x87, 0x7f,   // 0x82 battery 33%
    0x7f, 0x8f, 0x8f, 0x8f, 0x7f,   // 0x83 battery 50%
    0x7f, 0x9f, 0x9f, 0x9f, 0x7f,   // 0x84 battery 67%
    0x7f, 0xbf, 0xbf, 0xbf, 0x7f,   // 0x85 battery 83%
    0x7f, 0xff, 0xff, 0xff, 0x7f,   // 0x86 battery 100%
    0x20, 0x40, 0xff, 0x40, 0x20,   // 0x87 up arrow (charging)
    0x62, 0x15, 0x17, 0x15, 0x65,   // 0x88 AC powered
    0x00, 0xfB, 0xfB, 0x00, 0x00    // 0x89 double !! (batt needs charging!!)

}; // end char_bitmap[].

// ------------------------- bitmap logo pixel data ------------------------- //
const char CEENBoT_Inc_logo_bitmap [] PROGMEM = {

   0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0x03, 0x00, 0xe0, 0xff, 0x07,
   0x00, 0xf0, 0xff, 0x0f, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0x78, 0x00, 0x1e,
   0x00, 0x78, 0x00, 0x1e, 0x00, 0x78, 0x00, 0x1e, 0x00, 0x78, 0x00, 0x1e,
   0x00, 0x78, 0x00, 0x1e, 0x00, 0x78, 0x00, 0x1e, 0x00, 0x78, 0x00, 0x1e,
   0x00, 0x78, 0x00, 0x1e, 0x00, 0x78, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xff, 0x03, 0x00, 0xe0, 0xff, 0x07,
   0x00, 0xf0, 0xff, 0x0f, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf8, 0x3c, 0x1f,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0xe0, 0xff, 0x03, 0x00, 0xf0, 0xff, 0x07, 0x00, 0xf8, 0xff, 0x0f,
   0x00, 0xf8, 0xff, 0x1f, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0xff, 0x03, 0x00, 0xf8, 0xff, 0x0f,
   0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0x00, 0x00, 0x1e,
   0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x80, 0x1f, 0x00, 0x00, 0xf0, 0x1f,
   0x00, 0x00, 0xff, 0x0f, 0x00, 0xe0, 0xff, 0x07, 0x00, 0xf0, 0xff, 0x00,
   0x00, 0xf8, 0x0f, 0x00, 0x00, 0xf8, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00,
   0x00, 0x78, 0x00, 0x00, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf8, 0xff, 0x1f,
   0x00, 0xf0, 0xff, 0x1f, 0x00, 0xe0, 0xff, 0x1f, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf8, 0xff, 0x1f,
   0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0x78, 0x00, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e, 0x00, 0x78, 0x3c, 0x1e,
   0x00, 0xf8, 0x7e, 0x1f, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf0, 0xff, 0x0f,
   0x00, 0xe0, 0xe7, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38,
   0x00, 0xee, 0xff, 0x6c, 0x00, 0x2a, 0x80, 0x45, 0x00, 0x3a, 0x00, 0x47,
   0xfc, 0x03, 0xff, 0x6c, 0xfc, 0x83, 0xff, 0x39, 0x00, 0xde, 0xff, 0x0b,
   0x7c, 0xd0, 0x81, 0x0b, 0xfc, 0xd0, 0x81, 0x0b, 0xc0, 0xd0, 0x81, 0x0b,
   0xfc, 0xd0, 0x81, 0x0b, 0x7c, 0xde, 0xff, 0x0b, 0x00, 0x82, 0xff, 0x39,
   0x70, 0x02, 0xff, 0x6c, 0xf8, 0x3a, 0x00, 0x47, 0xdc, 0x2a, 0x80, 0x45,
   0xcc, 0xee, 0xff, 0x6c, 0xcc, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x00,
   0x0c, 0x00, 0x00, 0x1e, 0x0c, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x1e,
   0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x1e, 0x00, 0xf8, 0xff, 0x1f,
   0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf8, 0xff, 0x1f, 0x00, 0xf8, 0xff, 0x1f,
   0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x1e,
   0x00, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x5e, 0x00, 0x00, 0x00, 0x40,
   0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40,
   0x00, 0x00, 0x00, 0x7c, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x10,
   0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x7c

}; // end CEENBoT_Inc_logo_bitmap[]
