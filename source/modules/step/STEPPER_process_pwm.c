/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_process_pwm()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_process_pwm( unsigned char *pPattern, 
                          unsigned char process_left,
                          unsigned char process_right )
{

    static unsigned char prev_state_left  = 0;
    static unsigned char prev_state_right = 0;
    static unsigned char processing_left_pwm  = 0;
    static unsigned char processing_right_pwm = 0;

    // *** Left Motor ***
    
    // First check if we're braking...
    if ( STEPPER_params.astate.left == STEPPER_BRAKING )
    {

        prev_state_left ^= 1;   // Toggle state.

        // Turn the coil off on every other 'tick'.
        if ( !prev_state_left )

            *pPattern &= 0b11100011;


    } // end if()

    // Otherwise, do PWM processing for left motor ONLY if the current speed 
    // is equal to or below 50 stps/sec ( not including 0 stps/sec ).
    else if ( STEPPER_params.curr_speed.left <= 50 )
    {

        // If 'process_left' is non-zero, then that means the steppers will
        // change phase.  We need to recompute the time-out count for the
        // corresponding motor.
        if ( process_left )
        {

            // Compute the [new] number of steps before 'time-out' occurs.
            STEPPER_params.pwm_timeout.left = 
                   (( __PWM_PROP_RATE * STEPPER_params.curr_speed.left ) >> 7 );

            // Indicate we're processing a new PWM period.
            processing_left_pwm = 1;

        } // end if()

        // Otherwise, we simply count down on the time-out counter.
        else if ( processing_left_pwm )
        {

            // Decrement time-out count.
            STEPPER_params.pwm_timeout.left--;

            // If terminal count...
            if ( STEPPER_params.pwm_timeout.left == 0 )
            {

                // Clear the bits to dis-engage the stepper coils.
                *pPattern &= 0b11100011;

                // Stop counting down, and wait for the next PWM period.
                processing_left_pwm = 0;

            } // end if()

        } // end else-if()

    } // end else-if()

    // *** Right Motor ***

    // First check if the current speed is zero AND we're braking.
    if ( STEPPER_params.astate.right == STEPPER_BRAKING ) 
    {

        prev_state_right ^= 1;  // Toggle state.

        // Turn the coil off on every other 'tick'.
        if ( !prev_state_right )

            *pPattern &= 0b00011111;

    } // end if()

    // Otherwise, do PWM processing for left motor ONLY if the current speed 
    // is equal to or below 50 stps/sec ( not including 0 stps/sec ).
    else if ( STEPPER_params.curr_speed.right <= 50 )
    {

        // If 'process_right' is non-zero, then that means the steppers will
        // change phase.  We need to recompute the time-out count for the
        // corresponding motor.
        if ( process_right )
        {

            // Compute the [new] number of steps before 'time-out' occurs.
            STEPPER_params.pwm_timeout.right = 
                  (( __PWM_PROP_RATE * STEPPER_params.curr_speed.right ) >> 7 );

            // Indicate we're processing a new PWM period.
            processing_right_pwm = 1;

        } // end if()

        // Otherwisee, we simply count down on the time-out counter.
        else if ( processing_right_pwm )
        {

            // Decrement time-out count.
            STEPPER_params.pwm_timeout.right--;

            // If terminal count...
            if ( STEPPER_params.pwm_timeout.right == 0 )
            {

                // Clear the bits to dis-engage the stepper coils.
                *pPattern &= 0b00011111;

                // Stop counting down, and wait for the next PWM period.
                processing_right_pwm = 0;

            } // end else-if()

        } // end else-if()

    } // end else-if()

} // end STEPPER_process_pwm()
