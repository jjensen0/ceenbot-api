/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_wait_and_then()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_wait_and_then( STEPPER_ID which, ... )
{

    // Function pointers.
    STEPPER_EVENT_PTR p_L_step_event = NULL;
    STEPPER_EVENT_PTR p_R_step_event = NULL;

    BOOL triggered_L = FALSE;
    BOOL triggered_R = FALSE;

    va_list ap;

    switch( which )
    {

        case LEFT_STEPPER:

            // First get stepper event function pointers.
            va_start( ap, which );

            p_L_step_event = va_arg( ap, STEPPER_EVENT_PTR );

            va_end( ap );

            // Then 'busy' wait until stepper motion is done.
            while( ! ( step_done.left ) );

            // When done, trigger the stepper event, but only if NOT NULL.
            if ( p_L_step_event != NULL )
                
                p_L_step_event();
            


        break;

        case RIGHT_STEPPER:


            // First get stepper event function pointers.
            va_start( ap, which );

            p_R_step_event = va_arg( ap, STEPPER_EVENT_PTR );

            va_end( ap );

            // Then 'busy' wait until stepper motion is done.
            while( ! ( step_done.right ) );

            // When done, trigger the stepper event, but only if NOT NULL.
            if ( p_R_step_event != NULL )

                p_R_step_event();


        break;

        case BOTH_STEPPERS:

            // First get stepper event function pointers.
            va_start( ap, which );

            p_L_step_event = va_arg( ap, STEPPER_EVENT_PTR );
            p_R_step_event = va_arg( ap, STEPPER_EVENT_PTR );

            va_end( ap );

            do {

                // See if left stepper event needs to be triggered.
                if ( ( ! triggered_L ) && ( step_done.left ) ) {

                    // Trigger the stepper event only if NOT NULL.
                    if ( p_L_step_event != NULL )

                        p_L_step_event();

                    // Prevent function above from being 'retriggered'.
                    triggered_L = TRUE;

                } // end if()

                // See if right stepper event needs to be triggered.
                if (  ( ! triggered_R ) && ( step_done.right ) ) {

                    // Trigger the stepper event only if NOT NULL.
                    if ( p_R_step_event != NULL )

                        p_R_step_event();

                    // Prevent function above from being 'retriggered'.
                    triggered_R = TRUE;

                } // end if()

            } while( ( ! triggered_L ) || ( ! triggered_R ) );

        break;

    } // end switch()

} // end STEPPER_wait_and_then()
