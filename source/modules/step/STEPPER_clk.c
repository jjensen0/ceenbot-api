/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_clk()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_clk( void )
{

    unsigned char accel_process_left  = 0;
    unsigned char accel_process_right = 0;

    unsigned char speed_process_left  = 0;
    unsigned char speed_process_right = 0;

    // DO NOT 'tick' unless the stepper subsystem is open (initialized).
    if ( SYS_get_state( SUBSYS_STEPPER ) == SUBSYS_OPEN )
    {

        // NOTE: The DDS accumulator (which triggers DDS events) is directly 
        //       tied to the step speed of the motor in question.

        // *************************************
        // ****** ACCEL/DECEL DDS PROCESS ******
        // *************************************

        // *** DDS left motor acceleration ***

        // If left motor acceleration is not zero, then...
        if ( STEPPER_params.step_accel.left != 0 )
        {

            // We need to process DDS acceleration/deceleration.
            STEPPER_params.dds_accel.left += STEPPER_params.step_accel.left;

            if ( STEPPER_params.dds_accel.left >= __DDS_DIVISOR )
            {

                // Adjust accumulator.
                STEPPER_params.dds_accel.left -= __DDS_DIVISOR;

                // Indicate we have to process.
                accel_process_left = 1;

            } // end if()

        } // end if()

        // Otherwise...
        else
        {

            // Simply set the immedate current speed to the set value.
            STEPPER_params.curr_speed.left = STEPPER_params.step_speed.left;

        } // end else.

        // *** DDS right motor acceleration ***

        // If right motor acceleration is not zero, then...
        if ( STEPPER_params.step_accel.right != 0 )
        {

            // We need to process acceleration/deceleration.
            STEPPER_params.dds_accel.right += STEPPER_params.step_accel.right;

            if ( STEPPER_params.dds_accel.right >= __DDS_DIVISOR )
            {

                // Adjust accumulator.
                STEPPER_params.dds_accel.right -= __DDS_DIVISOR;

                // Indicate we have to process.
                accel_process_right = 1;

            } // end if()
        } // end if()

        // Otherwise...
        else

            // Simply set the immediate current speed to the set value.
            STEPPER_params.curr_speed.right = STEPPER_params.step_speed.right;


        // Process acceleration if needed.
        STEPPER_process_accel( accel_process_left, accel_process_right );
        

        // ******************************
        // ****** SPEED DDS PROCESS *****
        // ******************************

        // *** DDS left motor speed ***

        STEPPER_params.dds_speed.left += STEPPER_params.curr_speed.left;

        if ( STEPPER_params.dds_speed.left >= __DDS_DIVISOR )
        {

            // Adjust accumulator.
            STEPPER_params.dds_speed.left -= __DDS_DIVISOR;

            // Indicate we have to process.
            speed_process_left = 1;

        } // end if()

        // *** DDS right motor speed ***

        STEPPER_params.dds_speed.right += STEPPER_params.curr_speed.right;

        if ( STEPPER_params.dds_speed.right >= __DDS_DIVISOR )
        {

            // Addjust accumulator.
            STEPPER_params.dds_speed.right -= __DDS_DIVISOR;

            // Indicate we have to process.
            speed_process_right = 1;

        } // end if()

        // Process DDS events.
        STEPPER_process_run_mode( speed_process_left, speed_process_right );

    } // end if()

} // end STEPPER_clk()
