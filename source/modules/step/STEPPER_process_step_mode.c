/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_process_step_mode()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_process_step_mode( STEPPER_ID which )
{

    switch( which )
    {

        case LEFT_STEPPER:


            // IF! we still have steps to count down, then...
            if ( STEPPER_params.nSteps.left != 0 )

                // Decrement steps, since we just switched phases.
                STEPPER_params.nSteps.left--;

            // If zero (or if we've reached the deceleration point), 
            // we must have exhausted the number of steps for
            // the left motor, so...
            if (  ( ! STEPPER_params.pending.left ) &&
                  (   STEPPER_params.nSteps.left == 
                                        STEPPER_params.decel_begin.left ) )
            {

                // Set the step speed to zero -- this will begin the decel-
                // eration process (if 'acceleration' value is non-zero).
                STEPPER_set_speed( which, 0 );

                // Set that we should notify once current speed reaches
                // zero for this motor.
                STEPPER_params.pending.left = 1;

            } // end if().


            break;

        case RIGHT_STEPPER:



            // IF! we still have steps to count down, then...
            if ( STEPPER_params.nSteps.right != 0 )

                // Decrement steps, since we just switched phases.
                STEPPER_params.nSteps.right--;

            // If zero (or if we've reached the deceleration point),
            // we must have exhausted the number of steps for
            // the right motor, so...
            if (  ( ! STEPPER_params.pending.right ) &&
                  (   STEPPER_params.nSteps.right ==
                                       STEPPER_params.decel_begin.right ) )
            {

                // Set the step speed to zero.
                STEPPER_set_speed( which, 0 );

                // Set that we should notify once current speed reaches
                // zero for this motor.
                STEPPER_params.pending.right = 1;

            } // end if().


            break;

        case BOTH_STEPPERS:

            // DO nothing.

            break;

    } // end switch()

} // end STEPPER_process_step_mode()
