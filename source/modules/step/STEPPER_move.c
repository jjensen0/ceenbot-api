/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_move()' function.

#include "step324v221.h"
#include "__step324v221.h"

// ======================= private prototypes =============================== //
static inline void STEPPER_freerun( STEPPER_ID          which,

                                    STEPPER_DIR         dir_L,
                                    unsigned short int  speed_L,
                                    unsigned short int  accel_L,

                                    STEPPER_DIR         dir_R,
                                    unsigned short int  speed_R,
                                    unsigned short int  accel_R );

// -------------------------------------------------------------------------- //
static inline void STEPPER_stepmode( STEPPER_RUNMODE        run_mode,
                                     STEPPER_ID             which,

                                     STEPPER_DIR            dir_L,
                                     unsigned short int     steps_L,
                                     unsigned short int     speed_L,
                                     unsigned short int     accel_L,
                                     STEPPER_BRKMODE        brkmode_L,
                                     STEPPER_EVENT_PTR      step_event_L,

                                     STEPPER_DIR            dir_R,
                                     unsigned short int     steps_R,
                                     unsigned short int     speed_R,
                                     unsigned short int     accel_R,
                                     STEPPER_BRKMODE        brkmode_R,
                                     STEPPER_EVENT_PTR      step_event_R );

// ============================ function ==================================== //
void STEPPER_move( STEPPER_RUNMODE      run_mode,
                   STEPPER_ID           which,

                   STEPPER_DIR          dir_L,
                   unsigned short int   steps_L,
                   unsigned short int   speed_L,
                   unsigned short int   accel_L,
                   STEPPER_BRKMODE      brkmode_L,
                   STEPPER_EVENT_PTR    step_event_L,

                   STEPPER_DIR          dir_R,
                   unsigned short int   steps_R,
                   unsigned short int   speed_R,
                   unsigned short int   accel_R,
                   STEPPER_BRKMODE      brkmode_R,
                   STEPPER_EVENT_PTR    step_event_R )
{

    switch( run_mode )
    {

        case STEPPER_FREERUNNING:

            STEPPER_freerun( which, dir_L, speed_L, accel_L,
                                    dir_R, speed_R, accel_R );

        break;

        case STEPPER_STEP_BLOCK:

            STEPPER_stepmode( run_mode, which,
                dir_L, steps_L, speed_L, accel_L, brkmode_L, step_event_L,
                dir_R, steps_R, speed_R, accel_R, brkmode_R, step_event_R );

        break;

        case STEPPER_STEP_NO_BLOCK:

            STEPPER_stepmode( run_mode, which,
                dir_L, steps_L, speed_L, accel_L, brkmode_L, NULL,
                dir_R, steps_R, speed_R, accel_R, brkmode_R, NULL );

        break;

    } // end switch()

} // end STEPPER_move()
// ========================= inline functions =============================== //
static inline void STEPPER_freerun( STEPPER_ID          which,

                                    STEPPER_DIR         dir_L,
                                    unsigned short int  speed_L,
                                    unsigned short int  accel_L,

                                    STEPPER_DIR         dir_R,
                                    unsigned short int  speed_R,
                                    unsigned short int  accel_R )
{

   switch( which )
    {

        case LEFT_STEPPER:
    
            // Set the stepper mode to 'freerunning' mode.
            STEPPER_set_mode( LEFT_STEPPER, STEPPER_NORMAL_MODE );

            // Set the acceleration.
            STEPPER_set_accel( LEFT_STEPPER, accel_L  );

            // Issue a move via 'STEPPER_run()'.
            STEPPER_run( LEFT_STEPPER, dir_L, speed_L );

        break;

        case RIGHT_STEPPER:

            // Set the stepper mode to 'freerunning' mode.
            STEPPER_set_mode( RIGHT_STEPPER, STEPPER_NORMAL_MODE );

            // Set the acceleration.
            STEPPER_set_accel( RIGHT_STEPPER, accel_R  );

            // Issue a move via 'STEPPER_run()'.
            STEPPER_run( RIGHT_STEPPER, dir_R, speed_R );

        break;

        case BOTH_STEPPERS:

            // Set the stepper mode to 'freerunning' mode.
            STEPPER_set_mode( BOTH_STEPPERS, STEPPER_NORMAL_MODE );

            // Set the acceleration.
            STEPPER_set_accel( LEFT_STEPPER,   accel_L );
            STEPPER_set_accel( RIGHT_STEPPER,  accel_R );

            // Issue a move via 'STEPPER_run()'.
            STEPPER_run( LEFT_STEPPER,  dir_L, speed_L );
            STEPPER_run( RIGHT_STEPPER, dir_R, speed_R );

        break;

    } // end switch()

} // end STEPPER_freerun()
// -------------------------------------------------------------------------- //
static inline void STEPPER_stepmode( STEPPER_RUNMODE        run_mode,
                                     STEPPER_ID             which,

                                     STEPPER_DIR            dir_L,
                                     unsigned short int     steps_L,
                                     unsigned short int     speed_L,
                                     unsigned short int     accel_L,
                                     STEPPER_BRKMODE        brkmode_L,
                                     STEPPER_EVENT_PTR      step_event_L,

                                     STEPPER_DIR            dir_R,
                                     unsigned short int     steps_R,
                                     unsigned short int     speed_R,
                                     unsigned short int     accel_R,
                                     STEPPER_BRKMODE        brkmode_R,
                                     STEPPER_EVENT_PTR      step_event_R )
{

    switch( which )
    {

        case LEFT_STEPPER:

            // Set the stepper mode.
            STEPPER_set_mode( LEFT_STEPPER, STEPPER_STEP_MODE );

            // Set the acceleration.
            STEPPER_set_accel( LEFT_STEPPER, accel_L );

            // Issue non-blocking move command.
            STEPPER_stepnb( LEFT_STEPPER, dir_L, steps_L, speed_L, brkmode_L );

            if ( run_mode == STEPPER_STEP_BLOCK )

                // BLOCK until motion completes and then notify.
                STEPPER_wait_and_then( LEFT_STEPPER, step_event_L );

        break;

        case RIGHT_STEPPER:

            // Set the stepper mode.
            STEPPER_set_mode( RIGHT_STEPPER, STEPPER_STEP_MODE );

            // Set the acceleration.
            STEPPER_set_accel( RIGHT_STEPPER, accel_R );

            // Issue non-blocking move command.
            STEPPER_stepnb( RIGHT_STEPPER, dir_R, steps_R, speed_R, brkmode_R );

            if ( run_mode == STEPPER_STEP_BLOCK )

                // BLOCK until motion completes and then notify.
                STEPPER_wait_and_then( RIGHT_STEPPER, step_event_R );

        break;

        case BOTH_STEPPERS:

            // Set the stepper mode.
            STEPPER_set_mode( BOTH_STEPPERS, STEPPER_STEP_MODE );

            // Set the acceleration.
            STEPPER_set_accel( LEFT_STEPPER,  accel_L );
            STEPPER_set_accel( RIGHT_STEPPER, accel_R );

            // Issue  non-blocking move command.
            STEPPER_stepnb( LEFT_STEPPER,  dir_L, steps_L, speed_L, brkmode_L );
            STEPPER_stepnb( RIGHT_STEPPER, dir_R, steps_R, speed_R, brkmode_R );

            if ( run_mode == STEPPER_STEP_BLOCK )

                // BLOCK until motion completes and then notify.
                STEPPER_wait_and_then( BOTH_STEPPERS, 
                                                 step_event_L, step_event_R );

        break;

    } // end switch()

} // end STEPPER_stepmode()
