/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_open()' function.

#include "step324v221.h"
#include "__step324v221.h"

SUBSYS_OPENSTAT STEPPER_open( void )
{

    SUBSYS_OPENSTAT rval;
    
    // Set up default values.
    rval.subsys = SUBSYS_STEPPER;
    rval.state  = SUBSYS_OPEN;

    // Do MCU-specific port initialization.
    __STEPPER_init();

    // Initialize internal state variables.
    //
    // FIXME: Use appropriate functions to modify these.

    // Set the busy status first.  A 'busy' status prevents any
    // 'stepping' to take effect while parameters are being modified.
    STEPPER_params.busy_status = STEPPER_BUSY;

    // Set the default operating mode for each motor.
    STEPPER_params.op_mode.left  = STEPPER_NORMAL_MODE;
    STEPPER_params.op_mode.right = STEPPER_NORMAL_MODE;

    // Initialize the step speed to zero.
    STEPPER_params.step_speed.left  = 0;
    STEPPER_params.step_speed.right = 0;

    // Initilize current step speed parameters.
    STEPPER_params.curr_speed.left  = 0;
    STEPPER_params.curr_speed.right = 0;

    // Initialize stepper acceleration values.
    STEPPER_params.step_accel.left  = 0;
    STEPPER_params.step_accel.right = 0;

    // Initialize stepper deceleration values.
    STEPPER_params.step_decel.left  = 0;
    STEPPER_params.step_decel.right = 0;

    // Initialize step-mode values ('STEP' mode only).
    STEPPER_params.nSteps.left  = 0;
    STEPPER_params.nSteps.right = 0;

    // Initialize step-mode deceleration begin step value.
    STEPPER_params.decel_begin.left  = 0;
    STEPPER_params.decel_begin.right = 0;

    // Set the brake mode to off for both motors.
    STEPPER_params.brake.left  = STEPPER_BRK_OFF;
    STEPPER_params.brake.right = STEPPER_BRK_OFF;

    // Set the 'on-stop' brake mode (for STEP mode) to off.
    STEPPER_params.stop_mode.left  = STEPPER_BRK_OFF;
    STEPPER_params.stop_mode.right = STEPPER_BRK_OFF;

    // Set default direction to FORWARD.
    STEPPER_params.dir_mode.left  = STEPPER_FWD;
    STEPPER_params.dir_mode.right = STEPPER_FWD;

    // Set the default starting 'phase state'.
    STEPPER_params.phase.left  = PHASE0;
    STEPPER_params.phase.right = PHASE0;

    // Set the state of each motor.
    STEPPER_params.astate.left  = STEPPER_STOPPED;
    STEPPER_params.astate.right = STEPPER_STOPPED;

    // Set the power mode to default to LOW power (normal mode).
    STEPPER_params.power_mode = STEPPER_PWR_LOW;

    // Initialize the DDS accumulators.
    STEPPER_params.dds_speed.left  = 0;     // Speed accumulators.
    STEPPER_params.dds_speed.right = 0;

    STEPPER_params.dds_accel.left  = 0;     // Acceleration accumulators.
    STEPPER_params.dds_accel.right = 0;     // (also used during deceleration).

    STEPPER_params.pwm_timeout.left  = 0;  // Stepper PWM timeout counters.
    STEPPER_params.pwm_timeout.right = 0;

    // Initialize the notify pointer to a safe value.
    STEPPER_params.pNotify = 0;

    STEPPER_params.busy_status = STEPPER_NOT_BUSY;

    // Signal the STEPPER subsystem is ready for use.
    SYS_set_state( SUBSYS_STEPPER, SUBSYS_OPEN );

    return rval;

} // end STEPPER_init()
