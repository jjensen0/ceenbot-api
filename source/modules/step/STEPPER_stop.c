/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_stop()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_stop( STEPPER_ID which, STEPPER_BRKMODE brakeMode )
{

    // If the brake mode requested is OFF, then we simply set the
    // velocity to zero -- this will effectively stop the motors.
    if ( brakeMode == STEPPER_BRK_OFF )
    {

        STEPPER_set_speed( which, 0 );
        STEPPER_set_steps( which, 0 );

    } // end if()

    STEPPER_params.busy_status = STEPPER_BUSY;

    // Then update the variables accordingly, and let the 'process'
    // function take care of the rest.
    switch( which )
    {

        case LEFT_STEPPER:

            STEPPER_params.brake.left = brakeMode;

            break;

        case RIGHT_STEPPER:

            STEPPER_params.brake.right = brakeMode;

            break;

        case BOTH_STEPPERS:

            STEPPER_params.brake.left  = brakeMode;
            STEPPER_params.brake.right = brakeMode;

            break;

    } // end switch()

    STEPPER_params.busy_status = STEPPER_NOT_BUSY;

} // end STEPPER_stop()
