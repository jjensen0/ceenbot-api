/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'STEPPER_process_run_mode()' function.

#include "step324v221.h"
#include "__step324v221.h"

void STEPPER_process_run_mode( unsigned char process_left, 
                               unsigned char process_right )
{

    // This variable is used to construct the 'phase bit pattern' for 
    // both stepper motors before being written into the PORT corresponding
    // to stepper motor control so that it can be done in one write cycle
    // instead of two separate ones.
    static unsigned char stepper_pattern = 0;

    // Don't do ANYTHING if the stepper subsystem is busy!
    if ( STEPPER_params.busy_status == STEPPER_NOT_BUSY )
    {

        // *** Left Motor Process ***
        //
        // If not 'braking', then process accordingly.
        if ( STEPPER_params.brake.left == STEPPER_BRK_OFF )
        {

            // If the step speed is zero...
            if ( STEPPER_params.curr_speed.left == 0 )
            {


                // Clear bit pattern for left motor.
                stepper_pattern &= 0b11100011;

                // Set the state to reflect we're stopped.
                STEPPER_params.astate.left = STEPPER_STOPPED;

                // Notify if there is a pending notification.
                // NOTE: The pending parameter is only modified when operating 
                //       in STEP mode.
                if ( STEPPER_params.pending.left )
                {

                    // Apply the brakes as needed so on the next iteration
                    // they take effect.
                    STEPPER_stop( LEFT_STEPPER, STEPPER_params.stop_mode.left );

                    if ( STEPPER_params.pNotify != NULL )
                    {

                        ( STEPPER_params.pNotify )->left = 1;

                        STEPPER_params.pending.left = 0;

                    } // end if()


                } // end if()

            } // end if()

            // Otherwise, if the step speed is not zero, then...
            else
            {


                
                // If the number of steps (while in STEP mode) is 1, then
                // we need to force processing.
                if ( ( STEPPER_params.op_mode.left == STEPPER_STEP_MODE ) &&
                     ( STEPPER_params.nSteps.left  == 1 ) )

                    process_left = 1;

                // Then, IF it's time to process, then go to the next step
                // and proceed accordingly.

                if ( process_left )
                {


                    // Clear the bit pattern for left motor.
                    stepper_pattern &= 0b11100011;

                    // Just set the pattern to the current 'phase'.
                    stepper_pattern |= Motor_L_LUT[ STEPPER_params.phase.left ];

                    // Move to the next 'phase' depending on the direction of
                    // the current motor.  If forward...
                    if ( STEPPER_params.dir_mode.left == STEPPER_FWD )

                        // Move to the next 'phase'.
                        STEPPER_params.phase.left++;

                    // If reversing...
                    else

                        // Move to the previous 'phase'.
                        STEPPER_params.phase.left--;

                    // Ensure phase value is within limits (only 4 states).
                    STEPPER_params.phase.left &= 0x03;

                    // Set the state to notify we're running.
                    STEPPER_params.astate.left = STEPPER_RUNNING;

                    // If the left motor is operating in STEP mode, then we need
                    // to keep track of the step count thus far.
                    if ( STEPPER_params.op_mode.left == STEPPER_STEP_MODE )

                        STEPPER_process_step_mode( LEFT_STEPPER );

                } // end if()

            } // end else.

        } // end if()

        // Else, set the 'braking' bit pattern.
        else
        {

            // Clear the previous bit pattern.
            stepper_pattern &= 0b11100011;

            // Just set the pattern to the current 'phase'.
            stepper_pattern |= Motor_L_LUT[ STEPPER_params.phase.left ];

            // Indicate that this motor is 'braking'.
            STEPPER_params.astate.left = STEPPER_BRAKING;

            // Set current speed to zero.
            STEPPER_params.curr_speed.left = 0;

        } // end else.


        // *** END Left Motor Process ***

        // *** Right Motor Process ***
        //
        // If not 'braking', then process accordingly.
        if ( STEPPER_params.brake.right == STEPPER_BRK_OFF )
        {

            // If the step speed is zero...
            if ( STEPPER_params.curr_speed.right == 0 )
            {


                // Clear the bit pattern for the right motor.
                stepper_pattern &= 0b00011111;

                // Set the state to reflect we're stopped.
                STEPPER_params.astate.right = STEPPER_STOPPED;

                // Notify if there is a pending notification.
                // NOTE: The 'pending' parameter is only modified when operating
                //       in STEP mode.
                if ( STEPPER_params.pending.right )
                {

                    // Apply the brakes as needed so on the next iteration
                    // they take effect.
                    STEPPER_stop( RIGHT_STEPPER, 
                                        STEPPER_params.stop_mode.right );

                    if ( STEPPER_params.pNotify != NULL )
                    {

                        ( STEPPER_params.pNotify )->right = 1;

                        STEPPER_params.pending.right = 0;

                    } // end if()

                } // end if()

            } // end if()

            // Otherwise, if the step speed is not zero, then...
            else
            {



                // If the number of steps (while in STEP mode) is 1, then
                // we need to force processing.
                if ( ( STEPPER_params.op_mode.right == STEPPER_STEP_MODE ) &&
                     ( STEPPER_params.nSteps.right  == 1 ) )

                    process_right = 1;

                // Then, IF it's time to process, then go to the next step
                // and proceed accordingly.
                if ( process_right )
                {

                    // Clear the bit pattern for the right motor.
                    stepper_pattern &= 0b00011111;

                    // Just set the pattern to the current 'phase', and combine
                    // it with the pattern for the 'left' motor.
                   stepper_pattern |= Motor_R_LUT[ STEPPER_params.phase.right ];

                    // Move to the next 'phase' depending on the direction of
                    // the current motor.  If forward...
                    if ( STEPPER_params.dir_mode.right == STEPPER_FWD )

                        // Move to the next 'phase'.
                        STEPPER_params.phase.right++;

                    // If reversing...
                    else

                        // Move to the previous 'phase'.
                        STEPPER_params.phase.right--;

                    // Ensure phasee value is within limits (only 4 states).
                    STEPPER_params.phase.right &= 0x03;

                    // Set the state to notify we're running.
                    STEPPER_params.astate.right = STEPPER_RUNNING;

                    // If the right motor is operating in STEP mode, then we 
                    // need to keep track of the step count thus far.
                    if ( STEPPER_params.op_mode.right == STEPPER_STEP_MODE )

                        STEPPER_process_step_mode( RIGHT_STEPPER );

                } // end if()

            } // end else

        } // end if()

        // Else, set the 'braking' bit pattern.
        else
        {

            // Clear the previous bit pattern.
            stepper_pattern &= 0b00011111;

            // Just set the pattern to the current 'phase' WITHOUT disturbing
            // the existing bit pattern for the left motor.
            stepper_pattern |= Motor_R_LUT[ STEPPER_params.phase.right ];

            // Indicate that this motor is 'braking'.
            STEPPER_params.astate.right = STEPPER_BRAKING;

            // Set current speed to zero.
            STEPPER_params.curr_speed.right = 0;

        } // end else.

        // *** END Right Motor Process ***

    } // end if( Not Busy )

    // If we're in 'LOW POWER' mode, we may need to change the stepper 
    // pattern by processing the time-out counters.
    if ( STEPPER_params.power_mode == STEPPER_PWR_LOW )

        STEPPER_process_pwm( &stepper_pattern, process_left, process_right );

    // Finally, write the pattern to the stepper port in one shot.
    __STEPPER_PORT = (( __STEPPER_PORT & 0b00000011 ) | stepper_pattern ) ;

} // end STEPPER_process()
