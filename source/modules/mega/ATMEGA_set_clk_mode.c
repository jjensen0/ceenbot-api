/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'ATMEGA_set_clk_mode()' function.

#include "mega324v221.h"
#include "__mega324v221.h"

void ATMEGA_set_clk_mode( CLKMODE clk_mode )
{

    // Disable global interrupts ONLY if entering any mode other
    // than 20MHz speed, at which point engaging or disengaging of global
    // interrupts is handled outside of this function.  All other cases
    // are handled in THIS function.

    if ( clk_mode != CLKMODE_20MHZ )

        // Disable global interrupts.
        cli();

    switch( clk_mode )
    {

        // Go to full-speed mode (20MHz).  Presently this mode is only
        // engaged upon reset of the MCU.
        case CLKMODE_20MHZ:

            // Engage supply for 5V operation.
            SBV( 4, PORTB );

            // Allow the clock divider register to be written (modified).
            CLKPR = ( 1 << CLKPCE );

            // NOTE: All the 'CLKPSx' bits MUST be written at once, once
            //       the 'CLKPCE' bit is set to '1'.
            //
            // Set clock divider to factor of 1 -- that's 20MHz full speed.
            CLKPR = 0x00;

            // Allow time for other 3.3V/5V devices to power up.
            for( unsigned int delay = 0; delay < ATMEGA_NORMAL_STARTUP_WAIT ; 
                                                                      ++delay )
            {

                __asm__ __volatile__( "nop" );

            } // end for()

            // Disable all pin change interrupts.
            PCICR  = 0;  // Disable interrupts for pin group PCINT31..0.
            PCMSK0 = 0;  // Disable the PCINT7..0 group.

            break;

        // Set speed to 2MHz (or 20MHz/8) mode.
        case CLKMODE_2MHZ:

            // Allow the clock divider register to be written (modified).
            CLKPR = ( 1 << CLKPCE );

            // NOTE: All the 'CLKPSx' bits MUST be written at once, once
            //       the 'CLKPCE' bit is set to '1'.
            //
            // Set divide by 8.
            CLKPR = 0x03;

            break;

        // At 1MHz, we go to low-speed mode at 2.7V operation.
        case CLKMODE_1MHZ:

            // Set up clock divider change.
            CLKPR = ( 1 << CLKPCE );

            // NOTE: All the 'CLKPSx' bits MUST be written at once, once
            //       the 'CLKPCE' bit is set to '1'.
            //
            // Set divide by 256.
            CLKPR = 0x08;

            break;

        // In this mode, we place the MCU in sleep mode.
        case CLKMODE_SLEEP:

            for( unsigned int delay = 0; delay < ATMEGA_SLEEP_STARTUP_WAIT 
                                                                    ; ++delay )
            {

                __asm__ __volatile__( "nop" );

            } // end for()

            // Set for 2.7V operation.
            CBV( PB4, PORTB );

            // Wait again.
            for( unsigned int delay = 0; delay < ATMEGA_SLEEP_STARTUP_WAIT 
                                                                    ; ++delay )
            {

                __asm__ __volatile__( "nop" );

            } // end for()

            // Disable ADC & se ADC0..2 to digital inputs.
            ADCSRA = 0;

            // Power reduction enable for all peripherals.  Enable pin
            // change interrupt for PA2; used to detect when charger is 
            // plugged in.
            PRR = 0xFF;

            SBV( PCIE0, PCICR );    // Enable interrupt for pin group
                                    // PCINT7..0: this includes PA2 (PCINT2)
                                    // within the PCINT7..0 group, only enable
                                    // PCINT2 for PA2.

            SBV( PCINT2, PCMSK0 );

            // Enable global interrupts.
            sei();

            // Set the 'permission to enter sleep-mode' bit before issuing
            // the 'sleep' instruction.
            SBV( SE, SMCR );

            // Go to sleep.
            __asm__( "sleep" );

            break;

    } // end switch()

    // Re-engage the interrupts ONLY if NOT full speed mode, OR if 
    // we're going to sleep.
    if ( ( clk_mode != CLKMODE_20MHZ ) && ( clk_mode != CLKMODE_SLEEP ) )

        sei();

} // end ATMEGA_set_clk_mode()
