/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'I2C_open()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

SUBSYS_OPENSTAT I2C_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Assume there is an error.
    rval.subsys = SUBSYS_I2C;
    rval.state  = SUBSYS_CLOSED;

    // Only do this if the system is not already open.
    if ( SYS_get_state( SUBSYS_I2C ) == SUBSYS_CLOSED )
    {
        // Initialize internal parameters.
        I2C_params.configured = FALSE;
        I2C_params.master_active = FALSE;
        I2C_params.slave_active  = FALSE;
        I2C_params.using_slave   = FALSE;
        I2C_params.using_pullups = FALSE;

        // Do MCU-specific initialization.
        __I2C_init();

        // Configure the I2C for default operating clock of 100KHz.
        I2C_set_BRG( 23, I2C_PRESCALER_1 );

        // Enable the I2C.
        I2C_enable();
        // Note that the I2C module is open for business.
        SYS_set_state( SUBSYS_I2C, SUBSYS_OPEN );

    } // end if()
    rval.state = SYS_get_state(SUBSYS_I2C);
    return rval;

} // end I2C_open()
