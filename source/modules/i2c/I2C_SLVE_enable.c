/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'I2C_SLV_enable()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_SLVE_enable( unsigned char slave_addr )
{

    // Assume everything will go ok.
    I2C_STATUS rval = I2C_STAT_OK;

    // ONLY allow this to occur IF the I2C is NOT BUSY. If NOT, then...
    if ( I2C_IsBusy() == FALSE )
    {

        // Shift the slave address by one bit.
        slave_addr <<= 1;

        // Write it to the register.
        TWAR = slave_addr;

        // Enable the I2C to recognize its own address, enable 
        // interrupts, and also enable the I2C device itself (if it isn't
        // already).
        TWCR = ( 1 << TWEA ) | ( 1 << TWIE ) | ( 1 << TWEN );

        // Note that the SLAVE portion of the I2C has been enabled for 'use'.
        I2C_params.using_slave = TRUE;
        
    } // end if()

    // Otherwise, note that the system is busy.
    else

        rval = I2C_STAT_BUSY;

    return rval;        

} // end I2C_SLVE_enable()
