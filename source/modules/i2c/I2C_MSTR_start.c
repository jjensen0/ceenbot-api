/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'I2C_start()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_MSTR_start( unsigned char slave_addr, I2C_MODE mode )
{
	unsigned int check = 0;
    // Assume everything will go OK.
    I2C_STATUS rval = I2C_STAT_OK;
    // Only do this if the I2C has been configured.
    if ( I2C_params.configured == TRUE )
    {
        // Next, we only continue, if the SLAVE is not in the middle of
        // an 'active' transaction.
        if ( I2C_params.slave_active != TRUE )
        {
            // ** Note that MASTER is BUSY, so no SLAVE operations
            //    should take place.
            I2C_params.master_active = TRUE;

            // ** Begin with a START condition, also disable the interrupt
            //    if it has been enabled by use of the SLAVE device:
            TWCR = ( 1 << TWINT ) | ( 1 << TWSTA ) | ( 1 << TWEN );

            // Wait for TWINT to be set...

            while( ! ( GBV( TWINT, TWCR ) ) )
            {
            	check++;
            	if(check>=65535)
            	{
            		return I2C_STAT_START_ERROR;
				}
			}

            // Continue only if start-condition went okay.
            if ( ( __I2C_STATUS( TWSR ) == __I2C_STAT_START_OK ) ||
                 ( __I2C_STATUS( TWSR ) == __I2C_STAT_RESTART_OK ) )
            {


                // ** Load the SLAVE ADDRESS into the data register.

                // Determine if we're writing, or receiving.  If receiving...
                if ( mode == I2C_MODE_MR )

                    // Set bit0 to 1 (SLA+R).
                    TWDR = ( slave_addr << 1 ) | 0x01;

                else if ( mode == I2C_MODE_MT )

                    // Set bit0 to 0 (SLAR+W)
                    TWDR = ( slave_addr << 1 );

                // Send the SLAVE address by setting TWINT.
                TWCR = ( 1 << TWINT ) | ( 1 << TWEN );

                // wait for TWINT to be set.
                while( ! ( GBV( TWINT, TWCR ) ) );

                // Make sure ACK-nowledgement has been received -- this depends
                // on the mode we're in...
                if ( mode == I2C_MODE_MR )
                {

                    if ( __I2C_STATUS( TWSR ) != __I2C_STAT_MR_SLAR_ACK_OK )

                        rval = I2C_STAT_ACK_ERROR;


                } // end if()

                else if ( mode == I2C_MODE_MT )
                {

                    if ( __I2C_STATUS( TWSR ) != __I2C_STAT_MT_SLAW_ACK_OK )

                        rval = I2C_STAT_ACK_ERROR;

                } // end else-if()


            } // end if()

            // Otherwise, mark that there was a start error.
            else

                rval = I2C_STAT_START_ERROR;

        } // end if()
            
        // Otherwise, note that SLAVE is active.
        else

            rval = I2C_STAT_SLAVE_BUSY;

    } // end if()        

    else

        rval = I2C_STAT_CONFIG_ERROR;

    return rval;

} // end I2C_MSTR_start()
