/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'I2C_SLVE_finished()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_SLVE_finished( BOOL keep_enabled )
{

    // Assume everything went ok.
    I2C_STATUS rval = I2C_STAT_OK;

    // Make sure the SLAVE is the current active device.
    if ( I2C_params.slave_active == TRUE )
    {

        // Double check the status reflects a request to complete a 
        // transaction.  You can only finish under these two conditions.
        if ( ( __I2C_STATUS( TWSR ) == __I2C_STAT_SR_START_STOP_REQ ) ||
             ( __I2C_STATUS( TWSR ) == __I2C_STAT_ST_DATA_NO_ACK ) )
        {

            // What we do next depends on whether we wish to keep the 
            // SLAVE device enabled or not.
            if ( keep_enabled == TRUE )
            {

                TWCR = ( 1 << TWINT ) | ( 1 << TWEA ) | ( 1 << TWIE ) |
                       ( 1 << TWEN  );

            } // end if()                       

            else

                TWCR = ( 1 << TWINT ) | ( 1 << TWIE ) | ( 1 << TWEN );

            // Note that the SLAVE device is NO LONGER in SLAVE mode.
            I2C_params.slave_active = FALSE;

        } // end if()

    } // end if()

    else

        rval = I2C_STAT_NOT_SLAVE;

    return rval;        

} // end I2C_SLVE_finished()
