/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'I2C_MSTR_get_multiple()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_MSTR_get_multiple( unsigned char *pBuffer,
                                  unsigned short int count )
{

    // Assume everything will go OK.
    I2C_STATUS rval = I2C_STAT_OK;

    // Acknowledgement status should be true except for the LAST byte.
    BOOL acknowledge = TRUE;

    unsigned short int i = 0;

    // Get the bytes one-by-one and STOP if an error occurs.
    for( i = 0; i < count ; ++i )
    {

        // Determine if this is the last byte.  If so...
        if ( i == ( count - 1 ))

            // Do NOT acknowledge.
            acknowledge = FALSE;

        // Receive a 'byte' and store it...
        rval = I2C_MSTR_get( &pBuffer[ i ], acknowledge );

        // Check the status -- if 'no good'...
        if ( rval != I2C_STAT_OK )

            // Get out.
            break;

    } // end for()

    return rval;

} // end I2C_MSTR_get_multiple()
