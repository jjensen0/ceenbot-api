/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'I2C_MSTR_send()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_MSTR_send( unsigned char data )
{

    // Assume everything is ok.
    I2C_STATUS rval = I2C_STAT_OK;

    // First check if the device has been configured.
    if ( I2C_params.configured == TRUE )
    {

        // Then check if the device is operating as MASTER.
        if ( I2C_params.master_active == TRUE )
        {

            // Write data to send into TWDR.
            TWDR = data;

            // ... and send it.
            TWCR = ( 1 << TWINT ) | ( 1 << TWEN );

            // Wait for the TWINT to be set.
            while( ! ( GBV( TWINT, TWCR ) ) );

            // Determine which error to return to the user.
            switch ( __I2C_STATUS( TWSR ) )
            {

                // Data sent AND acknowledged.
                case __I2C_STAT_MT_DATA_ACK_OK:

                    /* DO NOTHING -- 'rval' is already set. */

                break;

                // Data sent AND NOT acknowedged.
                case __I2C_STAT_MT_DATA_NO_ACK:

                    rval = I2C_STAT_NO_ACK;

                break;

                // If none of the above, an UNKNOWN error has occurred.
                default:

                    rval = I2C_STAT_UNKNOWN_ERROR;

            } // end switch()

        } // end if()

        // Otherwise note that we're not in MASTER mode as we should be.
        else

            rval = I2C_STAT_NOT_MASTER;

    } // end if()

    else

        rval = I2C_STAT_CONFIG_ERROR;

    return rval;        

} // end I2C_MSTR_send()
