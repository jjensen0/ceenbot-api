/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'I2C_SLVE_send()' function.

#include "i2c324v221.h"
#include "__i2c324v221.h"

I2C_STATUS I2C_SLVE_send( unsigned char data, BOOL acknowledge_expected )
{

    // Assume everything will go OK.
    I2C_STATUS rval = I2C_STAT_OK;

    // We ONLY continue if the 'MASTER' is NOT busy doing something (i.e.,
    // the MASTER is not in the middle of an 'active' transaction).
    if ( I2C_params.master_active != TRUE )
    {

        // ** Note that the SLAVE is BUSY, so no MASTER operations should
        //    be allowed to take place.
        I2C_params.slave_active = TRUE;

        // Wait for the TWINT to be set (NOTE: it SHOULD be at this point).
        while( ! ( GBV( TWINT, TWCR ) ) );

        // Check if the device has been initially addressed or waiting
        // for the next data byte.
        if( ( __I2C_STATUS( TWSR ) == __I2C_STAT_ST_SLAR_ACK_OK ) ||
            ( __I2C_STATUS( TWSR ) == __I2C_STAT_ST_DATA_ACK_OK ) )
        {

            // Load the TWDR with the data to send.
            TWDR = data;

            // The TWCR should be constructed depending on whether the
            // sent data should or should NOT be acknowleged.
            if( acknowledge_expected == TRUE )
            {

                TWCR = ( 1 << TWINT ) | ( 1 << TWEA ) | ( 1 << TWIE ) |
                       ( 1 << TWEN  ) ;

            } // end if()                       

            else

                TWCR = ( 1 << TWINT ) | ( 1 << TWIE ) | ( 1 << TWEN );

            // Wait for the TWINT to be set.
            while( ! ( GBV( TWINT, TWCR ) ) );

            // Check that the data was sent and acknowledged, and NOT 
            // acknowledged, depending on 'acknowledge_expected'.
            if ( acknowledge_expected == TRUE )
            {

                if ( __I2C_STATUS( TWSR ) != __I2C_STAT_ST_DATA_ACK_OK )

                    rval = I2C_STAT_ACK_ERROR;

            } // end if()

            else
            {

                if ( __I2C_STATUS( TWSR ) != __I2C_STAT_ST_DATA_NO_ACK )

                    rval = I2C_STAT_ACK_ERROR;

                // In this case we want to denote the last byte sequence
                // occurred as expected so that the user can take the 
                // necessary next step.
                else

                    rval = I2C_STAT_NO_ACK;

            } // end else
            
        } // end if()

        // Check if the status was data sent and no acknowledge received.
        else if ( __I2C_STATUS( TWSR ) == __I2C_STAT_ST_DATA_NO_ACK ) 

            rval = I2C_STAT_NO_ACK;

        // Otherwise, we have an unknown error.
        else

            rval = I2C_STAT_UNKNOWN_ERROR;

    } // end if()

    // Otherwise, note that we can't do anything because the MASTER is
    // active.
    else

        rval = I2C_STAT_MASTER_BUSY;

    return rval;

} // end I2C_SLVE_send()
