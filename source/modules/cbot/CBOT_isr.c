/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the interrupt service routines (ISRs) in use
//       by the CEENBoT API.

#include "cbot324v221.h"

// ========================== private define ================================ //
#define __ISR_SAFE_CALL( n ) \
    {\
    if ( CBOT_ISR_vtable[ n ] != NULL ) \
        CBOT_ISR_vtable[ n ]();         \
    }
        
// Desc: This function is needed here to force the linker to bring in the 
//       'CBOT_isr' object module to force undefined references to interrupt
//       service routines to be resolved.
void CBOT_ISR_init( void )
{

    // Do nothing.
    __asm__ __volatile__( "nop" );

} // end if()
// ========================== System ISRs =================================== //
#ifdef __ALL_ISRS_DYNAMIC
// -------------------------------------------------------------------------- //
// *** External Interrupt ISRs ***
// -------------------------------------------------------------------------- //
ISR( INT0_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_INT0_VECT );

} // end ISR( INT0_vect )
// -------------------------------------------------------------------------- //
ISR( INT1_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_INT1_VECT );

} // end ISR( INT1_vect )
// -------------------------------------------------------------------------- //
ISR( INT2_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_INT2_VECT );

} // end ISR( INT2_vect )

#endif /* __ALL_ISRS_DYNAMIC */
// -------------------------------------------------------------------------- //
// *** TIMER0 ISRs ***
// -------------------------------------------------------------------------- //
ISR( TIMER0_COMPA_vect )
{


    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER0_COMPA_VECT );

} // end ISR( TIMER0_COMPA_vect )
// -------------------------------------------------------------------------- //
ISR( TIMER0_COMPB_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER0_COMPB_VECT );
    
} // end ISR( TIMER0_COMPB_vect )
// -------------------------------------------------------------------------- //
ISR( TIMER0_OVF_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER0_OVF_VECT );

} // end ISR( TIMER0_OVF_vect )
// -------------------------------------------------------------------------- //
// *** TIMER 2 ISRs ***
// -------------------------------------------------------------------------- //
ISR( TIMER2_COMPA_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER2_COMPA_VECT );

} // end ISR( TIMER2_COMPA_vect )
// -------------------------------------------------------------------------- //
ISR( TIMER2_COMPB_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER2_COMPB_VECT );

} // end ISR( TIMER2_COMPA_vect )
// -------------------------------------------------------------------------- //
ISR( TIMER2_OVF_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER2_OVF_VECT );

} // end ISR( TIMER2_OVF_vect )
// -------------------------------------------------------------------------- //
// *** TIMER1 ISRs ***
// -------------------------------------------------------------------------- //
ISR( TIMER1_COMPA_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER1_COMPA_VECT );

} // end ISR( TIMER1_COMPA_vect )
// -------------------------------------------------------------------------- //
ISR( TIMER1_COMPB_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER1_COMPB_VECT );

} // end ISR( TIMER1_COMPB_vect )
// -------------------------------------------------------------------------- //
ISR( TIMER1_CAPT_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER1_CAPT_VECT );

} // end ISR( TIMER1_CAPT_vect )
// -------------------------------------------------------------------------- //
ISR ( TIMER1_OVF_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TIMER1_OVF_VECT );

} // end ISR( TIMER1_OVF_vect )
// -------------------------------------------------------------------------- //
// *** Pin-Change ISRs ***
// -------------------------------------------------------------------------- //
ISR( PCINT0_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_PCINT0_VECT );
    
} // end ISR( PCINT0_vect )
// -------------------------------------------------------------------------- //
ISR( PCINT1_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_PCINT1_VECT );

} // end ISR( PCINT1_vect )
// -------------------------------------------------------------------------- //
ISR( PCINT2_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_PCINT2_VECT );

} // end ISR( PCINT2_vect )
// -------------------------------------------------------------------------- //
ISR( PCINT3_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_PCINT3_VECT );

} // end ISR( PCINT3_vect )

#ifdef __ALL_ISRS_DYNAMIC
// -------------------------------------------------------------------------- //
// *** WatchDog ISR ***
// -------------------------------------------------------------------------- //
ISR( WDT_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_WDT_VECT );

} // end ISR( WDT_vect )
// -------------------------------------------------------------------------- //
// *** SPI ***
// -------------------------------------------------------------------------- //
ISR( SPI_STC_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_SPI_STC_VECT );

} // end ISR( SPI_STC_vect )
// -------------------------------------------------------------------------- //
// *** USART0 ***
// -------------------------------------------------------------------------- //
ISR( USART0_RX_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_USART0_RX_VECT );

} // end ISR( USART0_RX_vect )
// -------------------------------------------------------------------------- //
ISR( USART0_UDRE_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_USART0_UDRE_VECT );

} // end ISR( USART0_UDRE_vect )
// -------------------------------------------------------------------------- //
ISR ( USART0_TX_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_USART0_TX_VECT );

}
// -------------------------------------------------------------------------- //
// *** USART1 ***
// -------------------------------------------------------------------------- //
ISR( USART1_RX_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_USART1_RX_VECT );

} // end ISR( USART1_RX_vect )
// -------------------------------------------------------------------------- //
ISR( USART1_UDRE_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_USART1_UDRE_VECT );

} // end ISR( USART1_UDRE_vect )
// -------------------------------------------------------------------------- //
ISR ( USART1_TX_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_USART1_TX_VECT );

}
// -------------------------------------------------------------------------- //
// *** TWI ***
// -------------------------------------------------------------------------- //
ISR( TWI_vect )
{


    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_TWI_VECT );

} // end ISR( TWI_vect )
// -------------------------------------------------------------------------- //
// *** ADC ***
// -------------------------------------------------------------------------- //
ISR( ADC_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_ADC_VECT );

} // end ISR()
// -------------------------------------------------------------------------- //
// *** EEPROM ***
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// *** Analog Comparator ***
// -------------------------------------------------------------------------- //
ISR( ANALOG_COMP_vect )
{

    // Just call the registered CBOT-ISR routine.
    __ISR_SAFE_CALL( ISR_ANALOG_COMP_VECT );

} // end ISR( ANALOG_COMP_vect )
// -------------------------------------------------------------------------- //
// *** FLASH ***
// -------------------------------------------------------------------------- //
#endif /* __ALL_ISRS_DYNAMMIC */
