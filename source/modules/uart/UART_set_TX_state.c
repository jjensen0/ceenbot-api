/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'UART_set_TX_state()'.

#include "uart324v221.h"
#include "__uart324v221.h"

void UART_set_TX_state( UART_ID which, UART_STATE uart_state )
{

    switch( uart_state )
    {

        case UART_ENABLE:

            switch( which )
            {

                case UART_UART0:

                    // If the UART is configured...
                    if ( UART_params.uart0.configured == TRUE ) 
                    {

                        // Enable the transmitter for UART0.
                        SBV( TXEN0, UCSR0B );   // Set Bit 3.

                        // Indicate the transmitter is enabled.
                        UART_params.uart0.TX_enabled = TRUE;

                    } // end if()

                    break;

                case UART_UART1:

                    // If the UART is configured...
                    if ( UART_params.uart1.configured == TRUE ) 
                    {

                        // Enable the transmitter for UART1.
                        SBV( TXEN1, UCSR1B );   // Set Bit 3.

                        // Indicate the transmitter is enabled.
                        UART_params.uart1.TX_enabled = TRUE;

                    } // end if()

                    break;

            } // end switch()

        break;

        case UART_DISABLE:

            switch( which )
            {

                case UART_UART0:

                    // Disable the transmitter for UART0.
                    //
                    // NOTE: We don't care if the UART is configured
                    //       in order to disable it.  Just disable it!
                    //
                    CBV( TXEN0, UCSR0B );   // Clear Bit 3.

                    // Indicate the transmitter is disabled.
                    UART_params.uart0.TX_enabled = FALSE;

                break;

                case UART_UART1:

                    // Disable the transmitter for UART1.
                    //
                    // NOTE: We don't care if the UART is configured
                    //       in order to disable it.  Just disable it!
                    //
                    CBV( TXEN1, UCSR1B );   // Clear Bit 3.

                    // Indicate the transmitter is disabled.
                    UART_params.uart1.TX_enabled = FALSE;

                break;

            } // end switch()

        break;

    } // end switch()

} // end UART_set_TX_state()
