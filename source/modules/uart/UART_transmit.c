/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'UART_transmit()' function.

#include "uart324v221.h"
#include "__uart324v221.h"

UART_COMM_RESULT UART_transmit( UART_ID which, unsigned char data )
{

    // Assume there are no errors.
    UART_COMM_RESULT rval = UART_COMM_OK;

    switch( which )
    {

        case UART_UART0:

            // Transmit only if the transmitter is enabled.
            if ( UART_params.uart0.TX_enabled == TRUE )
            {

                // TODO: Instead of 'if()' we should implement as a 
                //       'while()' loop and do a 'time-out'.
                //
                // Proceed only if the Data Register is ready.
                // If UDRE0 is set, then...
                if ( GBV( UDRE0, UCSR0A ) )
                {

                    // Send it.
                    UDR0 = data;

                    // Wait for the data to be shifted out.
                    while( ! ( GBV( TXC0, UCSR0A ) ) ) /* NULL */ ;

                    // Clear the TXC bit.
                    // NOTE: This bit is cleared by SETTING it.
                    SBV( TXC0, UCSR0A );

                } // end if()

                // Otherwise...
                else
                {

                    // Denote the UART wasn't ready because it couldn't
                    // accept new data.
                    rval = UART_COMM_TX_FULL;

                } // end else.

            } // end if()

            // Otherwise, don't do anything, but let the user know
            // there was an error.
            else

                rval = UART_COMM_ERROR; // Generic ERROR.

        break;



        case UART_UART1:

            // Transmit only if the transmitter is enabled.
            if ( UART_params.uart1.TX_enabled == TRUE )
            {

                // TODO: Instead of 'if()' we should implement as a 
                //       'while()' loop and do a 'time-out'.
                //
                // Proceed only if the Data Register is ready.
                // If UDRE0 is set, then...
                if ( GBV( UDRE1, UCSR1A ) )
                {

                    // Send it.
                    UDR1 = data;

                    // Wait for the data to be shifted out.
                    while( ! ( GBV( TXC1, UCSR1A ) ) ) /* NULL */ ;

                    // Clear the TXC bit.
                    // NOTE: This bit is cleared by SETTING it.
                    SBV( TXC1, UCSR1A );

                } // end if()

                // Otherwise...
                else
                {

                    // Denote the UART wasn't ready because it couldn't
                    // accept new data.
                    rval = UART_COMM_TX_FULL;

                } // end else.

            } // end if()

            // Otherwise, don't do anything, but let the user know
            // there was an error.
            else

                rval = UART_COMM_ERROR; // Generic ERROR.

        break;

    } // end switch()

    return rval;

} // end UART_transmit()
