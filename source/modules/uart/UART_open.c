/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'UART_open()' function.

#include "uart324v221.h"
#include "__uart324v221.h"

SUBSYS_OPENSTAT UART_open( UART_ID which )
{

    SUBSYS_OPENSTAT rval;

    switch( which )
    {

        case UART_UART0:

            // Assume there are no errors.
            rval.subsys = SUBSYS_UART0;
            rval.state  = SUBSYS_OPEN;

            // Only do this *IF* UART0 is currenty closed.
            if ( SYS_get_state( SUBSYS_UART0 ) == SUBSYS_CLOSED )
            {

                // Reset the 'configured' flag.
                UART_params.uart0.configured = FALSE;

                // Reset the 'enabled' flag.
                UART_params.uart0.TX_enabled = FALSE;
                UART_params.uart0.RX_enabled = FALSE;

                // Do MCU-specific initialization for UART0.
                __UART_init( UART_UART0 );

                // Set the default timeout to 5 seconds.
                UART_set_timeout( UART_UART0, 5 );

                // Start the time-out timer to always count in seconds..
                TMRSRVC_new( &UART0_timeout_timer, TMRFLG_NOTIFY_FLAG,
                              TMRTCM_RESTART, TMR_SECS( 1 ) );

                // Annouce the UART0 is open for business.
                SYS_set_state( SUBSYS_UART0, SUBSYS_OPEN );

            } // end if()

        break;

        case UART_UART1:

            // Assume there are no errors.
            rval.subsys = SUBSYS_UART1;
            rval.state  = SUBSYS_OPEN;

            // Only do this *IF* UART1 is currenty closed.
            if ( SYS_get_state( SUBSYS_UART1 ) == SUBSYS_CLOSED )
            {

                // Reset the 'configured' flag.
                UART_params.uart1.configured = FALSE;

                // Reset the 'enabled' flag.
                UART_params.uart1.TX_enabled = FALSE;
                UART_params.uart1.RX_enabled = FALSE;

                // Do MCU-specific initialization for UART1.
                __UART_init( UART_UART1 );

                // Set the default timeout to 5 seconds.
                UART_set_timeout( UART_UART1, 5 );

                // Start the time-out timer to always count in seconds..
                TMRSRVC_new( &UART1_timeout_timer, TMRFLG_NOTIFY_FLAG,
                              TMRTCM_RESTART, TMR_SECS( 1 ) );

                // Annouce the UART1 is open for business.
                SYS_set_state( SUBSYS_UART1, SUBSYS_OPEN );

            } // end if()

        break;

        default:

            rval.subsys = SUBSYS_NA;
            rval.state  = SUBSYS_CLOSED;

    } // end switch()

    return rval;

} // end UART_open()
