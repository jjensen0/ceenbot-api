/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'UART_receive()' function.

#include "uart324v221.h"
#include "__uart324v221.h"

UART_COMM_RESULT UART_receive( UART_ID which, unsigned char *pDest )
{

    // Assume there are no errors.
    UART_COMM_RESULT rval = UART_COMM_OK;

    // UART timeout counters.
    UART_TIMEOUT uart0_timeout;
    UART_TIMEOUT uart1_timeout;

    switch( which )
    {

        case UART_UART0:

            // Initialize the timeout counter.
            uart0_timeout = UART_params.uart0.timeout_sec;

            // Receive ONLY if the receiver is enabled.
            if ( UART_params.uart0.RX_enabled == TRUE )
            {

                // At this point assume a timeout-error will occur.
                rval = UART_COMM_TIMEOUT;

                // Here we'll keep checking until we time out.
                while ( uart0_timeout > 0 )
                {

                    TMRSRVC_on_TC( UART0_timeout_timer, --uart0_timeout );

                    // Check to see if we have data to read.
                    if ( GBV( RXC0, UCSR0A ) )
                    {

                        // Read the data.
                        *pDest = UDR0;

                        // Change the status variable to indicate
                        // the read was successful.
                        rval = UART_COMM_OK;

                        // Break out of the loop.
                        break;

                    } // end if()

                } // end while()

            } // end if()

            // Otherwise, don't do anything, but let the user know
            // there was an error.
            else

                rval = UART_COMM_ERROR;


        break;



        case UART_UART1:

            // Initialize the timeout counter.
            uart1_timeout = UART_params.uart1.timeout_sec;

            // Receive ONLY if the receiver is enabled.
            if ( UART_params.uart1.RX_enabled == TRUE )
            {

                // At this point assume a timeout-error will occur.
                rval = UART_COMM_TIMEOUT;

                // Here we'll keep checking until we time out.
                while ( uart1_timeout > 0 )
                {

                    TMRSRVC_on_TC( UART1_timeout_timer, --uart1_timeout );

                    // Check to see if we have data to read.
                    if ( GBV( RXC1, UCSR1A ) )
                    {

                        // Read the data.
                        *pDest = UDR1;

                        // Change the status variable to indicate
                        // the read was successful.
                        rval = UART_COMM_OK;

                        // Break out of the loop.
                        break;

                    } // end if()

                } // end while()

            } // end if()

            // Otherwise, don't do anything, but let the user know
            // there was an error.
            else

                rval = UART_COMM_ERROR;


        break;

    } // end switch()

    return rval;

} // end UART_receive()
