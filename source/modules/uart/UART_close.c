/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'UART_close()' function.

#include "uart324v221.h"
#include "__uart324v221.h"

void UART_close( UART_ID which )
{

    switch( which )
    {

        case UART_UART0:

            // Only do this *IF* UART0 is currently OPEN.
            if ( SYS_get_state( SUBSYS_UART0 ) == SUBSYS_OPEN )
            {

                // Stop the timer!
                TMRSRVC_stop_timer( &UART0_timeout_timer );

                // Wait for the timer to be removed from the service.
                //
                // FIXME:  We need a way to remove the timer immediately!
                //
                while( ! ( UART0_timeout_timer.tc ) );

                // Disable the transmitter and receiver.
                UART_set_TX_state( UART_UART0, UART_DISABLE );
                UART_set_RX_state( UART_UART0, UART_DISABLE );           

                // Do MCU-specific resource release.
                __UART_close( UART_UART0 );

                // Annouce that UART0 is closed for business.
                SYS_set_state( SUBSYS_UART0, SUBSYS_CLOSED );

            } // end if()

        break;

        case UART_UART1:

            // Only do this *IF* UART1 is currently OPEN.
            if ( SYS_get_state( SUBSYS_UART1 ) == SUBSYS_OPEN )
            {

                // Stop the timer!
                TMRSRVC_stop_timer( &UART1_timeout_timer );

                // Wait for the timer to be removed from the service.
                //
                // FIXME:  We need a way to remove the timer immediately!
                //
                while( ! ( UART1_timeout_timer.tc ) );

                // Disable the transmitter and receiver.
                UART_set_TX_state( UART_UART1, UART_DISABLE );
                UART_set_RX_state( UART_UART1, UART_DISABLE );           

                // Do MCU-specific resource release.
                __UART_close( UART_UART1 );

                // Annouce that UART1 is closed for business.
                SYS_set_state( SUBSYS_UART1, SUBSYS_CLOSED );

            } // end if()

        break;

    } // end switch()

} // end UART_close()
