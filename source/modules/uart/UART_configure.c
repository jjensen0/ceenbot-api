/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'UART_configure()' function.

#include "uart324v221.h"
#include "__uart324v221.h"

// ============================ inline prototypes =========================== //
static inline void __UART0_configure( UART_DBITS data_bits,
                                      UART_SBITS stop_bits,
                                      UART_PARITY parity,
                                      UART_BAUD   baud_rate );
// -------------------------------------------------------------------------- //
static inline void __UART1_configure( UART_DBITS data_bits,
                                      UART_SBITS stop_bits,
                                      UART_PARITY parity,
                                      UART_BAUD   baud_rate );
// -------------------------------------------------------------------------- //
static inline void __UART_UBRR_compute( UART_BAUD baud_rate,
                                        unsigned char *p_UBRR_L,
                                        unsigned char *p_UBRR_H );
// ============================  functions ================================== //
void UART_configure( UART_ID        which,
                     UART_DBITS     data_bits,
                     UART_SBITS     stop_bits,
                     UART_PARITY    parity,
                     UART_BAUD      baud_rate )
{

    switch( which )
    {

        case UART_UART0:

            // Configure UART0.
            __UART0_configure( data_bits, stop_bits, parity, baud_rate );

        break;

        case UART_UART1:

            // Configure UART1.
            __UART1_configure( data_bits, stop_bits, parity, baud_rate );

        break;

    } // end switch()

} // end UART_configure()

// ============================ inline functions ============================ //
static inline void __UART0_configure( UART_DBITS data_bits,
                                      UART_SBITS stop_bits,
                                      UART_PARITY parity,
                                      UART_BAUD baud_rate )
{

    unsigned char UBRR_L = 0;
    unsigned char UBRR_H = 0;
    unsigned char config_bits = 0;

    // Reinitialize all configuration registers so the configuration
    // is guaranteed to always start the same way.
    UCSR0A = 0x00;
    UCSR0B = 0x00;
    UCSR0C = 0x00;

    // Compute UBRR.
    __UART_UBRR_compute( baud_rate, &UBRR_L, &UBRR_H );

    // Prepare positioning of bits.
    config_bits |= ( data_bits << 1 );  // Write data-bits size.
    config_bits |= ( stop_bits << 3 );  // Write the stop-bits.
    config_bits |= ( parity    << 4 );  // Write the parity mode.

    // Set the UBRR registers.
    UBRR0L = UBRR_L;
    UBRR0H = UBRR_H;

    // Write the configuration bits.
    //
    // NOTE: Default operating MODE will be ASYNCHRONOUS.
    //
    UCSR0C = config_bits;

    // If the baud rate is > 57600, then we need to enable the
    // the U2X<n> bit to double the transfer rate.
    if ( baud_rate > 57600 )

        SBV( 1, UCSR0A );

    // Denote that UART0 is now configured, so that it can be enabled
    // at the user's command.
    UART_params.uart0.configured = TRUE;

} // end __UART0_configure()
// -------------------------------------------------------------------------- //
static inline void __UART1_configure( UART_DBITS data_bits,
                                      UART_SBITS stop_bits,
                                      UART_PARITY parity,
                                      UART_BAUD baud_rate )
{

    unsigned char UBRR_L = 0;
    unsigned char UBRR_H = 0;
    unsigned char config_bits = 0;

    // Reinitialize all configuration registers so the configuration
    // is guaranteed to always start the same way.
    UCSR1A = 0x00;
    UCSR1B = 0x00;
    UCSR1C = 0x00;

    // Compute UBRR.
    __UART_UBRR_compute( baud_rate, &UBRR_L, &UBRR_H );

    // Prepare positioning of bits.
    config_bits |= ( data_bits << 1 );  // Write data-bits size.
    config_bits |= ( stop_bits << 3 );  // Write the stop-bits.
    config_bits |= ( parity    << 4 );  // Write the parity mode.

    // Set the UBRR registers.
    UBRR1L = UBRR_L;
    UBRR1H = UBRR_H;

    // Write the configuration bits.
    //
    // NOTE: Default operating MODE will be ASYNCHRONOUS.
    //
    UCSR1C = config_bits;

    // If the baud rate is > 57600, then we need to enable the
    // the U2X<n> bit to double the transfer rate.
    if ( baud_rate > 57600 )

        SBV( 1, UCSR1A );

    // Denote that UART1 is now configured, so that it can be enabled
    // at the user's command.
    UART_params.uart1.configured = TRUE;

} // end __UART1_configure()
// -------------------------------------------------------------------------- //
static inline void __UART_UBRR_compute( UART_BAUD baud_rate,
                                        unsigned char *p_UBRR_L,
                                        unsigned char *p_UBRR_H )
{

    unsigned long int UBRR;

    // Compute the UBRR values based on the equation given for asynchronous
    // operation with U2X<n> bit disabled (if the baud rate <= 57600), or
    // with the U2X<n> bit enabled (if > 57600).
    //
    // NOTE:  The equations below are scaled by 10 so that we perform all
    //        arithmetic operation in the 'integer' domain.  The equations
    //         are then scaled back down.
    if ( baud_rate <= 57600 )

        UBRR = ( ( 10UL * F_CPU ) / ( 16UL * ( baud_rate ) ) ) - 10UL;

    else

        UBRR = ( ( 10UL * F_CPU ) / ( 8UL * ( baud_rate ) ) ) - 10UL;


    // Check if we need to adjust for remainder.
    if ( ( UBRR % 10UL ) >= 5 )

        // Adjust by 1.
        UBRR += 10UL;

    // Divide by 10 to scale it back down.
    UBRR /= 10UL;

    // Get low word.
    *p_UBRR_L = UBRR & 0x000000FF;

    // Get high word.
    *p_UBRR_H = ( UBRR >> 8 ) & 0x000000FF;

} // end __UART_UBRR_compute()
