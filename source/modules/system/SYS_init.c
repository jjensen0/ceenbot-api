/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'SYS_init()' function.

#include "sys324v221.h"

void SYS_init( void )
{

    // FIXME:  Thesee should be modified via 'SYS_set_state()'!
    //
    // Reset internal variables.
    CBOT_system.CPU0_subsys    = SUBSYS_CLOSED;
    CBOT_system.CPU1_subsys    = SUBSYS_CLOSED;
    CBOT_system.PSXC_subsys    = SUBSYS_CLOSED;
    CBOT_system.SPI_subsys     = SUBSYS_CLOSED;
    CBOT_system.LCD_subsys     = SUBSYS_CLOSED;
    CBOT_system.LED_subsys     = SUBSYS_CLOSED;
    CBOT_system.STEP_subsys    = SUBSYS_CLOSED;
    CBOT_system.TMRSRVC_subsys = SUBSYS_CLOSED;
    CBOT_system.SPKR_subsys    = SUBSYS_CLOSED;
    CBOT_system.BEEP_subsys    = SUBSYS_CLOSED;
    CBOT_system.SWATCH_subsys  = SUBSYS_CLOSED;
    CBOT_system.USONIC_subsys  = SUBSYS_CLOSED;
    CBOT_system.UART0_subsys   = SUBSYS_CLOSED;
    CBOT_system.UART1_subsys   = SUBSYS_CLOSED;
    CBOT_system.ADC_subsys     = SUBSYS_CLOSED;
    CBOT_system.ISR_subsys     = SUBSYS_CLOSED;
    CBOT_system.TI_subsys      = SUBSYS_CLOSED;
    CBOT_system.PWM_subsys	   = SUBSYS_CLOSED;


} // end SYS_init()
