/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'SYS_set_state()' function.

#include "sys324v221.h"

void SYS_set_state( SUBSYS which, SUBSYS_ST state )
{

    switch( which )
    {

        case SUBSYS_CPU0:

            CBOT_system.CPU0_subsys = state;

            break;

        case SUBSYS_CPU1:

            CBOT_system.CPU1_subsys = state;

            break;

        case SUBSYS_SPI:

            CBOT_system.SPI_subsys = state;

            break;

        case SUBSYS_SPIFLASH:

            CBOT_system.SPIFLASH_subsys = state;

            break;

        case SUBSYS_LCD:

            CBOT_system.LCD_subsys = state;

            break;

        case SUBSYS_LED:

            CBOT_system.LED_subsys = state;

            break;

        case SUBSYS_PSXC:

            CBOT_system.PSXC_subsys = state;

            break;

        case SUBSYS_STEPPER:

            CBOT_system.STEP_subsys = state;

            break;

        case SUBSYS_TMRSRVC:

            CBOT_system.TMRSRVC_subsys = state;

            break;

        case SUBSYS_SPKR:

            CBOT_system.SPKR_subsys = state;

            break;

        case SUBSYS_BEEP:

            CBOT_system.BEEP_subsys = state;

            break;

        case SUBSYS_SWATCH:

            CBOT_system.SWATCH_subsys = state;

            break;

        case SUBSYS_USONIC:

            CBOT_system.USONIC_subsys = state;

            break;

        case SUBSYS_UART0:

            CBOT_system.UART0_subsys = state;

            break;

        case SUBSYS_UART1:

            CBOT_system.UART1_subsys = state;

            break;

        case SUBSYS_ADC:

            CBOT_system.ADC_subsys = state;

            break;

        case SUBSYS_ISR:

            CBOT_system.ISR_subsys = state;

            break;

        case SUBSYS_TI:

            CBOT_system.TI_subsys = state;

            break;

        case SUBSYS_I2C:

            CBOT_system.I2C_subsys = state;

            break;
            
        case SUBSYS_PWM:
        
        	CBOT_system.PWM_subsys = state;
        
        	break;

       default: /* do nothing */ ;

       	   break;

    } // end switch()

} // end SYS_get_state()
