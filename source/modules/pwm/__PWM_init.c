/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Travis Zoucha
// Desc: Implementation file for '__PWM_init()' function.

#include "pwm324v221.h"
#include "__pwm324v221.h"

void __PWM_init( unsigned short int percentage )
{
	unsigned char Char1; // lower byte 
	unsigned char Char2; // upper byte 

	// Split short into two char 

	Char1 = percentage & 0xFF; 
	Char2 = percentage >> 8; 
    // Setting the direction of PORTD
	DDRD = 0b00010000; //Used for the PWM, only need PD4

	TCCR1A = (1<<COM1B1) | (1<<WGM11) | (1<<WGM10); // WGM11=1,WGM10=1: count to OCRA instead of 0xffff
													// WGM11=1,WGM10=1: count to OCRA instead of 0xffff,
													// CS10=1: use clkI/O with 1:1 prescaler
	TCCR1B = (1<<WGM13) | (1<<WGM12) | (1<<CS10);

	OCR1AH = 0x04;									// count up to 0x271 (625 for 32kHz PWM)
	OCR1AL = 0xb0; 									// count up to 0x271 (625 for 32kHz PWM)

	OCR1BH = Char2; 								// % duty cycle
	OCR1BL = Char1;									// Inputting a 0x00A0 into these registers has safe charge 
													// and was used when putting this together
    // That's it.

} // end __PWM_init()
