/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Travis Zoucha / Modified by Kevin Dethlefs-Moreno
// Desc: Implementation file for 'PWM_open()' function.
// Special thanks goes out to Jose Santos, who without his 
// Format I would have been completely lost on how to apply
// This to the API.  So thank you ;)


#include "pwm324v221.h"
#include "__pwm324v221.h"
#include "adc324v221.h"
#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"
#include "lcd324v221.h"
#include "__lcd324v221.h"
#include "spkr324v221.h"
#include "__spkr324v221.h"

SUBSYS_OPENSTAT PWM_charge( ) //Blocking call for CEENBoT Commander
{
	unsigned short int percentage = 0x00A0;
    ADMUX |= (1<<REFS0);
	ADC_set_channel(ADC_CHAN2);
    double bat_charger_voltage = (double) ADC_sample();
    bat_charger_voltage *= 0.0150715;
    

    SUBSYS_OPENSTAT rval;

    // Assume everything went NOT OK.
    rval.subsys = SUBSYS_PWM;
    rval.state  = SUBSYS_CLOSED;
    
    //Open ONLY if the charger is plugged in
	while(bat_charger_voltage >= 8)
	{
		// Open ONLY if the subsystem is currently closed.
    	if ( SYS_get_state( SUBSYS_PWM ) == SUBSYS_CLOSED )
    	{
    		// Do MCU-specific initialization.
        	__PWM_init( percentage );
    		LCD_open();
    		LCD_clear();
    		SPKR_open(SPKR_BEEP_MODE);
    		SPKR_play_beep(500, 2000, 100);
    		unsigned char secs = 00;
    		unsigned char min = 00;
    		unsigned char h = 00;
    		unsigned char hours = 24;
    		while(h != 24)
    		{
    			LCD_printf(" Recovery Charging\n Done in %d hours\nThen reload firmware\nand finish charging.\n", hours);
    			secs++;
    			TMRSRVC_delay( TMR_SECS( 1 ) );
    			if(secs == 59 && min == 59)
    			{
    				secs = 0; 
    				min = 0;
    				TMRSRVC_delay( TMR_SECS( 1 ) );
    				h++;
    				hours--;
    			}
    			if(secs == 59)
    			{
    				TMRSRVC_delay( TMR_SECS( 1 ) );
    				secs = 0;
    				min++;
    				if(min % 10 == 0)
    				{
    					SPKR_play_beep(500, 1000, 100);
    				}
    			}
    		}
    		__PWM_close();
    		LCD_clear();
    		printf("      Charged\n  Please disconnect\n       from AC\n\n");
    		SPKR_play_beep(500, 2000, 100);
    		

	        // Denote the PWM is open for business.
    	    SYS_set_state( SUBSYS_PWM, SUBSYS_OPEN );

    		 
		}
	}// end if()
	ADMUX &= ~(1<<REFS0);
	LCD_clear();
    return rval;

} // end PWM_open()
