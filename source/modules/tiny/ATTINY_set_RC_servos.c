/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'ATTINY_set_RC_servos()' function.

#include "tiny324v221.h"

void ATTINY_set_RC_servos( unsigned int RCS0_pos,
                           unsigned int RCS1_pos,
                           unsigned int RCS2_pos,
                           unsigned int RCS3_pos,
                           unsigned int RCS4_pos )
{

    // Only do this if the subsystem is OPEN.
    if ( SYS_get_state( SUBSYS_CPU1 ) == SUBSYS_OPEN )
    {

        // Start with message separator.
        SPI_set_slave_addr( SPI_ADDR_NA );

        // Wait a bit.
        DELAY_us( 35 );

        // Set the slave address to communicate with 'tiny'.
        SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_ATTN );  // Get it's attention.
        SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_SERVO ); // We're sending servo
                                                        // data.

        // Enforce limits:
        
        // Servo 0.
        if ( RCS0_pos > ATTINY_params.servos.limits.max.RCS0 ) 
            
            RCS0_pos = ATTINY_params.servos.limits.max.RCS0;

        else if ( RCS0_pos < ATTINY_params.servos.limits.min.RCS0 )

            RCS0_pos = ATTINY_params.servos.limits.min.RCS0;

        // Servo 1.
        if ( RCS1_pos > ATTINY_params.servos.limits.max.RCS1 ) 
            
            RCS1_pos = ATTINY_params.servos.limits.max.RCS1;

        else if ( RCS1_pos < ATTINY_params.servos.limits.min.RCS1 )

            RCS1_pos = ATTINY_params.servos.limits.min.RCS1;

        // Servo 2.
        if ( RCS2_pos > ATTINY_params.servos.limits.max.RCS2 ) 
            
            RCS2_pos = ATTINY_params.servos.limits.max.RCS2;

        else if ( RCS2_pos < ATTINY_params.servos.limits.min.RCS2 )

            RCS2_pos = ATTINY_params.servos.limits.min.RCS2;

        // Servo 3.
        if ( RCS3_pos > ATTINY_params.servos.limits.max.RCS3 ) 
            
            RCS3_pos = ATTINY_params.servos.limits.max.RCS3;

        else if ( RCS3_pos < ATTINY_params.servos.limits.min.RCS3 )

            RCS3_pos = ATTINY_params.servos.limits.min.RCS3;

        // Servo 4.
        if ( RCS4_pos > ATTINY_params.servos.limits.max.RCS4 ) 
            
            RCS4_pos = ATTINY_params.servos.limits.max.RCS4;

        else if ( RCS4_pos < ATTINY_params.servos.limits.min.RCS4 )

            RCS4_pos = ATTINY_params.servos.limits.min.RCS4;

        // ** Store the current position for when the user wishes to
        //    use 'ATTINY_set_RC_servo()' (singular) function.
        ATTINY_params.servos.pos.RCS0 = RCS0_pos;
        ATTINY_params.servos.pos.RCS1 = RCS1_pos;
        ATTINY_params.servos.pos.RCS2 = RCS2_pos;
        ATTINY_params.servos.pos.RCS3 = RCS3_pos;
        ATTINY_params.servos.pos.RCS4 = RCS4_pos;

        // ** Send RC-servo 0 data.
        
        // Wait a bit.
        DELAY_us( 35 );

        // Send it.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS0_pos >> 8 ) ); // High-byte.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS0_pos ) );      // Low-byte.

        // ** Send RC-servo 1 data.
        
        // Wait a bit.
        DELAY_us( 35 );

        // Send it.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS1_pos >> 8 ) ); // High-byte.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS1_pos ) );      // Low-byte.

        // ** Send RC-servo 2 data.
        
        // Wait a bit.
        DELAY_us( 35 );

        // Send it.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS2_pos >> 8 ) ); // High-byte.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS2_pos ) );      // Low-byte.

        // ** Send RC-servo 3 data.
        
        // Wait a bit.
        DELAY_us( 35 );

        // Send it.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS3_pos >> 8 ) ); // High-byte.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS3_pos ) );      // Low-byte.

        // ** Send RC-servo 4 data.
        
        // Wait a bit.
        DELAY_us( 35 );

        // Send it.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS4_pos >> 8 ) ); // High-byte.
        SPI_transmit( SPI_ADDR_ATTINY0, ( RCS4_pos ) );      // Low-byte.

        // Wait a bit.
        DELAY_us( 35 );

        // Set slave address to mark message separator.
        SPI_set_slave_addr( SPI_ADDR_NA );

    } // end if()


} // end ATTINY_set_RC_servos()
