/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'ATTINY_get_IR_state()' function.

#include "tiny324v221.h"

// ============================== private prototypes ======================== //
static inline
    BOOL __ATTINY_process_IR_state( unsigned char sensors, ATTINY_IR which );

// ============================== private globals =========================== //
// Timer object for controlling how often a new 'IR sample' is obtained
// from the 'Tiny.
static TIMEROBJ ir_wait_timer;

// Bit-mask table saved in an array so that we can easily loop through it.
static const char __ir_bit_mask[] = {

    SNSR_IR_LEFT,
    SNSR_IR_RIGHT,
    ( SNSR_IR_LEFT | SNSR_IR_RIGHT ),
    ( SNSR_IR_LEFT | SNSR_IR_RIGHT )

};    

// ================================== functions ============================= //
BOOL ATTINY_get_IR_state( ATTINY_IR which )
{

    BOOL rval = FALSE;

    BOOL start_timer = FALSE;

    static BOOL waiting = FALSE;

    static unsigned char sensors = 0;

    // If NOT waiting...
    if ( !waiting )

        // Denote we have to start the timer.
        start_timer = TRUE;

    // Otherwise, if waiting...
    else
    {

        // Check if the 'wait time' is up...
        if ( ir_wait_timer.tc )
        {

            // Reset the no-longer-waiting flag.
            waiting = FALSE;

            // But still restart the timer.
            start_timer = TRUE;

            // Reset the timer flag.
            ir_wait_timer.tc = 0;

        } // end if()

    } // end else.

    // Check if we need to start the timer.
    if ( start_timer == TRUE )
    {

        // Query the 'Tiny for new sensor status bits.
        sensors = ATTINY_get_sensors();

        // Start the timer...
        TMRSRVC_new( &ir_wait_timer, TMRFLG_FLAGNOTIFY, TMR_TCM_RUNONCE,
                                                        __IR_WAIT_DELAY );

        // Note that we're waiting...
        waiting = TRUE;

    } // end if()

    // Proces the status of the requested IR(s) sensors.
    rval = __ATTINY_process_IR_state( sensors, which );

    return rval;

} // end ATTINY_get_IR_state()
// -------------------------------------------------------------------------- //
BOOL __ATTINY_process_IR_state( unsigned char sensors, ATTINY_IR which )
{

    BOOL rval = FALSE;

    switch( which )
    {

        case ATTINY_IR_LEFT:
        case ATTINY_IR_RIGHT:
        case ATTINY_IR_EITHER:

            if ( sensors & __ir_bit_mask[ which ] )

                rval = TRUE;

        break;


        case ATTINY_IR_BOTH:

            if ( ( sensors & __ir_bit_mask[ ATTINY_IR_LEFT ] ) &&
                 ( sensors & __ir_bit_mask[ ATTINY_IR_RIGHT ] ) )

                 rval = TRUE;

        break;

        default: /* DO NOTHING. */ ;

    } // end switch()

    return rval;

} // end __ATTINY_process_IR_state()
