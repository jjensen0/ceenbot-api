/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'ATTINY_get_sensors()' function.

#include "tiny324v221.h"

unsigned char ATTINY_get_sensors( void )
{

    unsigned char rval = 0;     // 'rval' will collect sensor data.

    // Make sure the ATTINY is open for business.
    if ( SYS_get_state( SUBSYS_CPU1 ) == SUBSYS_OPEN )
    {

        // Set slave address to mark message separator.
        SPI_set_slave_addr( SPI_ADDR_NA );

        // Indicate message we wish to receive to ATtiny.
        SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_ATTN );   // Get it's attention.
        SPI_transmit( SPI_ADDR_ATTINY0, ATTMSG_SW_IR );  // We want sensor data.

        // Give the ATtiny a moment to process this info.
        DELAY_us( 30 );

        // Read the data.
        rval = SPI_receive( SPI_ADDR_ATTINY0, SPI_NULL_DATA );

        DELAY_us( 30 );

        // Read the XOR verification byte (and discard it).
        SPI_receive( SPI_ADDR_ATTINY0, SPI_NULL_DATA );

        DELAY_us( 30 );

        // Set slave address to mark message separator.
        SPI_set_slave_addr( SPI_ADDR_NA );

        // Wait a bit in case this function is invoked again.  The TINY needs
        // time to settle and respond.
        DELAY_ms( 1 );

    } // end if()

    // Return the data.
    return rval;

} // end ATTINY_get_sensors()
