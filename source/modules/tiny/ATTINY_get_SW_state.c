/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'ATTINY_get_SW_state()' function.

#include "tiny324v221.h"

// ============================= private prototypes ======================== //
static inline 
    BOOL __ATTINY_process_SW_state( unsigned char sensors, ATTINY_SW which );

// =========================== private globals ============================= //
// Timer object for contolling how often a new 'switch sample' is obtained
// from the 'Tiny.
static TIMEROBJ sw_wait_timer;

// Bit-mask table saved in an array so that we can easily loop through it.
static const char __sw_bit_mask[] = {

    SNSR_SW3_STATE, // Switch 3 bitmask.
    SNSR_SW4_STATE, // Switch 4 bitmask.
    SNSR_SW5_STATE  // Switch 5 bitmask.

};

// The 'released[]' state indicates the 'physical' state of the pushbutton.
// If the user has the button depressed, then 'release[]' will read 'FALSE'
// for the corresponding entry.  If s/he lets go, then 'release[]' will read
// 'TRUE'.
static BOOL released[] = {

    TRUE,           // SW3.
    TRUE,           // SW4.
    TRUE            // SW5.

};

// The 'active[]' state indicates the state of the switch as reported TO the
// USER and does not (or is not always equal to), the 'physical' state of
// said switch.
static BOOL active[] = {

    FALSE,          // SW3.
    FALSE,          // SW4.
    FALSE           // SW5.

};

// ============================= functions ================================= //
BOOL ATTINY_get_SW_state( ATTINY_SW which )
{

    BOOL rval = FALSE;

    BOOL start_timer = FALSE;

    static BOOL waiting = FALSE;

    static unsigned char sensors = 0;

    // If NOT waiting...
    if ( !waiting )

        // Denote we have to start the timer.
        start_timer = TRUE;

    // Otherwise, if waiting...
    else
    {

        // Check if the 'wait time' is up...
        if ( sw_wait_timer.tc )
        {

            // Reset the no-longer-waiting flag.
            waiting = FALSE;

            // But still restart the timer.
            start_timer = TRUE;

            // Reset the timer flag.
            sw_wait_timer.tc = 0;

        } // end if()

    } // end else.

    // Check if we need to start the timer.
    if ( start_timer == TRUE )
    {

        // Query the 'Tiny for new sensor status bits.
        sensors = ATTINY_get_sensors();

        // Start the timer...
        TMRSRVC_new( &sw_wait_timer, TMRFLG_FLAGNOTIFY, TMR_TCM_RUNONCE,
                                                        __SW_WAIT_DELAY );

        // Note that we're waiting...
        waiting = TRUE;

    } // end if()


    // Process the status of the requested switch.
    rval = __ATTINY_process_SW_state( sensors, which );

    return rval;

} // end ATTINY_get_SW_state()
// -------------------------------------------------------------------------- //
BOOL __ATTINY_process_SW_state( unsigned char sensors, ATTINY_SW which )
{

    // If the button is currently released...
    if ( released[ which ] )
    {

        // If the corresponding bit is active...
        if ( sensors & __sw_bit_mask[ which ] )
        {

            // Note that it is active.
            active[ which ] = TRUE;

            // Note that is is depressed.
            released[ which ] = FALSE;

        } // end if()

    } // end if()

    // Otherwise, if NOT released...
    else
    {

        // ...but, still active...
        if ( active[ which ] )

            // Turn it off.
            active[ which ] = FALSE;

        // Also, check if the user has released the button.
        if ( ! ( sensors & __sw_bit_mask[ which ] ) )

            released[ which ] = TRUE;

    } // end else

    // Return the state of 'active'.
    return active[ which ];

} // end __ATTINY_get_SW3_state()
// -------------------------------------------------------------------------- //
