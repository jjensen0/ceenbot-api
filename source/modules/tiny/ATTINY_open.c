/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'ATTINY_open()' function.

#include "tiny324v221.h"

SUBSYS_OPENSTAT ATTINY_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Set default values.
    rval.subsys = SUBSYS_CPU1;
    rval.state  = SUBSYS_OPEN;

    // The ATTiny is an SPI device.  Determine if the SPI is open.
    if ( SYS_get_state( SUBSYS_SPI ) == SUBSYS_CLOSED )
    {

        // State the specific error.
        rval.subsys = SUBSYS_SPI;
        rval.state  = SUBSYS_CLOSED;

    } // end if()

    // Otherwise, the ATTINY is good to go.
    else
    {

        // Initialize current position values.
        ATTINY_params.servos.pos.RCS0 = 0;
        ATTINY_params.servos.pos.RCS1 = 0;
        ATTINY_params.servos.pos.RCS2 = 0;
        ATTINY_params.servos.pos.RCS3 = 0;
        ATTINY_params.servos.pos.RCS4 = 0;

        // Initialize the servo limits.
        ATTINY_params.servos.limits.min.RCS0 = SERVO_POS_MIN_LIMIT;
        ATTINY_params.servos.limits.min.RCS1 = SERVO_POS_MIN_LIMIT;
        ATTINY_params.servos.limits.min.RCS2 = SERVO_POS_MIN_LIMIT;
        ATTINY_params.servos.limits.min.RCS3 = SERVO_POS_MIN_LIMIT;
        ATTINY_params.servos.limits.min.RCS4 = SERVO_POS_MIN_LIMIT;

        ATTINY_params.servos.limits.max.RCS0 = SERVO_POS_MAX_LIMIT;
        ATTINY_params.servos.limits.max.RCS1 = SERVO_POS_MAX_LIMIT;
        ATTINY_params.servos.limits.max.RCS2 = SERVO_POS_MAX_LIMIT;
        ATTINY_params.servos.limits.max.RCS3 = SERVO_POS_MAX_LIMIT;
        ATTINY_params.servos.limits.max.RCS4 = SERVO_POS_MAX_LIMIT;


        // Set system variable to denote the ATtiny is open for business.
        SYS_set_state( SUBSYS_CPU1, SUBSYS_OPEN );

    } // end if()

    return rval;

} // end ATTINY_open()
