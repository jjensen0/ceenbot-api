/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'ATTINY_set_RC_limits()' function.

#include "tiny324v221.h"

void ATTINY_set_RC_limits( unsigned int min,
                           unsigned int max,
                           RCSERVO_ID which )
{

    // We need to limit the upper limit to avoid messing with the 20ms.
    if ( max > SERVO_POS_MAX_LIMIT )

        max = SERVO_POS_MAX_LIMIT;

    switch( which )
    {

        case RC_SERVO0:

            ATTINY_params.servos.limits.min.RCS0 = min;
            ATTINY_params.servos.limits.max.RCS0 = max;

            break;

        case RC_SERVO1:

            ATTINY_params.servos.limits.min.RCS1 = min;
            ATTINY_params.servos.limits.max.RCS1 = max;

            break;

        case RC_SERVO2:

            ATTINY_params.servos.limits.min.RCS2 = min;
            ATTINY_params.servos.limits.max.RCS2 = max;

            break;

        case RC_SERVO3:

            ATTINY_params.servos.limits.min.RCS3 = min;
            ATTINY_params.servos.limits.max.RCS3 = max;

            break;

        case RC_SERVO4:

            ATTINY_params.servos.limits.min.RCS4 = min;
            ATTINY_params.servos.limits.max.RCS4 = max;

            break;

    } // end switch()

} // end ATTINY_set_RC_limits()
