/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'ADC_set_channel()'.

#include "adc324v221.h"
#include "__adc324v221.h"

void ADC_set_channel( ADC_CHAN which )
{

    // The default previous channel should be '31', which is GND (0V).
    static unsigned char previous_chan = 31;

    // If we're specifyin ADC channels 0-7, then...
    if ( previous_chan < 8 )
    {

        // We need to re-connect the previous channel's DIGITAL pin.
        CBV( previous_chan, DIDR0 );

        // ... and disconect the current channel from the DIGITAL pin.
        SBV( which, DIDR0 );

        // Save it for next time.
        previous_chan = ( unsigned char ) which;
        
    } // end if()

    // Otherwise...
    else
    {

        // Just disconnect the DIGITAL pin for the requested channel.
        SBV( which, DIDR0 );

        // Save it for the next time.
        previous_chan = ( unsigned char ) which;

    } // end else.


    // Enusre the channel specified is within limits (0-31).
    which &= 0b00011111;

    // Take the current state, clear lower 5-bits, attach new bits and write.
    ADMUX = ( ADMUX & 0b11100000 ) | which;

} // end ADC_set_channel().
