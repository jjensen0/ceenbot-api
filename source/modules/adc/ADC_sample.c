/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'ADC_sample()' function.

#include "adc324v221.h"
#include "__adc324v221.h"

ADC_SAMPLE ADC_sample( void )
{

    ADC_SAMPLE sample = 0;

    unsigned char sample_L = 0;
    unsigned char sample_H = 0;

    // Only do this if the ADC subsystem module is open.
    if ( SYS_get_state( SUBSYS_ADC ) == SUBSYS_OPEN )
    {

        // Trigger the ADC to do a single-shot sample (i.e., to start the
        // 'conversion').
        SBV( ADSC, ADCSRA );

        // Wait for the conversion to complete.
        while( GBV( ADSC, ADCSRA ) ) /* NULL */ ;

        // Return the sample.
        //
        // NOTE: In the default ADC settings, ADCL MUST be read first prior
        //       to reading ADCH! (Consule datasheet for details).
        //
        sample_L = ADCL;    // Get Low-byte.
        sample_H = ADCH;    // Get remaining upper bits.

        // Combine low and high samples to obtain 10-bits.
        sample = ( sample_H << 8 ) | sample_L;

    } // end if()

    return sample;

} // end ADC_sample()
