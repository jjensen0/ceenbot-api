/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation for the '__TI_CMD_beep()' function.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

void __TI_CMD_play_beep_pattern( TI_PARAMS *p_TI_params )
{

    // Start by opening the speaker subsystem module in 'beep' mode.
    SPKR_open( SPKR_BEEP_MODE );

    unsigned short int i = 0;

    LCD_clear();
    LCD_printf_PGM( PSTR( "  Playing Beep\n" ) );
    LCD_printf_PGM( PSTR( "      Pattern...\n" ) );
    LCD_printf_PGM( PSTR( "Freq: %d Hz\n" ), p_TI_params->data_buffer[ 1 ] );

    // We need to loop this the number of times indicated by the user.
    for( i = 0; i < ( p_TI_params->data_buffer[ 4 ] ); ++i )
    {

        // Invoke the API function from the SPKR subsystem module.
        SPKR_play_beep( ( SPKR_FREQ )( p_TI_params->data_buffer[ 1 ] ),
                        ( SPKR_TIME )( p_TI_params->data_buffer[ 2 ] ),
                        ( unsigned short int )( p_TI_params->data_buffer[ 3 ] )
        );

    } // end for()

    LCD_printf_PGM( PSTR( "Done!" ) );

    // Close the speaker subsystem module.
    SPKR_close( SPKR_BEEP_MODE );

    __TI_return( 0 );

} // end __TI_CMD_beep()
