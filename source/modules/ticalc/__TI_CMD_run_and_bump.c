/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the '__TI_CMD_run_and_bump()' function.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

// Private proto:
inline unsigned char check_sensors( void );

// Functions:
void __TI_CMD_run_and_bump( TI_PARAMS *p_TI_params )
{

    unsigned char which_IR = 0;

    // Get the timeout first.
    TIMER16 time = p_TI_params->data_buffer[ 4 ];

    LCD_clear();

    

    // Ensure timeout is not greater than 30.
    if ( time < 0 )

        time = 0;

    else if ( time > 30 )

        time = 30;

    time *= 1000;  // Scale to milliseconds.

    // Start the interval timer to go off 8 times per second.
    TMRSRVC_new( &bump_check_timer, TMRFLG_NOTIFY_FLAG, TMRTCM_RESTART, 125 );

    // Star the timeout timer.
    TMRSRVC_new( &timeout_timer, TMRFLG_NOTIFY_FLAG, TMRTCM_RUNONCE, time );

    if ( p_TI_params->data_len > 4 )
    {

        // Start the motion.
        STEPPER_run( ( STEPPER_ID )         p_TI_params->data_buffer[ 1 ],
                     ( STEPPER_DIR )        p_TI_params->data_buffer[ 2 ],
                     ( unsigned short int ) p_TI_params->data_buffer[ 3 ] );

        LCD_printf_PGM( PSTR( "Waiting to bump...\n" ) );                     

        while( 1 )
        {

            // Check if it's time to check the sensors.
            TMRSRVC_on_TC( bump_check_timer, which_IR = check_sensors() );

            // If an IR has been tripped.
            if ( which_IR != 0 )
            {

                LCD_printf_PGM( PSTR( "Bump hit!\n" ) );

                // Get out.
                break;

            } // end if()

            // Check if it's time to get out.
            TMRSRVC_on_TC( timeout_timer, break );


        } // end while()

        // Stop the motors either way.
        STEPPER_stop( STEPPER_BOTH, STEPPER_BRK_OFF );

        // Just to be safe, reset the timer flags.
        timeout_timer.tc    = 0;
        bump_check_timer.tc = 0;

        // Ensure both timers will terminate.
        TMRSRVC_stop_timer( &timeout_timer );
        TMRSRVC_stop_timer( &bump_check_timer );
        
    } // end if()

    else
    {

        __TI_print_param_error_msg();

    } // end else.

    LCD_printf_PGM( PSTR( "Done.\n" ) );

    __TI_return( which_IR );


} // end __TI_CMD_run_and_bump()
// -------------------------------------------------------------------------- //
unsigned char check_sensors( void )
{

    unsigned char sensors = 0;
    unsigned char rval = 0;

    // Get sensors.
    sensors = ATTINY_get_sensors();

    // First check if both sensors are tripped.
    if ( ( sensors & SNSR_IR_LEFT ) &&
         ( sensors & SNSR_IR_RIGHT ) )

        // Specify that both IR sensors have tripped.
        rval = 3;

    else if ( sensors & SNSR_IR_LEFT )
    {

        // Specify which IR stopped the motion.
        rval = 1;

    } // end if ()

    else if ( sensors & SNSR_IR_RIGHT )
    {

        // Specify which IR stopped the motion.
        rval = 2;

    } // end if()

    return rval;

} // end check_sensors()
