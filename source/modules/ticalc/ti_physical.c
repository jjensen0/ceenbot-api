/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
/*
 * Global Includes
 */
#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#include "ti324v221.h"

/*
 * Local Includes
 */

#include "__ti324v221.h"
#include "__ticmd324v221.h"

#define __TI_ACK_DELAY  10     /* In micro-seconds. */
#define __TI_PUTC_DELAY 10     /* In micro-seconds. */

/*
 * Global data.
 */

/*
 * Define module-level data.
 */
static uint8_t  s_transfer_char   = 0;
static uint8_t  s_active_bit      = 0;

#ifdef __DEBUG_TI_UART0_MSG
    static unsigned char byte_num = 0; 
#endif

/*
 * Define local function prototypes.
 */

/*
 * Define main functions.
 */

// -------------------------------------------------------------------------- //
/* Initialize the necessary interrupts and set the necessary variables for
 * this framework.
 */
TI_port_status_t TI_init(void)
{

    TI_port_status_t connection;

    BOOL red_ok = FALSE, white_ok = FALSE;

    // Determine the presence of calculator.

    // NOTE:  There's two ways to do this, previously, a 'glitchy' test was
    //        performed, whereby the RED line was pulled low and we waited
    //        for the calculator to acknowledge and then let it time out.
    //
    //        With the MOSFETs adapter boards, we can detect calculator
    //        presence by ensuring that the input lines are both HIGH.  If this
    //        is not the case, then either the calculator is OFF, or there's
    //        no calculator attached, period.  For now, we'll try this approach
    //        and see how it works -- otherwise, we'll default back to the
    //        'glitchy' test mechanism.

    // Wait a bit before checking...
    DELAY_ms( 100 );
    DELAY_ms( 100 );

    if ( __GET_RED_WIRE()   ) 

        red_ok   = TRUE;

    DELAY_ms( 5 );        // Wait a bit before sampling the next, just in 
                           // case something 'glitchy' is going on.

    if ( __GET_WHITE_WIRE() ) 

        white_ok = TRUE;

#ifdef __DEBUG_TI_UART0_MSG

    if ( red_ok && white_ok )
    {

        UART0_printf( "TI_init(): Both wires HIGH as they should be.\n" );

    } // end if()
    else
    {

        UART0_printf( "TI_init(): ERROR: One or both wires are NOT HIGH.\n" );

    } // end else

#endif

    // Determine if port should be OPENED.
    if ( red_ok && white_ok )

        connection = TI_ATTACHED_PORT;

    else

        connection = TI_OPEN_PORT;


    return connection;

} // end TI_init()

// -------------------------------------------------------------------------- //
/* pin change interrupt.
 */
CBOT_ISR( TI_isr )
{


    BOOL    end_isr = FALSE;

#ifdef __DEBUG_TI_UART0_MSG

    unsigned int i = 0;

    byte_num = 0;

#endif

    enum {

        RED_IN,     /* Recv a '0' */
        WHITE_IN,   /* Recv a '1' */
        NA_IN

    } pin_cmd = NA_IN;   /* responsible for directing actions with robot. */

    /* 
     * This happens too quickly to interrupt on every pin change. The whole
     * packet must be processed. In order to acommodate this need,
     * we disable this pin's interrupt and re-enable interrupts.
     */

     // Stop listening to pin-change interrupts while we process this.
     __TI_port_mute();

#ifdef __DEBUG_TI_UART0_MSG

     UART0_printf_PGM( PSTR( "In.\n" ) );

#else     

    // Allow for time before we start looking at the line levels.
    DELAY_us( 20 );

#endif

    // NOTE: DO NOT RE-ENABLE GLOBAL INTERRUPTS!  IT CAUSES ISSUES!

    while( !end_isr )
    {

        unsigned long int wait = 0x000FFFFF;

        /* debounce */
        while( __GET_RED_WIRE() && __GET_WHITE_WIRE() && wait > 0 ) wait-- ;

        // Wait a bit, to make sure this was not a false trip.
        DELAY_us( 20 );

        while( __GET_RED_WIRE() && __GET_WHITE_WIRE() && wait > 0 ) wait-- ;

        /*
        while( __GET_RED_WIRE() && __GET_WHITE_WIRE() && wait > 0 ) wait-- ;
        while( __GET_RED_WIRE() && __GET_WHITE_WIRE() && wait > 0 ) wait-- ;
        while( __GET_RED_WIRE() && __GET_WHITE_WIRE() && wait > 0 ) wait-- ;
        while( __GET_RED_WIRE() && __GET_WHITE_WIRE() && wait > 0 ) wait-- ;
        */

        /* if we get in some sort of bounce situation, this is no good. */
        if ( wait == 0 ) 
        {

#ifdef __DEBUG_TI_UART0_MSG

            UART0_printf_PGM( 
                    PSTR( "TI_isr(): ERROR: Timeout during debounce.\n" ) );

            byte_num = 0;
#endif            

            s_active_bit = 0;       // Reset the active bit.
            s_transfer_char = 0;    // Clear the byte buffer.
            ti_datalink_abort();    // Abort.

            break;

        } // end if()

        // Wait a bit again, before deciding what to do...
        DELAY_us( 50 );

        /* Check for an error */
        if ( ! ( __GET_RED_WIRE() ) && ! ( __GET_WHITE_WIRE() ) )
        {
        
#ifdef __DEBUG_TI_UART0_MSG

            UART0_printf_PGM( 
                        PSTR( "TI_isr(): ERROR: Simultaneous R/W trip.\n" ) );


            byte_num = 0;

#endif

            s_transfer_char = 0;    // Clear the data byte buffer.
            s_active_bit = 0;       // Reset the active bit.
            ti_datalink_abort();    // Abort.

            break;

        } // end if()

        // IF we get to this point, then we're likely to be able to 
        // 'read in' a 'bit' from the port -- so let's get to it.


        // If the white trips first, then we're expecting a '1'.
        if ( ! ( __GET_WHITE_WIRE() ) )
        {

            s_transfer_char |= 1 << s_active_bit;
            s_active_bit++;
            pin_cmd = WHITE_IN;

        } // end if()

        // Otherwise, if the red wire trips first, we're expecting a '0'.
        else if (! ( __GET_RED_WIRE() ) )
        {

            s_active_bit++;
            pin_cmd = RED_IN;

        } // end else-if()

        // At this point we have to perform a careful sequence of bit
        // acknowledgements to ensure proper reception of the corresponding
        // incoming 'bit'.
        switch( pin_cmd )
        {

            // If the red wire tripped...
            case RED_IN:

                // Acknowledge and pull the WHITE wire LOW.
                SBV( __WHITE_OUT_PIN, __TI_PIN_OUT_PORT );

                DELAY_us( __TI_ACK_DELAY );

                // Wait for the RED wire to go back high.
                while( ! ( __GET_RED_WIRE() ));

                DELAY_us( __TI_ACK_DELAY );

                // Acknowledge again.
                CBV( __WHITE_OUT_PIN, __TI_PIN_OUT_PORT );

                DELAY_us( __TI_ACK_DELAY );

                break;

            // If the white wire tripped...
            case WHITE_IN:

                // Acknowledge and pull the RED wire LOW.
                SBV( __RED_OUT_PIN, __TI_PIN_OUT_PORT );

                DELAY_us( __TI_ACK_DELAY );

                // Wait for the WHITE wire to go back high.
                while( !( __GET_WHITE_WIRE() ));

                DELAY_us( __TI_ACK_DELAY );

                // Acknowledge again.
                CBV( __RED_OUT_PIN, __TI_PIN_OUT_PORT );

                DELAY_us( __TI_ACK_DELAY );

                break;

            default: /* Do nothing. */ ;

        } // end switch()

        // Process 'byte' once we have collected 8-bits.
        if ( s_active_bit > 7 ) 
        {

#ifdef __DEBUG_TI_UART0_DUMP_DATA

            // Stash it in the analysis buffer.
            ti_data_buffer[ byte_num++ ] = s_transfer_char;


#endif
            // Process the current byte...
            end_isr = ti_process_char( s_transfer_char );

            // ...then reset the current active bit, and data byte buffers
            // in order to 'construct' the next byte.
            s_transfer_char = 0;
            s_active_bit = 0;

        } // end if()

    } // end while()

#ifdef __DEBUG_TI_DUMP_TI_PARAMS_STRUCT

    // Print the contents of the data structure.
    for( i = 0; i < TI_params.data_len; ++i )
    {

        // Print it.
        UART0_printf_PGM( PSTR( "{ 0x%02X } [ %d ]\n" ),    
                                            TI_params.data_buffer[ i ],
                                            TI_params.data_buffer[ i ] );

    } // end for()

#endif    

#ifdef __DEBUG_TI_UART0_DUMP_DATA


    // At this point it should be 'safe' to dump the data.
    for( i = 0; i < 128; i++ )
    {

        // Print it.
        UART0_printf_PGM( PSTR( "0x%02X [ %d ]\n" ), ti_data_buffer[ i ],
                                                     ti_data_buffer[ i ] );

        // Clear it.
        ti_data_buffer[ i ] = 0xAA;


    } // end for()

#endif

    // Start listening to pin-change interrupts.
    __TI_port_listen();

#ifdef __DEBUG_TI_UART0_MSG

     UART0_printf_PGM( PSTR( "Out.\n" ) );

#else     

    // Allow for time before we start looking at the line levels.
    DELAY_us( 50 );

#endif

} // end CBOT_ISR( TI_isr )

// -------------------------------------------------------------------------- //
/* inputs:  the character to send
 * outputs: the character if successful, otherwise error code.
 * notes:   The TI protocol says these must be sent LSB first.
 */
void ti_putc(uint8_t ch)
{

    uint8_t i;
    uint8_t isr_stat = PCMSK0;  // Save the current state of interrupts.

    unsigned long int wait = 0x000FFFFF;

    /* disable pin change interrupts. Otherwise we will trigger them. */
    __TI_port_mute();

    for (i = 0; i < 8; i++)
    {

        // First check if there's an error condition (i.e., if 
        // both lines are being pulled LOW.
        if (! ( __GET_RED_WIRE() ) && ! ( __GET_WHITE_WIRE() ) )
        {

#ifdef __DEBUG_TI_UART0_MSG

            UART0_printf_PGM( PSTR( "ERROR: Sending.\n" ) );

#endif

            s_transfer_char = 0;    // Reset the byte data buffer.
            s_active_bit = 0;       // Reset the current 'bit' to zero.
            ti_datalink_abort();    // Abort and start again.

        } // end if()

        // If no error condition then, wait for both lines to go HIGH.
        // We also have to 'debounce' just in case.
        while( ( ! ( __GET_RED_WIRE()) || ! ( __GET_WHITE_WIRE()) ) && 
                                                                  ( wait > 0 ) )
            --wait;


        while( ( ! ( __GET_RED_WIRE()) || ! ( __GET_WHITE_WIRE()) ) && 
                                                                  ( wait > 0 ) )
            --wait;


        while( ( ! ( __GET_RED_WIRE()) || ! ( __GET_WHITE_WIRE()) ) && 
                                                                  ( wait > 0 ) )
            --wait;


        while( ( ! ( __GET_RED_WIRE()) || ! ( __GET_WHITE_WIRE()) ) && 
                                                                  ( wait > 0 ) )
            --wait;

        
        // We need some 'hold time' before we send the next 'bit'. This
        // is a VERY important delay.  Omitting this causes problems!
        // The present delay is the 'sweet spot'.
        DELAY_us( 15 );

        // Check if we have a timeout sending...
        if ( wait == 0 )
        {

#ifdef __DEBUG_TI_UART0_MSG

            UART0_printf_PGM( PSTR( "ERROR: ti_putc() timeout.\n" ) );
#endif

            s_transfer_char = 0;    // Reset the byte data buffer.
            s_active_bit = 0;       // Reset the current 'bit' to zero.
            ti_datalink_abort();    // Abort and start again.

        } // end if()

        if ( ch & ( 1 << i ) )                /* Sending a '1'. */
        {


            // Pull the WHITE wire LOW.
            SBV( __WHITE_OUT_PIN, __TI_PIN_OUT_PORT );

            DELAY_us( __TI_PUTC_DELAY );

            // Wait for RED to acknowledge and pull LOW also.
            while( __GET_RED_WIRE() );

            DELAY_us( __TI_PUTC_DELAY );

            // Let the WHITE wire go HIGH.
            CBV( __WHITE_OUT_PIN, __TI_PIN_OUT_PORT );

            DELAY_us( __TI_PUTC_DELAY );

            // Wait for the RED to acknowledge and go HIGH also.
            while(! ( __GET_RED_WIRE()) );

            DELAY_us( __TI_PUTC_DELAY );

        } // end if()

        else                    /* Sending a '0'. */
        {


            // Pull the RED wire LOW.
            SBV( __RED_OUT_PIN, __TI_PIN_OUT_PORT );

            DELAY_us( __TI_PUTC_DELAY );

            // Wait for WHITE to acknowledge and pull LOW also.
            while( __GET_WHITE_WIRE() );
    
            DELAY_us( __TI_PUTC_DELAY );

            // Let the RED wire to go HIGH.
            CBV( __RED_OUT_PIN, __TI_PIN_OUT_PORT );

            DELAY_us( __TI_PUTC_DELAY );

            // Wait for the WHITE to acknowledge and go HIGH also.
            while(! ( __GET_WHITE_WIRE()) );

            DELAY_us( __TI_PUTC_DELAY );

        } // end else.

    } // end for()

    PCMSK0 = isr_stat;  // Restore the interrupt state.

} // end ti_putc()
