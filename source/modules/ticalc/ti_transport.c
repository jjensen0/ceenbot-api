/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
#include <inttypes.h>
#include <stdio.h>

#include "ti324v221.h"
#include "__ti324v221.h"

#define MAX_NAME_LEN 8

// Function pointers should be initialized to zero for 'safety'.
static void (*s_get_callback)(void) = NULL;
static void (*s_send_callback)(int16_t *list, uint8_t len) = NULL;

/* Arguments for the Send() callback */
static int16_t  s_list[MAX_LIST_LEN]; /* List of numbers from calculator  */
static uint8_t  s_list_len; /* The number of items sent from the calculator */
/* Arguments for the Get() callback */
static int16_t  s_rval;     /* The value to return to the ceenbot.          */
static uint8_t  s_name[MAX_NAME_LEN];  /* Resource name.                    */
static uint8_t  s_name_len; /* Resource name length.                        */

/* State data */
static uint8_t  s_machine_id;   /* machine ID we are spoofing as            */
/* 
 * Listing of packet interactions for different TI requests. NOTE: not all
 * these states are actually used below.  This defines the entire exchange,
 * but only the gets (and SEND_VAR_HDR) are part of the state machine. The
 * sends happen inside of the get states.
 */
enum {  

    GET_REQ,        /* Receives either TI_REQ or TI_RTS packet              */

    /* Get() command states. */
    SEND_REQ_ACK,   /* Acknowledge the TI_REQ                               */
    SEND_VAR_HDR,   /* Send the variable header for the data.               */
    GET_VAR_ACK,    /* Get the TI_ACK for our TI_VAR                        */
    GET_VAR_CTS,    /* Get the TI_CTS for out TI_VAR                        */
    SEND_CTS_ACK,   /* Acknowledge the TI_CTS                               */
    SEND_DATA,      /* Send the variable over                               */
    GET_DATA_ACK,   /* Recv the ack for the data sent.                      */

    /* Send() command states. */
    SEND_RTS_ACK,   /* Acknowledges TI_RTS                                  */
    SEND_CTS,       /* CTS for Calculator's TI_RTS                          */
    GET_CTS_ACK,    /* Receive a TI_ACK for our CTS                         */
    GET_DATA,       /* Receive a list of data from a TI_RTS exchange        */
    SEND_DATA_ACK,  /* Acknowledge the data received.                       */
    GET_EOT,        /* Receive the EOT for the list we received.            */
    SEND_EOT_ACK    /* Acknowledge the EOT we received.                     */

} s_exchange_state = GET_REQ;

/*
 * Process the incoming packet based on the current state.
 */
BOOL ti_process_packet( packet_t *pk )
{

    BOOL end_interrupt = FALSE;  /* Exchange complete result */
    uint8_t i;

    /* debug code */
    if (pk == NULL && s_exchange_state != SEND_VAR_HDR)

        return end_interrupt;

    else if (pk == NULL)

        pk = &g_active_packet;

    /* begin state machine */
    switch (s_exchange_state)
    {
        /*******************************************************************
         * STEP 1: Receive a request.
         */
        case GET_REQ:

            /* determine the machine id that we should be spoofing. */ 
            switch(pk->machine_id)
            {

                case 0x89:

                    s_machine_id = TI89_CBR;

                    break;

                case 0x85:

                    s_machine_id = TI85_CBR;

                    break;

                case 0x86:

                    s_machine_id = TI86_CBR;

                    break;

                case 0x82:  /* this one has the best chance of working.  */

                default:

                    s_machine_id = TI82_CBR;

                    break;

            } // end switch()

            /* determine the request type. */
            switch(pk->command)
            {

                // Got a request to send variable name (TO the calculator).
                case TI_REQ:

                    /* format and send a response packet. */
                    pk->machine_id  = s_machine_id;
                    pk->command     = TI_ACK;
                    pk->len         = 0;

                    ti_send_packet(pk);

                    switch (s_machine_id)
                    {

                        case TI89_CBR:

                            s_name_len = pk->data[5];

                            for(i=0; i<s_name_len && i<MAX_NAME_LEN; ++i)

                                s_name[i] = pk->data[i+6];

                            break;

                        case TI82_CBR:
                        case TI85_CBR:
                        case TI86_CBR:
                        default:

                            s_name_len = 8;
                            for(i=0; i<s_name_len && i<MAX_NAME_LEN; ++i)

                                s_name[i] = pk->data[i+3];

                            break;

                    } // end switch()

                    s_exchange_state = SEND_VAR_HDR;
                    
                    if ( s_get_callback != NULL )

                        s_get_callback();

                    /* 
                     * Do a check to see if we ended up sending the variable
                     * header during the callback. If we did, we need to keep
                     * polling the active wires.  If not, we need to come back
                     * from this interrupt.
                     */
                    if (s_exchange_state == SEND_VAR_HDR)

                        end_interrupt = TRUE;

                    break;

                case TI_VAR:

                // Got a request to send variable name (TO the CEENBoT).
                case TI_RTS:

                    /* format and send a response packet. */
                    pk->machine_id  = s_machine_id;
                    pk->command     = TI_ACK;
                    pk->len         = 0;

                    ti_send_packet( pk );

                    /* format and send a CTS */
                    pk->machine_id  = s_machine_id;
                    pk->command     = TI_CTS;
                    pk->len         = 0;

                    ti_send_packet( pk );

                    s_exchange_state = GET_CTS_ACK;

                    break;

                default:
                    break;

            } // end switch()

            break;
            
        /*******************************************************************
         * Get() call case.
         */

        /* STEP 2: Send the variable header. */
        case SEND_VAR_HDR:

            pk->machine_id = s_machine_id;
            pk->command = TI_VAR;
            
            switch( s_machine_id )
            {

                case TI89_CBR:

                    ti_mkvar(pk, s_name, s_name_len, s_rval);

                    break;

                case TI82_CBR:
                case TI85_CBR:
                case TI86_CBR:

                default:

                    ti_legacy_mkvar(pk, s_name, s_name_len, s_machine_id );

                    break;

            } // end switch()

            ti_send_packet(pk);

            s_exchange_state = GET_VAR_ACK;

            break;

        /* STEP 3: Receive the ACK for the variable header. */
        case GET_VAR_ACK:

            s_exchange_state = GET_VAR_CTS;

            break;

        /* STEP 4: Receive the CTS for the variable header. */
        case GET_VAR_CTS:

            /* format and send a response packet. */
            pk->machine_id  = s_machine_id;
            pk->command     = TI_ACK;
            pk->len         = 0;

            ti_send_packet(pk);

            /* Generate and send the data packet. */
            pk->machine_id  = s_machine_id;
            pk->command     = TI_DATA;

            switch( s_machine_id )
            {

                case TI89_CBR:

                    ti_mkdata(pk, s_rval);

                    break;

                case TI82_CBR:
                case TI85_CBR:
                case TI86_CBR:
                default:

                    ti_legacy_mkdata( pk, s_rval, s_machine_id );

                    break;

            } // end switch()

            ti_send_packet(pk);

            s_exchange_state = GET_DATA_ACK;

            break;

        /* STEP 5: Get the ACK for the data sent */
        case GET_DATA_ACK:

            end_interrupt = TRUE;

            s_exchange_state = GET_REQ;

            break;

        /*******************************************************************
         * Send() call case.
         */
        /* STEP 2: Get Ack for CTS */
        case GET_CTS_ACK:

            s_exchange_state = GET_DATA;

            break;

        /* STEP 3: Get the data */
        case GET_DATA:

            switch ( s_machine_id )
            {

                case TI89_CBR:

                    // Process the packet with data (pk), and store the 
                    // extracted 'arguments' (i.e., numerical values) from the 
                    // calculator in 's_list'.
                    s_list_len = ti_getlist( pk, s_list, MAX_LIST_LEN );

                    break;

                case TI85_CBR:
                case TI86_CBR:
                case TI82_CBR:
                default:

                    // Process the packet with data (pk), and store the 
                    // extracted 'arguments' (i.e., numerical values) from the 
                    // calculator in 's_list'.
                    s_list_len = ti_legacy_getlist( pk, s_list, MAX_LIST_LEN, 
                                                    s_machine_id );

                    break;

            } // end switch()

            /* format and send a response packet. */
            pk->machine_id  = s_machine_id;
            pk->command     = TI_ACK;
            pk->len         = 0;

            ti_send_packet( pk );

            s_exchange_state = GET_EOT;

            break;

        /* STEP 4:  Get the EOT for data. */
        case GET_EOT:

            if ( s_send_callback != NULL )

                s_send_callback(s_list, s_list_len);

            /* format and send a response packet. */
            pk->machine_id  = s_machine_id;
            pk->command     = TI_ACK;
            pk->len         = 0;

            ti_send_packet(pk);

            s_exchange_state = GET_REQ;

            end_interrupt = TRUE;

            break;

        default:

            break;
    }

    return end_interrupt;

} // end ti_process_packet()

/* TI_complete_get_call() - Completes a get call, and gives the value in var
 *                          to the calculator.
 * Input:   The variable to return to the calculator.
 * Notes:   None.
 */
inline void TI_complete_get_call(int16_t var)
{

    s_rval = var;               /* Store off the return data.   */

    ti_process_packet( NULL );  /* Complete the get call.       */

} // end TI_complete_get_call()

/* TI_set_get_callback() - Set callback for calculator 'Get'. 
 * Input:   The callback to use.
 * Notes:   This will not complete the get command until 
 *          TI_complete_get_call() is called.
 */
inline void TI_set_get_callback(void (*callback)(void))
{

    s_get_callback = callback;

} // end TI_set_get_callback()

/* TI_set_send_callback() - Set callback for calculator 'Send'
 * Input: The callback to call after the robot receives the list.
 * Notes: None.
 */
inline void TI_set_send_callback(void (*callback)(int16_t *list, uint8_t len))
{

    s_send_callback = callback;

} // end TI_set_send_callback()

inline void ti_transport_abort()
{

    s_exchange_state = GET_REQ;

} // end ti_transport_abort()
