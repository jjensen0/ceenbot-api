/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
#include <inttypes.h>

#include "ti324v221.h"
#include "__ti324v221.h"

const uint8_t c_short_packets[] = {
    TI_ACK, // 0x56
    TI_CTS, // 0x09
    TI_EOT  // 0x92
};

/* define a structure for the active packet. */

static uint16_t s_packet_idx = 0; /* current byte read from current packet. */
static uint16_t s_checksum;
packet_t        g_active_packet;

/*
 * Process the received character. Here we collect 'bytes' received and
 * determine its part in the 'packet', and fill the current 'packet' 
 * or active packet accordingly as bytes come in.  In other words, the purpose
 * of this function is to collect bytes to form 'the packet' -- nothing else.
 */
inline BOOL ti_process_char(uint8_t ch)
{

    uint8_t i;
    uint16_t data_idx;
    BOOL       end_interrupt = FALSE;

    s_packet_idx++;

    switch (s_packet_idx)
    {

        case 0:

            break;

        case 1:     /* Machine ID */

            g_active_packet.machine_id = ch;
            s_checksum = 0;

            break;

        case 2:     /* Command ID */

            g_active_packet.command = ch;

            break;

        case 3:     /* Length LSB / short command data */

            g_active_packet.len = ch;

            break;

        case 4:     /* Length MSB / short command data */

            g_active_packet.len |= ch << 8;

            for(i = 0; i < sizeof(c_short_packets); ++i)
            {

                // Determine if the command is either TI_ACK, TI_CTS, or TI_EOT.
                if (g_active_packet.command == c_short_packets[i])
                {

                    end_interrupt = ti_process_packet( &g_active_packet );
                    s_packet_idx = 0;

                } // end if()

            } // end f0r()

            break;

        default:    /* Packet Data or Checksum */

            data_idx = s_packet_idx - 5;

            /* Data part of packet. */
            if (data_idx < g_active_packet.len)
            {

                if (data_idx < TI_MAX_PACKET_DATA)
                {

                    g_active_packet.data[ data_idx ] = ch;

                } // end if()

                s_checksum += (uint8_t)ch;

            } // end if()

            /* Checksum LSB */
            else if( data_idx == g_active_packet.len )
            {

                s_checksum -= ch;

            } // end else-if()

            else
            {

                s_checksum -= ch << 8;

                // If all data bytes have been accounted for, then...
                if (s_checksum == 0)

                    // Process the packet.
                    end_interrupt = ti_process_packet( &g_active_packet );

                // Otherwise...
                else
                {

                    // Something went wrong.  Abort -- and end the interrupt
                    // sequence.

                    ti_datalink_abort();
                    end_interrupt = TRUE;

#ifdef __DEBUG_TI_UART0_MSG                    

                    UART0_printf_PGM( 
                            PSTR( "ti_process_char(): Checksum error.\n" ) );

#endif

                } // end else

                s_packet_idx = 0;

            } // end else

            break;

    } // end switch()

    return end_interrupt;

} // end ti_process_char()

/* input:   the command ID, and packet data.
 * output:  none
 * notes:   none
 */
void ti_send_packet(const packet_t *pk)
{

    uint16_t i = 0;
    uint16_t checksum = 0;

    /* Send packet header */
    ti_putc( pk->machine_id );    /* Machine ID */
    ti_putc( pk->command );       /* Command ID */
    ti_putc( ( uint8_t )( pk->len & 0x00FF ) );             /* Length LSB */
    ti_putc( ( uint8_t )( ( pk->len >> 8 ) & 0x00FF ) );    /* Length MSB */

    /* We're done here if this is a short packet. */
    for ( i = 0; i < sizeof( c_short_packets ); ++i )
    {

        // Stop here if command is TI_ACK, TI_CTS, or TI_EOT.
        if ( c_short_packets[ i ] == pk->command )

            return;

    } // end for()

    // Otherwise... we need to send data.

    /* Send data */
    for (i = 0; i < pk->len; i++)
    {

        ti_putc( pk->data[i] );
        checksum += (uint8_t) pk->data[i];

    } // end for()

    /* Send the checksum */
    ti_putc( checksum & 0xff );
    ti_putc( checksum >> 8   );

} // end ti_send_packet()

/* 
 * Aborts the current transfer
 */
void ti_datalink_abort(void)
{

#ifdef __DEBUG_TI_UART0_MSG
    UART0_printf( "ti_datalink_abort(): Aborting.\n" );
#endif

    s_packet_idx = 0;
    ti_transport_abort();

} // end ti_datalink_abort()
