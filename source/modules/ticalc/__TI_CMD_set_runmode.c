/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for '__TI_CMD_set_runmode()' function.

#include "ti324v221.h"
#include "__ticmd324v221.h"

void __TI_CMD_set_runmode( TI_PARAMS *p_TI_params )
{

    LCD_clear();

    // Ensure all neede parameters have arrived.
    if ( p_TI_params->data_len > 1 )
    {

        LCD_printf_PGM( PSTR( "Setting Run Mode.\n" ) );

        if ( p_TI_params->data_buffer[ 1 ] == 0 )

            LCD_printf_PGM( PSTR( "Mode: Free-running.\n" ) );

        else if ( p_TI_params->data_buffer[ 1 ] == 1 )

            LCD_printf_PGM( PSTR( "Mode: Step Mode.\n" ) );

        STEPPER_set_mode( STEPPER_BOTH, 
                ( STEPPER_MODE ) p_TI_params->data_buffer[ 1 ] );

    } // end if()

    else
    {

        __TI_print_param_error_msg();

    } // end else.

    __TI_return( 0 );

} // end __TI_CMD_set_runmode()
