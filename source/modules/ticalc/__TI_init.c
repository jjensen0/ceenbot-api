/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the '__TI_init()' function.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

void __TI_init( void )
{

    // Here we must set up the pins that we're going to use.
    // For the time being, we're locking the I/O pin assignments
    // as follows, since right now this is VERY experimental:
    //
    //  I/O PA4: White-OUT (For outgoing data).
    //  I/O PA5: Red-OUT   (For outgoing data).
    //  I/O PA6: White-IN  (For incoming data).
    //  I/O PA7: Red-IN    (For incoming data).
    //
    // Thus, let us set up those pins:
    SBD( A, __WHITE_OUT_PIN, OUTPIN );
    SBD( A, __RED_OUT_PIN,   OUTPIN );
    SBD( A, __WHITE_IN_PIN,  INPIN );
    SBD( A, __RED_IN_PIN,    INPIN );

    // We need to enable the pullups for the inputs.
    // NOTE: Pull-ups are enabled by writing to 'PORTn' instead of 'PINn'.
    SBV( __WHITE_IN_PIN, __TI_PIN_OUTPORT );
    SBV( __RED_IN_PIN,   __TI_PIN_OUTPORT );

    // Make sure the outpins are set to zero so we don't pull down the line.
    CBV( __RED_OUT_PIN,   __TI_PIN_OUTPORT );
    CBV( __WHITE_OUT_PIN, __TI_PIN_OUTPORT );

    // Finally, we need to enable pin-change interrupt for group PCINT7..0, 
    // which includes the two input pins above PA3, and PA5.
    SBV( PCIE0, PCICR );

    // NOTE:  Once pin-change interrupt is enabled (for any group), they
    //        should NOT be disabled.  Other modules COULD potentially be using
    //        it.  We only want to ensure it is turned ON.  However, we can
    //        individually control the interrupt mask for the pin in question.
    //        THAT, we can [ eventually ] turn OFF via PCMSK0 register.

#ifdef __DEBUG_TI_UART0_MSG

    if ( __GET_RED_WIRE() )

        UART0_printf( "__TI_init(): Red wire is HIGH.\n" );

    else

        UART0_printf( "__TI_init(): Red wire is LOW.\n" );

    if ( __GET_WHITE_WIRE() )

        UART0_printf( "__TI_init(): White wire is HIGH.\n" );

    else

        UART0_printf( "__TI_init(): White wire is LOW.\n" );

#endif

    // That's it.

} // end __TI_init()
