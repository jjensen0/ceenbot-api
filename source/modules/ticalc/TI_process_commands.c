/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'TI_process_commands()' function.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

void TI_process_commands( void )
{

    unsigned short int i;

    // Check to see if there is a command in the buffer waiting to 
    // be dispatched.  If there's nothing to do, then just skip this and
    // do nothing.
    if ( TI_params.pending_dispatch == TRUE )
    {

        // First make sure the length is not zero.
        if ( ( TI_params.data_len > 0 ) &&  
             ( TI_params.data_len <= MAX_LIST_LEN ) )
        {

            // Determine the command and dispatch the appropriate function
            // to execute the requested action.
            switch( TI_params.data_buffer[ 0 ] )
            {

                case __TI_CMD_STOP:     

                    __TI_CMD_stop( &TI_params );       

                    break;

                case __TI_CMD_RUN:      

                    __TI_CMD_run( &TI_params );        

                    break;

                case __TI_CMD_STEPWT:   

                    __TI_CMD_stepwt( &TI_params );     

                    break;

                case __TI_CMD_TANKTURNWT:   

                    __TI_CMD_tank_turnwt( &TI_params );     

                    break;

                case __TI_CMD_RUNANDBUMP:

                    __TI_CMD_run_and_bump( &TI_params );

                    break;

                case __TI_CMD_STEPANDBUMP:

                    __TI_CMD_step_and_bump( &TI_params );

                    break;

                case __TI_CMD_DELAY_MS: 

                    __TI_CMD_delay_ms( &TI_params );   

                    break;

                case __TI_CMD_LED:      

                    __TI_CMD_led( &TI_params );        

                    break;

                case __TI_CMD_SET_ACCEL: 

                    __TI_CMD_set_accel( &TI_params ); 

                    break;

                case __TI_CMD_GET_SWIR:

                    __TI_CMD_get_ir_sw( &TI_params );

                    break;

                case __TI_CMD_SET_SPEED:

                    __TI_CMD_set_speed( &TI_params );

                    break;

                case __TI_CMD_SET_DIR:

                    __TI_CMD_set_dir( &TI_params );

                    break;

                case __TI_CMD_SET_STEPS:

                    __TI_CMD_set_steps( &TI_params );

                    break;

                case __TI_CMD_SET_RUNMODE:

                    __TI_CMD_set_runmode( &TI_params );

                    break;

                case __TI_CMD_SET_RC_SERVO:

                    __TI_CMD_set_RC_servo( &TI_params );

                    break;

                case __TI_CMD_PLAY_BEEP_PATTERN:

                    __TI_CMD_play_beep_pattern( &TI_params );

                    break;

                case __TI_CMD_PING:

                    __TI_CMD_ping( &TI_params );

                    break;

                case __TI_CMD_WAIT_ON_BUMP:

                    __TI_CMD_wait_on_bump( &TI_params );

                    break;

                case __TI_CMD_WAIT_ON_SWITCH:

                    __TI_CMD_wait_on_switch( &TI_params );

                    break;

                case __TI_CMD_TEST:

                    __TI_CMD_test( &TI_params );

                    break;

                default:

                    __TI_CMD_unknown( &TI_params );

            } // end switch()

            // Clean the buffer.
            for ( i = 0; i < MAX_LIST_LEN; i++ )

                TI_params.data_buffer[ i ] = 0;

            // Mark that the command in the buffer as no longer
            // 'pending' to be executed -- otherwise it will retrigger again!
            TI_params.pending_dispatch = FALSE;

        } // end if()

    } // end if()

    // Next check if 'Get' needs to be completed.  If so, return
    // the 'last' return value written by the previous command and reset to
    // zero.
    if ( TI_params.get_called == TRUE )
    {

        TI_Get_return( TI_params.get_return_val );

        TI_params.get_return_val = 0;
        TI_params.get_called     = FALSE;

    } // end if()

} // end TI_process_commands()
