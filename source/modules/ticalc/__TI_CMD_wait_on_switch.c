/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the '__TI_CMD_wait_on_switch()' command.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

void __TI_CMD_wait_on_switch( TI_PARAMS *p_TI_params )
{

    unsigned char sensors      = 0;
    unsigned char which_switch = 0;

    // Proceed if we have the minimum number of required parameters.
    if ( p_TI_params->data_len > 1 )
    {

        LCD_clear();

        LCD_printf_PGM( PSTR( "Waiting for switch...\n" ) );

        // Enter the infinite while loop.
        while( 1 )
        {

            // Read the state of the switches.
            sensors = ATTINY_get_sensors();

            // Determine which switches have been triggered, based on
            // what the user wishes to be notified on...
            switch( p_TI_params->data_buffer[ 1 ] )
            {

                // Check ANY switch.
                case 0:

                    // If S3 is active...
                    if ( sensors & SNSR_SW3_EDGE )

                        // Make a note of it.
                        which_switch = 3;

                    // If S4 is active...
                    else if ( sensors & SNSR_SW4_EDGE )

                        // Make a note of it.
                        which_switch = 4;

                    // If S5 is active...
                    else if( sensors & SNSR_SW5_EDGE )

                        // Make a note of it.
                        which_switch = 5;

                break;

                // Check S3:
                case 3:

                    // If S3 is active...
                    if ( sensors & SNSR_SW3_EDGE )

                        // Make a note of it.
                        which_switch = 3;

                break;

                // Check S4:
                case 4:

                    // If S4 is active...
                    if ( sensors & SNSR_SW4_EDGE )

                        // Make a note of it.
                        which_switch = 4;

                break;

                // Check S5:
                case 5:

                    // If S5 is active...
                    if( sensors & SNSR_SW5_EDGE )

                        // Make a note of it.
                        which_switch = 5;

                break;

                default: /* Nothing to do. */ ;

            } // end switch()

            // IF a switch has been pressed, then we need to get
            // out of this loop.
            if ( which_switch != 0 )
            {

                switch( which_switch )
                {

                    case 3: LCD_printf_PGM( PSTR( "SW3 active!" ) ); break;
                    case 4: LCD_printf_PGM( PSTR( "SW4 active!" ) ); break;
                    case 5: LCD_printf_PGM( PSTR( "SW5 active!" ) ); break;

                    default: /* Nothing to do. */ ;

                } // end switch()

                break;

            } // end if()                

            // Otherwise delay bit (125ms) to check for switch
            // states 8 times per second.
            TMRSRVC_delay( 125 );

        } // end while()

    } // end if()

    else
    {

        __TI_print_param_error_msg();

    } // end else.

    __TI_return( which_switch );

} // end __TI_CMD_wait_on_switch()
