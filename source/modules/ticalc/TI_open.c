/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation module for 'TI_open()' function.

#include "ti324v221.h"
#include "__ticmd324v221.h"

SUBSYS_OPENSTAT TI_open( GET_CALLBACK get_function,
                         SEND_CALLBACK send_function )
{

    SUBSYS_OPENSTAT rval;

    TI_port_status_t port_stat;

    // Assume everything went well.
    rval.subsys = SUBSYS_TI;
    rval.state  = SUBSYS_OPEN;

    // Make sure the ISR module IS open.
    if ( SYS_get_state( SUBSYS_ISR ) == SUBSYS_CLOSED )
    {

        // Mark the error and notify.
        rval.subsys = SUBSYS_ISR;
        rval.state  = SUBSYS_CLOSED;

    } // end if()

    // Make sure the LED module IS open.
    else if ( SYS_get_state( SUBSYS_LED ) == SUBSYS_CLOSED )
    {

        // Mark the error and notify.
        rval.subsys = SUBSYS_LED;
        rval.state  = SUBSYS_CLOSED;

    } // end else-if()

    // Make sure the LCD module IS open.
    else if ( SYS_get_state( SUBSYS_LCD ) == SUBSYS_CLOSED )
    {

        // Mark the error and notify.
        rval.subsys = SUBSYS_LCD;
        rval.state  = SUBSYS_CLOSED;

    } // end else-if()

    // Make sure the STEPPER module IS open.
    else if ( SYS_get_state( SUBSYS_STEPPER ) == SUBSYS_CLOSED )
    {

        // Mark the error and notify.
        rval.subsys = SUBSYS_STEPPER;
        rval.state  = SUBSYS_CLOSED;

    } // end else-if()

    // Otherwise, only do this *IF* the subsystem module is currently closed.
    else if ( SYS_get_state( SUBSYS_TI ) == SUBSYS_CLOSED )
    {

#ifdef __DEBUG_TI_UART0_MSG

        // Open UART0 to send debugging info.
        UART_open( UART_UART0 );

        // Configure the UART0 @ 57600bps.
        UART_configure( UART_UART0, 
                            UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 57600 );

        // Enable it.
        UART_set_TX_state( UART_UART0, UART_ENABLE );

        // Print a message.
        UART0_printf_PGM( 
                        PSTR( "UART0 Started for debugging by TI_open().\n" ) );

        UART0_printf_PGM( PSTR( "TI module is about to opened...\n" ) );
        
#endif

        // Do MCU-specific initialization.
        __TI_init();

        // Register the ISR with the ISR module.
        ISR_attach( ISR_PCINT0_VECT, TI_isr );

        // Register the callback functions -- these should be registered
        // before 'TI_init()'.  If not NULL then register the user's supplied
        // callback function -- otherwise, register the internal callback
        // function.
        if ( send_function != NULL )

            TI_set_send_callback( send_function );

        else

            TI_set_send_callback( __TI_Send );

        // If not NULL, then register the user's supplied callback function --
        // otherwise, register the internal callback function.
        if ( get_function != NULL )

            TI_set_get_callback( get_function );

        else

            TI_set_get_callback( __TI_Get );

        // Initialize the TI module internals.
        port_stat = TI_init();

        // Verify we have established a link with the calculator.
        if ( port_stat == TI_ATTACHED_PORT )
        {

            // Initialize the internal parameters.
            TI_params.data_len = 0;
            TI_params.pending_dispatch = FALSE;
            TI_params.buffer_overrun   = FALSE;
            TI_params.get_called       = FALSE;
            TI_params.get_return_val   = 0;

            // Start listening to pin-change interrupt events.
            __TI_port_listen();

            // Note the TI subsystem module is open for business.
            SYS_set_state( SUBSYS_TI, SUBSYS_OPEN );

#ifdef __DEBUG_TI_UART0_MSG
            
            UART0_printf_PGM( PSTR( "TI module OPEN.\n") );

#endif

        } // end if()

        else
        {

            // TODO: Force a close.

            rval.subsys = SUBSYS_TI;
            rval.state  = SUBSYS_CLOSED;

        } // end else.

        
    } // end if()

    return rval;

} // end TI_open()
