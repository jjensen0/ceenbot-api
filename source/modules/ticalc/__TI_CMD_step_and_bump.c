/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the '__TI_CMD_step_and_bump()' function.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

// Functions:
void __TI_CMD_step_and_bump( TI_PARAMS *p_TI_params )
{

    unsigned char which_IR = 0;
    unsigned char sensors  = 0;

    STEPPER_STEPS steps_remaining;

    LCD_clear();

    // Proceed if we have the minimum number of required parameters.
    if ( p_TI_params->data_len > 4 )
    {

        // Start the motion in non-blocking mode so that we can monitor
        // the IRs once running.
        STEPPER_move_stnb( ( STEPPER_ID )   p_TI_params->data_buffer[ 1 ],
                            
                           // Left motor...
                           ( STEPPER_DIR )        p_TI_params->data_buffer[ 2 ],
                           ( unsigned short int ) p_TI_params->data_buffer[ 4 ],
                           ( unsigned short int ) p_TI_params->data_buffer[ 3 ],
                           STEPPER_params.step_accel.left,
                           STEPPER_BRK_OFF,

                           // Right motor...
                           ( STEPPER_DIR )        p_TI_params->data_buffer[ 2 ],
                           ( unsigned short int ) p_TI_params->data_buffer[ 4 ],
                           ( unsigned short int ) p_TI_params->data_buffer[ 3 ],
                           STEPPER_params.step_accel.right,
                           STEPPER_BRK_OFF );

        LCD_printf_PGM( PSTR( "Waiting for bump...\n" ) );                           

        // Sit in the loop, and keep monitoring bump sensors until 
        // we have exhausted all the remaining steps.
        while( 1 )
        {

            // Get the current number of steps left.
            steps_remaining = STEPPER_get_nSteps();

            // If we have remaining steps...
            if ( steps_remaining.left || steps_remaining.right )
            {

                // Check the sensors...
                sensors = ATTINY_get_sensors();

                // Check if BOTH sensors are triggered.
                if ( ( sensors & SNSR_IR_LEFT ) && 
                     ( sensors & SNSR_IR_RIGHT ) )
                {

                    LCD_printf_PGM( PSTR( "Dual bump hit!\n" ) );

                    which_IR = 3;

                } // end if()                    


                // Otherwise check if just the LEFT is triggered...
                else if ( sensors & SNSR_IR_LEFT )
                {

                    LCD_printf_PGM( PSTR( "LEFT bump hit!\n" ) );

                    which_IR = 1;

                } // end else-if()

                // Otherwise check if just the RIGHT is being triggered...
                else if ( sensors & SNSR_IR_RIGHT )
                {

                    LCD_printf_PGM( PSTR( "RIGHT bump hit!\n" ) );

                    which_IR = 2;

                } // end else-if()

                // If an IR has been triggered, we need to stop.
                if ( which_IR != 0 )
                {

                    // Stop the steppers.
                    STEPPER_stop( STEPPER_BOTH, STEPPER_BRK_OFF );

                    // Get out of the loop!
                    break;

                } // end if()

                // In any case.. wait a bit -- a delay of 125ms will
                // result in IR check 8-times per second.
                TMRSRVC_delay( 125 );

            } // end if()

            // Otherwise...
            else

                // Get out of the loop.
                break;

        } // end while()

    } // end if()

    else
    {

        __TI_print_param_error_msg();

    } // end else.

    LCD_printf_PGM( PSTR( "Done.\n" ) );

    __TI_return( which_IR );

} // end __TI_CMD_step_and_bump()
