/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
#include <inttypes.h>
#include <string.h>

#include "ti324v221.h"
#include "__ti324v221.h"

/* input: the name of the var
 * output: none
 * notes:  82-specific routine for sending data.
 */
void ti_legacy_mkvar( packet_t *pk, uint8_t *var_name, uint8_t var_len, 
                                                        uint8_t ss_machine_id )
{

    uint8_t i;

    /* format the [variable] header name */
    if ( ss_machine_id == TI85_CBR )

        pk->data[0] = 0x0A; // Variable size LSB = 10 bytes.

    else

        pk->data[0] = 0x09; // Variable size LSB = 9 bytes.

    // NOTE: The size above refers to the size of the 'DATA' once its sent,
    //       and it does not refer to the size of the data of *THIS* packet,
    //       and thus, refers to the size of the data of the variable, ONCE
    //       we get to that part!

    pk->data[1] = 0x00; // Variable size MSB.
    pk->data[2] = 0x00; // Type-ID, 0 = Single REAL number.

    // Fill in the variable name ( 8-bytes total ).
    for (i = 0; i < 8; i++)
    {
        pk->data[i+3] = var_name[i];
    }

    // Set the size of the data payload containing variable header info.
    // In this case 11 total.
    pk->len = 11;

} // end ti_legacy_mkvar()

/* input: the var to send
 * output: none
 * notes:  82-specific routine for sending data.
 */
void ti_legacy_mkdata(packet_t *pk, int16_t var, uint8_t ss_machine_id )
{

    uint8_t  mantissa_buf = 0;
    int16_t  places_buf;

    /* format the data */
    /* calculate if negative */
    //
    // Determine if we have to set the 'negative' bit in the 'flags' byte.
    //
    if (var < 0)
    {

        pk->data[ 0 ] = 0x80;   // Set the 'flags' byte.
        var = -var;

    } // end if()
    else

        pk->data[ 0 ] = 0x00;   // Set the 'flags' byte.

    /* Calculate the exponent */
    //
    // Next we calculate the appropriate exponent.
    //

    places_buf    = 1;

    if ( ss_machine_id == TI85_CBR )
    {

        pk->data[ 1 ] = 0x0;    // Reset the exponent field.
        pk->data[ 2 ] = 0x0;    // NOTE: For TI-85, this field is 2-bytes.

    } // end if()        

    else

        pk->data[ 1 ] = 0x0;    // Reset the exponent field.



    while( ( places_buf <= var ) && ( pk->data[ 1 ] < 5 ))
    {

        places_buf *= 10;
        pk->data[ 1 ]++;    // Increment the exponent.

    } // end while()

    places_buf = pk->data[ 1 ]; // Get the exponent.

    if ( ss_machine_id == TI85_CBR )
    {

        pk->data[ 1 ] -= 1;
        pk->data[ 2 ]  = 0xFC;

    
    } // end if()        


    else

        pk->data[ 1 ] += 0x7f;  /* Normalize to 80 */

    /* clear out the mantissa (unrolled) */
    if ( ss_machine_id != TI85_CBR )

        pk->data[2] = 0;

    pk->data[3] = 0;
    pk->data[4] = 0;
    pk->data[5] = 0;
    pk->data[6] = 0;
    pk->data[7] = 0;
    pk->data[8] = 0;

    if ( ss_machine_id == TI85_CBR )

        pk->data[ 9 ] = 0;

    /* 
     * Mantissa stores data as BCD starting at the first nibble.
     * Store data starting at last nibble. put it in backwards.
     */
    while(places_buf > 0)
    {
        places_buf--;
        if (places_buf & 1) {
            mantissa_buf = var % 10;
        }
        else {
            mantissa_buf |= ((var % 10) << 4) & 0xf0;
        }

        if ( ss_machine_id == TI85_CBR )

            pk->data[3 + (places_buf >> 1)] = mantissa_buf;

        else

            pk->data[2 + (places_buf >> 1)] = mantissa_buf;

        var /= 10;
    }

    if ( ss_machine_id == TI85_CBR )

        pk->len = 10;

    else

        pk->len = 9;

} // end ti_legacy_mkdata()


// Desc: This procedure looks at the packet data 'pk', and extract the
//       numerical arguments sent from the calculator and stores (and returns)
//       these values by way of 'list'.
uint8_t ti_legacy_getlist( packet_t *pk, int16_t *list, uint8_t len, 
                           uint8_t ss_machine_id )
{

    uint8_t idx = 0;
    uint8_t i   = 2;

    while ( i < ( pk->len ) && ( idx < len ) )
    {

        BOOL negative = FALSE;
        uint8_t exp = 0;
        uint8_t   j = 0;

        uint16_t exp16 = 0; // NOTE: For the TI-85/86.

        /* initialize buffer */
        list[ idx ] = 0;

        /* check flag for negative */
        //
        // Start by checking the 'FLAG' field, and determine if the
        // numerical value is supposed to be negative by checking the
        // appropriate 'bit'.
        //
        if ( pk->data[ i++ ] & 0x80 )

            negative = TRUE;

        /* get exponent. */
        //
        // The next byte is the exponent.
        //
        if ( ( ss_machine_id == TI85_CBR ) || ( ss_machine_id == TI86_CBR ) )
        {

            exp16  =   pk->data[ i++ ];            // Get exponent LSB.
            exp16 |= ( pk->data[ i++ ] ) << 8;     // Get exponent MSB.

        } // end if()

        else

            exp = pk->data[ i++ ];

#ifdef __DEBUG_TI_DUMP_TI_PARAMS_STRUCT
    
        UART0_printf_PGM( PSTR( "exp16 = 0x%04X\n" ), exp16 );

#endif

        /* get mantissa. */
        //
        // The next seven bytes represent the mantissa.
        //
        for ( j=0; j<7; j++ )
        {

            if ( ( ss_machine_id == TI85_CBR ) ||
                 ( ss_machine_id == TI86_CBR ) )
            {

                if ( exp16 >= 0xFC00 ) {

                    list[idx] *= 10;
                    list[idx] += ( uint8_t )( pk->data[i] & 0xf0 ) >> 4;
                    exp16--;

                } // end if()

                if ( exp16 >= 0xFC00 ) {

                    list[idx] *= 10;
                    list[idx] += ( uint8_t )( pk->data[i] & 0x0f );
                    exp16--;

                } // end if()

            } // end if()

            else
            {

                if ( exp >= 0x80 ) {

                    list[idx] *= 10;
                    list[idx] += ( uint8_t )( pk->data[i] & 0xf0 ) >> 4;
                    exp--;

                } // end if()

                if ( exp >= 0x80 ) {

                    list[idx] *= 10;
                    list[idx] += ( uint8_t )( pk->data[i] & 0x0f );
                    exp--;

                } // end if()

            } // end else.

            i++;

        } // end for()

        /* switch signs if negative */
        if (negative) 
        {
            list[idx] = -list[idx];
        }

        idx++;

    } // end while()

    return idx;

} // end ti_legacy_getlist()
