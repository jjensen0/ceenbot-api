/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for '__TI_CMD_tank_turnwt()' function.

#include "ti324v221.h"
#include "__ticmd324v221.h"

// FIXME:  I really don't like that we're accessing the 'STEPPER_params' 
//         variables directly -- we need to add an 'accessor' function to 
//         obtain the acceleration instead.

void __TI_CMD_tank_turnwt( TI_PARAMS *p_TI_params )
{

    LCD_clear();

    // We need to make sure all the parameters are here...

    if ( p_TI_params->data_len > 4 )
    {

        switch( p_TI_params->data_buffer[ 1 ] )
        {

            case 0:     // Turn LEFT.

                LCD_printf_PGM( PSTR( "Turning LEFT.\n" ) );
                LCD_printf_PGM( PSTR( "Speed: %d stps/sec\n" ), 
                                        p_TI_params->data_buffer[ 3 ] );

                LCD_printf_PGM( PSTR( "Dist: %d steps\n" ), 
                                        p_TI_params->data_buffer[ 2 ] );

                STEPPER_move_stwt( STEPPER_BOTH, 

                        STEPPER_REV,                       // Direction (L).
                        p_TI_params->data_buffer[ 2 ],     // Steps (L).
                        p_TI_params->data_buffer[ 3 ],     // Speed (L).
                        STEPPER_params.step_accel.left,    // Accel (L).
                        p_TI_params->data_buffer[ 4 ],     // Brk-mode (L).


                        STEPPER_FWD,                       // Direction (R).
                        p_TI_params->data_buffer[ 2 ],     // Steps (R).
                        p_TI_params->data_buffer[ 3 ],     // Speed (R).
                        STEPPER_params.step_accel.right,   // Accel (R).
                        p_TI_params->data_buffer[ 4 ] );   // Brk-mode (R).

                break;

            case 1:     // Turn RIGHT.

                LCD_printf_PGM( PSTR( "Turning RIGHT.\n" ) );
                LCD_printf_PGM( PSTR( "Speed: %d stps/sec\n" ), 
                                        p_TI_params->data_buffer[ 3 ] );

                LCD_printf_PGM( PSTR( "Dist: %d steps\n" ), 
                                        p_TI_params->data_buffer[ 2 ] );
                STEPPER_move_stwt( STEPPER_BOTH, 

                        STEPPER_FWD,                       // Direction (L).
                        p_TI_params->data_buffer[ 2 ],     // Steps (L).
                        p_TI_params->data_buffer[ 3 ],     // Speed (L).
                        STEPPER_params.step_accel.left,    // Accel (L).
                        p_TI_params->data_buffer[ 4 ],     // Brk-mode (L).


                        STEPPER_REV,                       // Direction (R).
                        p_TI_params->data_buffer[ 2 ],     // Steps (R).
                        p_TI_params->data_buffer[ 3 ],     // Speed (R).
                        STEPPER_params.step_accel.right,   // Accel (R).
                        p_TI_params->data_buffer[ 4 ] );   // Brk-mode (R).

                break;

            default: /* Do nothing. */ ;

        } // end switch()

    } // end if()
    
    else
    {

        __TI_print_param_error_msg();

    } // end else.

    LCD_printf_PGM( PSTR( "Done.\n" ) );

    __TI_return( 0 );

} // end __TI_CMD_turnwt()
