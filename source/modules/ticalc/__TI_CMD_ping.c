/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for '__TI_CMD_ping()' function.

#include "ti324v221.h"
#include "__ti324v221.h"
#include "__ticmd324v221.h"

void __TI_CMD_ping( TI_PARAMS *p_TI_params )
{

    SUBSYS_OPENSTAT ops_swatch;
    SUBSYS_OPENSTAT ops_usonic;

    unsigned long int usonic_time_us = 0;
    SWTIME            usonic_time_ticks = 0;
    float             distance_cm = 0;

    unsigned short int i = 0;
    unsigned short int distance_cm_int = 0;

    LCD_clear();

    // First determine if we have non-zero sample numbers.  If not,
    // just skip over this whole thing.
    if( p_TI_params->data_buffer[ 1 ] != 0 )
    {

        // Open the STOPWATCH and the USONIC modules.
        ops_swatch = STOPWATCH_open();
        ops_usonic = USONIC_open();

        LCD_printf_PGM( PSTR( "USONIC Ping:\n" ) );
        LCD_printf_PGM( PSTR( "Samples = %d\n" ), 
                                    p_TI_params->data_buffer[ 1 ] );

        // Only continue if these modules successfully opened.
        if( ( ops_swatch.state == SUBSYS_OPEN ) &&
            ( ops_usonic.state == SUBSYS_OPEN ) )
        {

            for( i = 0; i < ( p_TI_params->data_buffer[ 1 ] ) ; ++i )
            {

                // Ping once.
                usonic_time_ticks = USONIC_ping();

                DELAY_ms( 10 );

                // Convert to 'us'.
                usonic_time_us = 
                            10 * (( unsigned long int )( usonic_time_ticks ));

                // Convert (and add) to 'cm'.
                distance_cm += ( 0.01724 * usonic_time_us );

            } // end for()

            // Compute average.
            distance_cm /= ( float )( p_TI_params->data_buffer[ 1 ] );

            LCD_printf_PGM( PSTR( "  Dist = %.1f cm\n" ), distance_cm );

            // Scale by 10.
            distance_cm *= 10;

            // Close the SWATCH and USONIC modules.
            USONIC_close();
            STOPWATCH_close();

        } // end if()

    } // end if()

    else
    {

        distance_cm = 0;

        LCD_printf_PGM( PSTR( "  Dist = ???\n" ) );

    } // end if()

    // Typecast down from float to integer.
    distance_cm_int = ( unsigned short int ) distance_cm;        

    LCD_printf_PGM( PSTR( "Done!\n" ) );

    __TI_return( distance_cm_int );

} // end __TI_CMD_ping()
