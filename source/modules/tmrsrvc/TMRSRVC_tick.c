/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'TMRSRVC_tick()' function.

#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"

TMRTICK_RESULT TMRSRVC_tick( void )
{

    TMRPROC_RESULT proc_result;

    // Set up the default return value.
    TMRTICK_RESULT rval = TMRTICK_OK;

    // Check if the system is open.
    if ( SYS_get_state( SUBSYS_TMRSRVC ) == SUBSYS_CLOSED )

        // Let the user know the timer service is not open.
        rval = TMRTICK_NOT_OPEN;

    // Check if the system is running.
    else if ( timer_run_status == TMR_STOPPED )

        // Let the user know the timer service is stopped.
        rval = TMRTICK_STOPPED;

    // Otherwise, no problems, go ahead and 'tick'.
    else
    {


        // Process timer objects only if the timer service is not busy
        // inserting new timer object nodes.
        if ( timer_busy_status == TMR_NOT_BSY )
        {

            // Process ALL timer objects that have expired clocks.
            do 
            {

                proc_result = TMRSRVC_process();

            } while ( proc_result == TMRPROC_NOT_DONE );

        } // end if()

        // Otherwise, indicate this and leave (do nothing).
        else
        {

            // Let the user know the timer service is busy and was not
            // able to 'tick' (this is a 'tick-miss').
            rval = TMRTICK_BUSY;

        } // end else.

    } // end else.

    return rval;
    
} // end TMRSRVC_tick()
