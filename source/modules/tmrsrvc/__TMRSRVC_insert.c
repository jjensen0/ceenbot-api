/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for '__TMRSRVC_insert()' function.

#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"

void TMRSRVC_insert( TMRNODE *pNode )
{

    unsigned int i;
    unsigned char inserted = 0;
    TMRNODE *pCurr = 0;         // Current node being traversed.
    TMRNODE *pPrev = 0;         // Previous node that was traversed.

    // TIMER16 current_time;
    signed int residual_delta;

    // Start by determining if the list is currently empty.  If it is,
    // then insert it as the first of list.  This is the BEST case scenario.
    if ( tmrNodes.nNodes == 0 )
    {

        tmrNodes.pNodeList = pNode;

        // Now that we have nodes to process, we can let the timer run.
        TMRSRVC_start();


    } // end if()

    // Otherwise, we're gonna' have to 'walk' the list to find the appropriate
    // placement position in it.
    else
    {

        residual_delta = 0;

        // Start by getting the first node.
        pCurr = tmrNodes.pNodeList;

        for( i = 0 ; i < tmrNodes.nNodes ; ++i )
        {

            // Compute the first 'residual delta'.
            residual_delta = ( signed int )
                ( pNode->pTimerObj->ticks - pCurr->pTimerObj->ticks );

            // If negative, then this is 'prior' time from the current spot ( or
            // node ).
            if ( residual_delta < 0 )
            {

                // Adjust the time of the current node.
                pCurr->pTimerObj->ticks = 
                    pCurr->pTimerObj->ticks - pNode->pTimerObj->ticks;


                // Insert in front of this node.
                pNode->pNextNode = pCurr;

                // IF this is the first node, then make this node the new 
                // 'top-of-list' node.
                if ( i == 0 )

                    tmrNodes.pNodeList = pNode;

                // Otherwise, we connect this chain to the previous one.
                else

                    pPrev->pNextNode = pNode;

                // Set indicator flag that the node has been successfully
                // inserted.
                inserted = 1;

                // We're done here, get out of 'for()' loop.
                break;

            } // end if()

            // Else if the residual delta is zero, then we insert 'zero'
            // time after the current spot (or node).
            else if ( residual_delta == 0 )
            {

                // Set time of node to be inserted to zero.
                pNode->pTimerObj->ticks = 0;

                // Insert this 'behind' the current node, but be careful
                // not to break the 'chain' of the list.
                pNode->pNextNode = pCurr->pNextNode;

                // Restore the 'linkage'.
                pCurr->pNextNode = pNode;

                // Set indicator flag that the node has been successfully
                // inserted.
                inserted = 1;

                // We're done here, get out of 'for()' loop.
                break;

            } // end else-if()

            // Update the remaining time.
            pNode->pTimerObj->ticks = residual_delta;

            // Move on to the next node in the list if one exists.
            if ( pCurr->pNextNode != NULL )
            {

                // Save the previous node.
                pPrev = pCurr;

                // Make the next node the curret node.
                pCurr = pCurr->pNextNode;


            }

            // Otherwise, get out of the 'for()' loop since there are
            // no more nodes to process.
            else

                break;

        } // end for()

        // If we're here and at this point nothing has been inserted, 
        // then we can place the current timer at the end of the list.
        if ( !inserted )
        {

            // Adjust by '1' less.
            // -- ( pNode->pTimerObj->ticks );

            // Insert at the end of the list.  Note that 'pCurr' already
            // has the last node on the list.
            pCurr->pNextNode = pNode;

        } // end if()

    } // end else.

    // Increment node count.
    ++tmrNodes.nNodes;

} // end TMRSRVC_insert()
