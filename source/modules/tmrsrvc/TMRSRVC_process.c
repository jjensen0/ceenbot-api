/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'TMRSRVC_process()' function.

#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"

TMRPROC_RESULT TMRSRVC_process( void )
{

    TMRNODE  *pTopNode = 0;
    TIMEROBJ *pTopTimeObj = 0;

    // Assume only one node will be processed.
    TMRPROC_RESULT rval = TMRPROC_DONE;

    // Only do anything if there are nodes in the list.
    if ( tmrNodes.nNodes != 0 )
    {

        // Get the top node.
        pTopNode = tmrNodes.pNodeList;

        // Get the top time object.
        pTopTimeObj = pTopNode->pTimerObj;

        // Decrement the timer (head node) on the list.
        -- ( pTopTimeObj->ticks );

        // Check if we have reached TC (terminal count).  If so, we
        // need to process any events (notify function or notify flag).
        if ( pTopTimeObj->ticks <= 0 )
        {
            
            // *** Notify Events ***

            // If the user has validated flag notification, then notify
            // the given flag by updating it to non-zero value.
            if ( pTopTimeObj->flags & TMRFLG_FLAGNOTIFY )
            {

                /* FIXME: Remove this.
                if ( *( pTopTimeObj->pNotifyFlag ) == 0 )

                    // Set the flag.
                    *( pTopTimeObj->pNotifyFlag ) = 1;

                */

                // Set the flag to indicate 'Terminal Count' or 'TC'.
                if ( pTopTimeObj->tc == 0 )

                    pTopTimeObj->tc = 1;

            } // end if()

            // If the user has validated function notification, then
            // notify that function by 'dispatching it'.
            //
            // FIXME: This needs to be dispatched instead of invoked by 
            //        a task scheduler.  For the time being it is handled
            //        here.

            if ( pTopTimeObj->flags & TMRFLG_FUNCNOTIFY )
            {

                pTopTimeObj->pNotifyFunc();

            } // end if()

            // Next, we need to see if need to destroy the node object
            // or restart the timer again depending on the restart mode.
            // Either way, we need to remove this node from the top of the list.

            // Now, if there are two or more nodes, then we make the 'next'
            // node the top node or 'head' node.
            if ( tmrNodes.nNodes > 1 )
            {

                // Set the next node to be the 'head' node.
                tmrNodes.pNodeList = pTopNode->pNextNode;

                // The the 'next node' pointer of the 'detached' node to NULL.
                pTopNode->pNextNode = NULL;

                // And while we're at it, determine if it needs to be 
                // immediately processed as well.
                if ( pTopNode->pNextNode->pTimerObj->ticks <= 0 )

                    rval = TMRPROC_NOT_DONE;

            } // end if()

            // Otherwise set the top of the list to NULL.
            else

                tmrNodes.pNodeList = NULL;


            // Either way, decrement the node count since we've lost a node.
            --tmrNodes.nNodes;

            // If this is the last timer object node, then there's no
            // need to keep 'ticking' so stop the timer until more timer
            // requests arrive.
            if ( tmrNodes.nNodes == 0 )

                TMRSRVC_stop();

            // Check if we need to restart this timer again.
            if ( pTopTimeObj->flags & TMRFLG_RESTART )
            {

                // Reset the request time.
                pTopTimeObj->ticks = pTopTimeObj->timeReq;

                // The 'flags' and mode will remain the same so just
                // insert it.
                TMRSRVC_insert( pTopNode );

            } // end if()

            // Otherwise, we no longer need this timer object -- destroy it.
            else
            {

                // Destroy the object.
                free( pTopNode );

                pTopNode = NULL;

                // Set the 'pending removal 'bit' to zero for cases
                // where the user requested to stop the timer via
                // 'TMRSRVC_stop_timer()' function.
                CBV( 4, pTopTimeObj->flags ); 

            } // end else.

        } // end if()

    } // end if()

    return rval;

} // end TMRSRVC_process()
