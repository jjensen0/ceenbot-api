/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'TMRSRVC_delay()' function.

#include "tmrsrvc324v221.h"
#include "__tmrsrvc324v221.h"


TMRNEW_RESULT TMRSRVC_delay( TIMER16 delay_ms )
{

    TMRNEW_RESULT rval;

#ifndef __NO_DELAYS

    TIMEROBJ delay_timer;   // NOTE: It's okay for this timer object to not
                            //       be declared as 'static' since the function
                            //       will 'busy-wait' until the terminal count
                            //       is reached while in the context of this
                            //       function at which point it is no longer
                            //       needed.  This also allows multiples 
                            //       contexts of the 'delay' to be executed
                            //       concurrently.

    // TIMERFLG delay_flag;

    // Attach the notify flag.
    // delay_timer.pNotifyFlag = &delay_flag;

    // Start the timer.
    rval = TMRSRVC_new( &delay_timer, TMRFLG_FLAGNOTIFY, 
                        TMR_TCM_RUNONCE, delay_ms );

    // 'busy-wait' until terminal count is reached -- that is until the
    // 'tc' flag is set.
    if ( rval == TMRNEW_OK ) {

        while( !delay_timer.tc );

    } // end if()

#else

    rval = TMRNEW_OK;

#endif

    return rval;

} // end TMRSRVC_delay()
