/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'SPI_set_slave_addr()' function.

#include "spi324v221.h"

void SPI_set_slave_addr( SPI_SSADDR slaveSelectAddr )
{

    if ( SYS_get_state( SUBSYS_SPI ) == SUBSYS_OPEN )
    {

        // Enforce address limits.  Only values 0-7 are valid.
        slaveSelectAddr &= 0x07;

        // Next, only change SPI addresses if the new address is different
        // than the previous one.
        if ( slaveSelectAddr != curr_spi_addr )
        {

            // Save it.
            curr_spi_addr = slaveSelectAddr;

            // Set the slave address.
            PORTB = ( PORTB & 0xF8 ) | slaveSelectAddr;

            // Because different devices need different SPI configurations,
            // we need to set the configuration depending on the device.
            switch( slaveSelectAddr )
            {

                // *** Assigned SPI Devices ***
                //
                // Configure SPI for PSXC communication.
                case SPI_ADDR_PSXC:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_PSXC ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_PSXC ]();

                    break;

                // Configure SPI for LCD communication.
                case SPI_ADDR_LCD:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_LCD ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_LCD ]();

                    break;

                // Configure SPI for ATTiny communication.
                case SPI_ADDR_ATTINY0:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_ATTINY0 ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_ATTINY0 ]();

                    break;

                // Configure SPI for SPI-FLASH communication.
                case SPI_ADDR_SPIFLASH:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_SPIFLASH ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_SPIFLASH ]();

                    break;

                // *** Non-Assigned (User) SPI Devices ***
                //
                case SPI_ADDR_SMARTDEV0:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_SMARTDEV0 ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_SMARTDEV0 ]();

                    break;

                case SPI_ADDR_SMARTDEV1:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_SMARTDEV1 ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_SMARTDEV1 ]();

                    break;

                case SPI_ADDR_SMARTDEV2:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_SMARTDEV2 ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_SMARTDEV2 ]();

                    break;

                default:

                    // Only invoke config function if it's safe.
                    if ( spi_config_func_LUT[ SPI_ADDR_NA ] != 0 )

                        spi_config_func_LUT[ SPI_ADDR_NA ]();

            } // end switch()

        } // end if ( different SPI address )

    } // end if( OPEN )

} // end SPI_set_slave_addr()
