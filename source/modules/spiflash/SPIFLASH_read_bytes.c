/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'SPIFLASH_read_bytes()' function.

#include "spiflash324v221.h"
#include "__spiflash324v221.h"

void SPIFLASH_read_bytes( unsigned long int start_addr, 
                          unsigned char *pDest, unsigned short int size )
{

    unsigned short int i = 0;

    // Set the slave address to mark message separator.
    SPI_set_slave_addr( SPI_ADDR_NA );

    // Indicate we wish to read data ( send opcode )
    SPI_transmit( SPI_ADDR_SPIFLASH, __SPIF_OPC_CMD_READ_ARRAY );

    // Send the address bytes...
    SPIFLASH_send_addr( start_addr );

    // Send 'dummy byte'.
    SPI_transmit( SPI_ADDR_SPIFLASH, 0x00 );

    // Read the data...
    for( i = 0; i < size; ++i )

        pDest[ i ] = SPI_receive( SPI_ADDR_SPIFLASH, SPI_NULL_DATA );

    // Select null device.
    SPI_set_slave_addr( SPI_ADDR_NA );

    // Wait a bit.
    DELAY_us( 1 );

} // end SPIFLASH_read_bytes()
