/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for the 'SPIFLASH_block_erase_32K()' function.

#include "spiflash324v221.h"
#include "__spiflash324v221.h"

void SPIFLASH_block_erase_32K( unsigned short int which_block )
{

    // Make sure we're within the possible number of blocks -- otherwise,
    // do nothing.
    if ( which_block < __SPIF_MAX_32K_BLOCKS )
    {

        // Remove protection...
        SPIFLASH_global_unprotect();

        // Enable writing...
        SPIFLASH_write_enable();

        // Tell the SPI-FLASH we wish to erase a 32K block.
        SPI_transmit( SPI_ADDR_SPIFLASH, __SPIF_OPC_CMD_BLOCK_ERASE_32K );

        // Transmit the address.
        SPIFLASH_send_addr( ( which_block * __SPIF_32K_BYTES ) );

        // Pull the chip-select line HIGH to allow the command to take effect.
        SPI_set_slave_addr( SPI_ADDR_NA );

        // Wait a bit...
        DELAY_us( 1 );

        // Wait until the chip is done doing it's tha'ng.
        SPIFLASH_IsBusy();

        // Turn protection back on.
        SPIFLASH_global_protect();

    } // end if()

} // end SPIFLASH_block_erase_32K()
