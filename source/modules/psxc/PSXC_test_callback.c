/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'PSXC_test_callback()' function.  It is 
//       implemented as a 'TIMER EVENT' (or Timer Notification Routine) using
//       the appropriate macro via the timer service module.

#include "psxc324v221.h"
#include "__psxc324v221.h"

TMR_NR( PSXC_test_callback )
{

    // Variable for holding controller data from the PSX controller.
    static PSXC_STDATA controller;

    signed char up, down, left, right;
    signed char tri, cir, x, sqr;
    signed char L1, R1, L2, R2;
    signed char start, select;

    // Poll the controller for data.
    PSXC_read( &controller );

    // Set display line to first.
    LCD_set_next_PGC( 3, 0 );

    // Display controller mode.
    if ( controller.data_type == PSXC_ANALOG )

        LCD_printf( "Mode:ANALOG\t" );

    else if ( controller.data_type == PSXC_DIGITAL )
    {

        LCD_printf( "Mode:DIGITAL\t" );

        // Clear the line below.
        LCD_set_next_PGC( 2, 0 );
        LCD_printf( "\t" );

        // Move to the next line down and clear that line as well.
        LCD_set_next_PGC( 1, 0 );
        LCD_printf( "\t" );

    } // end else-if()

    else
    {

        LCD_printf( "Mode:INVALID\t" );

        // Print error.
        LCD_set_next_PGC( 2, 0 );
        LCD_printf( " ** Disconnected? **\t" );

        LCD_set_next_PGC( 1, 0 );
        LCD_printf( "\t" );

    } // end else.

    // Acquire Digital Pad Data:
    if ( controller.buttons0 & DPAD_UP_BIT ) up   = 1; else up   = 0;
    if ( controller.buttons0 & DPAD_DN_BIT ) down = 1; else down = 0;
    if ( controller.buttons0 & DPAD_LT_BIT ) left = 1; else left = 0;
    if ( controller.buttons0 & DPAD_RT_BIT ) right = 1; else right = 0;

    LCD_set_next_PGC( 3, 80 );

    // Display digital pad data.
    LCD_printf( "+DP:%d%d%d%d", up, down, left, right );

    // Analog data showcase.
    if ( controller.data_type == PSXC_ANALOG )
    {

        // Set to second display line.
        LCD_set_next_PGC( 2, 0 );

        // Display left stick analog data.
        if ( controller.buttons0 & L3_BIT )

            //LCD_printf( "L: U/D=0x%2x  L/R=0x%2x\t", 
            LCD_printf( "L: U/D=%4d  L/R=%4d\t", 

                                            controller.left_joy.up_down,
                                            controller.left_joy.left_right );
        else

            LCD_printf( "L: L3 PUSH\t" );

        // Move to the next line down.
        LCD_set_next_PGC( 1, 0 );

        // Display right stick analog data.
        if ( controller.buttons0 & R3_BIT )

            //LCD_printf( "R: U/D=0x%2x  L/R=0x%2x\t", 
            LCD_printf( "R: U/D=%4d  L/R=%4d\t", 

                                            controller.right_joy.up_down,
                                            controller.right_joy.left_right );
        else

            LCD_printf( "R: R3 PUSH\t" );

    } // end if()

    // Display button info in any mode regardless.
    LCD_set_next_PGC( 0, 0 );

    // Buttons.
    if ( controller.buttons1 & TRI_BIT ) tri = 1; else tri = 0;
    if ( controller.buttons1 & CIR_BIT ) cir = 1; else cir = 0;
    if ( controller.buttons1 & X_BIT )     x = 1; else   x = 0;
    if ( controller.buttons1 & SQR_BIT ) sqr = 1; else sqr = 0;

    // Shoulder:
    if ( controller.buttons1 & L1_BIT )  L1 = 1; else L1 = 0;
    if ( controller.buttons1 & R1_BIT )  R1 = 1; else R1 = 0;
    if ( controller.buttons1 & L2_BIT )  L2 = 1; else L2 = 0;
    if ( controller.buttons1 & R2_BIT )  R2 = 1; else R2 = 0;

    // Start/Select.
    if ( controller.buttons0 & SLCT_BIT ) select = 1; else select = 0;
    if ( controller.buttons0 & STRT_BIT ) start = 1;  else start  = 0;

    LCD_printf( "B:%d%d%d%d SH:%d%d%d%d SS:%d%d\t", tri, cir, x, sqr,
                                                    L1, R1, L2, R2,
                                                    select, start );



} // end PSXC_test_callback()
