/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file 'PSXC_run_test()' function.

#include "psxc324v221.h"
#include "__psxc324v221.h"

SUBSYS_OPENSTAT PSXC_run_test( void )
{

    SUBSYS_OPENSTAT rval;

    // Set up default values to assume no errors were encountered.
    rval.state  = SUBSYS_OPEN;
    rval.subsys = SUBSYS_PSXC;

    // First check that the timing services is available.
    if ( SYS_get_state( SUBSYS_TMRSRVC ) == SUBSYS_CLOSED )
    {

        rval.state  = SUBSYS_CLOSED;
        rval.subsys = SUBSYS_TMRSRVC;

    } // end if()

    // Then check the LCD is available.
    else if ( SYS_get_state( SUBSYS_LCD ) == SUBSYS_CLOSED )
    {

        rval.state  = SUBSYS_CLOSED;
        rval.subsys = SUBSYS_LCD;

    } // end else-if()

    // Finally, check that the PSX subsystem is running.
    else if ( SYS_get_state( SUBSYS_PSXC ) == SUBSYS_CLOSED )
    {

        rval.state  = SUBSYS_CLOSED;
        rval.subsys = SUBSYS_PSXC;

    } // end else-if()

    // If up to this point, we're ready to go.
    else
    {

        // Prepare the private timer object.
        psxc_test_timer.pNotifyFunc = PSXC_test_callback;

        // Start the test at the desired interval.
        TMRSRVC_new( &psxc_test_timer, TMRFLG_FUNCNOTIFY, 
                                        TMR_TCM_RESTART, PSXC_TEST_INTERVAL );
    } // end else.

    return rval;

} // end PSXC_run_test()
