/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'PSXC_plinear_map()' function.

#include "psxc324v221.h"

signed short int PSXC_plinear_map( signed char analog_val,
                                   signed char min_in_val,
                                   signed char max_in_val,
                                   signed short int max_out_val )
{

    signed short int y, x;

    signed short int g;  // Map scaling factor.

    // Make sure input parameters are positive.
    if ( min_in_val < 0 )  min_in_val  *= -1;
    if ( max_in_val < 0 )  max_in_val  *= -1;
    if ( max_out_val < 0 ) max_out_val *= -1;

    // Compute difference and make sure they're not equal.
    x = max_in_val - min_in_val;

    // Compute the mapping scaling factor.
    if ( x != 0 )

        g = ( signed short int )( max_out_val / ( max_in_val - min_in_val ));

    else

        g = 1;

    // Remap.
    if ( analog_val >= max_in_val )

        y = ( signed short int )max_out_val;

    else if ( analog_val <= -1*max_in_val )

        y = -1*max_out_val;

    else if ( ( analog_val >= -1*min_in_val ) && ( analog_val <= min_in_val ) )

        y = ( signed short int )0;

    
    else
    {

        // Map according to sign.
        if ( analog_val > 0 )
        {

            y = ( signed short int )( g*( analog_val - min_in_val ) );

        } // end if()

        else
        {

            y = ( signed short int )( g*( analog_val + min_in_val ) );

        } // end else.

    } // end else.

    return y;
    
} // end PSXC_linear_map()
