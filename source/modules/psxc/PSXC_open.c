/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Implementation file for 'PSXC_open()' function.

#include "psxc324v221.h"

SUBSYS_OPENSTAT PSXC_open( void )
{

    SUBSYS_OPENSTAT rval;

    // Set up default values to assume no errors were encountered.
    rval.state  = SUBSYS_OPEN;
    rval.subsys = SUBSYS_PSXC;

    // First check the SPI subsystem is available.
    if ( SYS_get_state( SUBSYS_SPI ) == SUBSYS_CLOSED )
    {

        rval.state  = SUBSYS_CLOSED;
        rval.subsys = SUBSYS_SPI;

    } // end if()

    // Otherwise if no problems, then proceed.
    else
    {

        // Invalidate the 'center' parameters.
        PSXC_params.center.valid = FALSE;

        // Set the default analog joystick 'idle' center values.
        PSXC_params.center.left_joy.up_down     = 0x80;
        PSXC_params.center.left_joy.left_right  = 0x80;
        PSXC_params.center.right_joy.up_down    = 0x80;
        PSXC_params.center.right_joy.left_right = 0x80;
        
        // The system is open and it can be put to use.
        SYS_set_state( SUBSYS_PSXC, SUBSYS_OPEN );

    } // end if()

    return rval;

} // end PSXC_open()
