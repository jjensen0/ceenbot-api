// Desc: Sample program to test out the TI subsystem module.  It is not meant
//       to be a comprehensive sample program.  It uses the UART to send
//       text messages to a terminal PC for debugging purposes.

#include "capi324v221.h"

// ============================ prototypes ================================== //
TIMER_EVENT( led_toggle );
void TI_Get( void );
void TI_Send( int16_t *list, uint8_t len );
// ============================  functions ================================== //
TIMER_EVENT( led_toggle )
{

	// Just toggle the Green LED so we know the system is stable.
	LED_toggle( LED_Green );

}
// -------------------------------------------------------------------------- //
void TI_Get( void )
{

    UART0_printf( "TI_Get(): Get function called.\n" );

	TI_Get_return( 0 ); // Finish servicing this call and return the value.

} // end TI_Get()
// -------------------------------------------------------------------------- //
void TI_Send( int16_t *list, uint8_t len )
{

	uint8_t i;

    UART0_printf( "TI_Send(): Set function called.\n" );

	UART0_printf( "TI_Send(): Array length = %d\n", len );

	for( i = 0 ; i < len ; i++ )

		UART0_printf( "TI_Send(): data[ %d ] = 0x%04X\n", i, list[ i ] );

    // NOTE:  It is unclear at this point whether we should issue a 
    //        'TI_return()' at the end of a 'TI_Send()' also?


} // end TI_Send()
// ============================ CBOT main =================================== //
void CBOT_main( void )
{

	TIMEROBJ	led_timer;
    SUBSYS_OPENSTAT ops;
    CAPI_REV api_rev;

    // Open needed modules.
    ops = LED_open();
    ops = LCD_open();
    ops = STEPPER_open();
    ops = UART_open( UART_UART0 );

	// Register timer event.
	TMRSRVC_REGISTER_EVENT( led_timer, led_toggle );

	// Start the timer event.
	TMRSRVC_new( &led_timer, TMRFLG_NOTIFY_FUNC, TMRTCM_RESTART, 
                                                                TMR_SECS( 1 ) );

    // Initialize the UART.
    UART_configure( UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 
                                                                        57600 );

    // Enable the UART's transmitter.
    UART_set_TX_state( UART_UART0, UART_ENABLE );

    // Print message on LCD.
    LCD_printf( "Started!\n" );

    // Notify we're about to open the TI subsystem module.
    UART0_printf( "Opening TI subsystem module...\n" );

    // Set both parameters to 'NULL' to use the internal command set.
    ops = TI_open( TI_Get, TI_Send );

    if ( ops.state == SUBSYS_OPEN )
	{

        UART0_printf( "TI module OK.\n" );
		UART0_printf( "TI module open and ready!\n" );

	}
    else
	{
		UART0_printf("Unable to open module -- Port Attachment Error.\n");
        UART0_printf("**TI module ERROR!**\n");
	}

	LCD_printf( "Awaiting...\n" );

    // Don't leave.
    while( 1 )
        /* NULL */ ;

} // end CBOT_main()

