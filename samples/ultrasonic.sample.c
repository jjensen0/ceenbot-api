// Auth: Jose Santos
// Desc: Sample program for using the 'stopwatch' service to measure the pulse
//       with of the 'echo' to measure distances using the Parallax Ultrasonic
//       sensor.  There are several modules being taken advantage of in this
//       sample.  Timer objects are used to trigger the 'pulse-function' 8-times
//       per second.  Pin-change interrupts are also implemented to detect
//       when the pulse begins and ends as determined by the Ultrasonic sensor.
//       These 'pin-change' interrupts are, in turn, used to determine when
//       to start the stopwatch and stop it.  Once it is stopped, the value is
//       obtained and from this, the distance is computed.

#include "capi324v221.h"

// ============================= defines ==================================== //
// =========================== declarations ================================= //
// ============================== globals =================================== //
volatile BOOL stopwatch_done = FALSE;    // We use this variable as a flag 
                                         // to indicate the ultrasonic pulse 
                                         // has completed.

TIMEROBJ  echo_time;                     // Timer object to trigger a 'PING'
                                         // with the Ultrasonic sensor.  We're
                                         // going to trigger this timer object
                                         // at least 8 times a second (every
                                         // 125ms or so).
// ============================ prototypes ================================== //

// *** Miscellaneous prototypes ***:
void init_bot( void );
void pinch_config( void );
void ping_timer_event( void );
SWTIME trigger_pulse( void );
// ============================== ISR's ===================================== //
// Desc: ISR for 'pin-change' interrupt.  We will use this interrupt to monitor
//       'PA4' when configured as an INPUT once the pulse has been sent to the
//       ultrasonic sensor.
ISR( PCINT0_vect )
{

    // Determine what transition has triggered THIS pin-change interrupt,
    // whether a 0->1 transition or 1->0 transition.

    // If 0->1 transition...
    if ( GBV( 4, PINA ) )
    {

        // The beginning of the 'echo pulse' has arrived!
        STOPWATCH_start();  // Start counting!

    } // end if()

    // Otherwise, if 1->0 transition...
    else
    {

        STOPWATCH_stop();       // Stop the stopwatch!
        stopwatch_done = TRUE;  // Indicate we're done so that 'trigger_pulse()'
                                // doesn't 'busy-wait' forever.

        CBV( 4, PCMSK0 );       // Disable further pin-change interrupts from
                                // takingp place.

    } // end else.

} // end ISR( PCINT0 )
// ============================ functions =================================== //
void init_bot( void )
{

    SUBSYS_OPENSTAT opstat;

    // Start by opening and initializing the needed subsystem modules:

    opstat = LCD_open();        // Open the LCD subsystem.
    opstat = LED_open();        // Open the LED subsystem.
    opstat = STOPWATCH_open();  // Open the STOPWATCH service.

    // We'll be using 'PA4' for triggering the Ultrasonic sensor, so let
    // us set up that pin:

    SBD( A, 4, OUTPIN );        // Set PA4 (PORTA) to Output.
    CBV( 4, PORTA );            // Set it to LOW.


} // end init_bot()
// -------------------------------------------------------------------------- //
void pinch_config( void )
{

    // Enable pin-change interrupts for group PCINT7..0.
    SBV( PCIE0, PCICR);

    // That's it really.  The only thing left to do is allow for pin-change
    // interrupts to take effect -- but we will do this elsewhere, NOT here.

} // end pinch_config()
// -------------------------------------------------------------------------- //
void ping_timer_event( void )
{

    unsigned long int time_us = 0;  // Holds time in micro-seconds.

    float dist_cm = 0;              // Holds distance in 'cm'.

    SWTIME stopwatch_ticks = 0;     // Holds stopwtach ticks in 10us/tick.

    // Trigger and wait for response.
    stopwatch_ticks = trigger_pulse();

    // Compute time in microseconds -- each stopwatch 'tick' = 10us.
    time_us = (( unsigned long int ) stopwatch_ticks ) * 10;

    // Compute the distance in 'cm'.
    dist_cm = (( float ) time_us ) * 0.01724;

    // Print results.
    LCD_printf_RC( 3, 0, "swtime = %d ticks\t", stopwatch_ticks );
    LCD_printf_RC( 2, 0, "time_us = %ld us\t", time_us );
    LCD_printf_RC( 1, 0, "dist_cm = %.2f cm\t", dist_cm );

} // end ping_timer_event()
// -------------------------------------------------------------------------- //
SWTIME trigger_pulse( void )
{

    // RESET the global variable used to denote the pulse is done.
    stopwatch_done = FALSE;

    // Make sure the stopwatch is reset prior to each pulse trigger.
    STOPWATCH_reset();

    // Turn the LED on so we know this is working.
    LED_state( LED_Green, LED_ON );

    // Start the pulse.
    SBV( 4, PORTA );

    // Wait 5us or so.
    DELAY_us( 5 );

    // Clear the pulse.
    CBV( 4, PORTA );

    // Wait ~50us.
    DELAY_us( 50 );

    // Swith PA4 to input so we can 'listen' for the echo.
    SBD( A, 4, INPIN );

    // Enable pin-change interrupt so we can be notified of changes to 'PA4'.
    SBV( 4, PCMSK0 );

    // Now we wait until the pulse echo is complete.  We're just
    // going to 'busy-wait'.
    while( stopwatch_done == FALSE );

    // Switch 'PA4' back to an OUTPUT for the next pulse occurrence.
    SBD( A, 4, OUTPIN );

    // ...and make sure it stays low.
    CBV( 4, PORTA );

    // Turn the LED OFF so we know we're not stuck busy-waiting.
    LED_state( LED_Green, LED_OFF );

    // Return the stopwatch's time.
    return ( STOPWATCH_get_ticks() );

} // end trigger_pulse()
// ============================ CBOT_main =================================== //
void CBOT_main( void )
{

    // Initialize.
    init_bot();

    // Initialize for 'pin-change' interrupt.
    pinch_config();

    // Notify we're about to start.
    LCD_printf( "Starting test...\n" );

#ifndef __DEBUG

    // Wait 3 seconds.
    TMRSRVC_delay( TMR_SECS( 3 ) );

#endif

    // Clear the screen.
    LCD_clear();

    // Start the timed event to occur 8-times per second.
    TMRSRVC_new( &echo_time, TMRFLG_NOTIFY_FLAG, TMRTCM_RESTART, 125 );

    // Enter the Oh-Eternal while loop...
    while( 1 )
    {

        // But only trigger a 'ping' every 125ms.
        TMRSRVC_on_TC( echo_time, ping_timer_event() );

    } // end while()

   

} // end CBOT_main()

