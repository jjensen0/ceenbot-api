// Auth: Jose Santos
// Desc: Sample code using the CEENBoT (TM) API.

#include "capi324v221.h"
// =========================== timer notify routines ======================== //
TMR_NR( timer0_callback );
TMR_NR( timer0_callback )
{

    // Just toggle the green LED.
    LED_toggle( LED_Green );

} // end TMR_NR( timer0_callback )

// ============================ CBOT main =================================== //

void CBOT_main( void )
{

    SUBSYS_OPENSTAT open_status;
    STEPPER_NOTIFY steps_done;

    TIMEROBJ timer0;
    timer0.pNotifyFunc = timer0_callback;

    // Open up the LED Interface.
    open_status = LED_open();

    // Start the timer.
    TMRSRVC_new( &timer0, TMRFLG_NOTIFY_FUNC, TMR_TCM_RESTART, 500 );

    // Turn the red LED ON.
    LED_set( LED_Red );

    // Open up the necessary subsystems needed.
    open_status = LCD_open();

    LCD_clear();

    LED_clr( LED_Red );

    open_status = STEPPER_open();

    // Set up the steppers to run in STEP mode.
    STEPPER_set_mode( BOTH_STEPPERS, STEPPER_STEP_MODE );

    // Set acceleration.
    STEPPER_set_accel( BOTH_STEPPERS, 500 );

    // Set stepper power mode.
    STEPPER_set_pwr_mode( STEPPER_PWR_LOW );

    // Clear the LCD.
    LCD_clear();
    LCD_printf( "Starting test...\n" );

    // Wait 3 seconds.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    LCD_clear();

    // This test sequence uses the 'step-wait' version of the step functions.

    /*
    LCD_printf( "Running:1\n" );
    STEPPER_stepwt( LEFT_STEPPER, STEPPER_FWD, 200, 200, STEPPER_BRK_OFF );

    LCD_printf( "Running:2\n" );
    STEPPER_stepwt( RIGHT_STEPPER, STEPPER_FWD, 200, 200, STEPPER_BRK_OFF );

    LCD_printf( "Running:3\n" );
    STEPPER_stepwt( BOTH_STEPPERS, STEPPER_REV, 300, 100, STEPPER_BRK_OFF );

    LCD_printf( "Running:4\n" );
    STEPPER_stepwt( BOTH_STEPPERS, STEPPER_FWD, 100, 300, STEPPER_BRK_OFF );
    */

    // This test sequence uses the non-blocking step functions.

    /*
    LCD_printf( "Testing b-wait 1.\n" );
    STEPPER_stepnb( BOTH_STEPPERS, STEPPER_FWD, 200, 300, STEPPER_BRK_ON, 
                                                            &steps_done );

    // Wait until both steppers are done.
    while( ( !steps_done.left ) || ( !steps_done.right ) );

    // Wait two second so we can verify the brakes are engaged.
    LCD_printf( "Brake check.\n" );
    TMRSRVC_delay( TMR_SECS( 2 ) );

    LCD_printf( "Testing b-wait 2.\n" );
    STEPPER_stepnb( BOTH_STEPPERS, STEPPER_REV, 200, 300, STEPPER_BRK_OFF,
                                                            &steps_done );

    // Wait until both steppers are done.
    while( ( !steps_done.left ) || ( !steps_done.right ) );
    */

    // Done message.
    LCD_clear();
    LCD_printf( "Test complete.\n" );

    STEPPER_close();

    while( 1 );

} // end CBOT_main()
