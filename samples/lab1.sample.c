// Auth: Jose Santos
// Desc: Sample program for Robotics Laboratory 1.

#include "capi324v221.h"

// Desc: This parameter constant controls the speed that the 'BoT "explores.".
//       NOTE: Try with '150', '250', '350', just for fun!
#define BOT_MOTION_SPEED    150

// Desc: This parameter controls the number of steps needed for an 'approximate'
//       90-degree turn.  The values are slightly different for when non-zero
//       acceleration is in effect, or when it is disabled.  These values are
//       determined 'experimentally', but once found there's almost no need
//       to change it as result are fairly consistent.
//
// #define DEG_90  150      /* Good with no acceleration enabled. */
#define DEG_90  140         /* Good with acceleration enabled. */

// ============================ prototypes ================================== //
// *** 'Behavioral' function prototypes ***

// Desc: Does square exploration using 'dead-reckoning'.
void explore( void );
void move_wt( signed short int how_far );
void turn_wt( signed short int how_much );

// *** Other function prototypes ***
void init_bot( void );

// ============================= functions ================================== //
void init_bot( void )
{

    SUBSYS_OPENSTAT opstat;
    
    // Open needed subsystem modules.
    
    opstat = LED_open();        // LED subsystem.
    opstat = LCD_open();        // LCD subsystem.
    opstat = STEPPER_open();    // STEPPER subsystem.
    
    // Set the stepper mode.
    STEPPER_set_mode( BOTH_STEPPERS, STEPPER_STEP_MODE );
    
    // Enable low-power mode.
    STEPPER_set_pwr_mode( STEPPER_PWR_LOW );
    
    // Set acceleration.
    STEPPER_set_accel( BOTH_STEPPERS, 400 );
    
} // end init_bot()
// -------------------------------------------------------------------------- //
void explore( void )
{

    // Move forward for 2 revolutions.
    move_wt( 400 );
    
    // Turn ~90-deg.
    turn_wt( DEG_90 );
    
    // Move forward for 2 revolutions.
    move_wt( 400 );
    
    // Turn ~90-deg.
    turn_wt( DEG_90 );
    
    // Move forward for 2 revolutions.
    move_wt( 400 );
    
    // Turn ~90-deg.
    turn_wt( DEG_90 );
    
    // Move forward for 2 revolutions.
    move_wt( 400 );

    // Turn ~90-deg one last time.
    turn_wt( DEG_90 );
    
} // end explore()
// -------------------------------------------------------------------------- //
void move_wt( signed short int how_far )
{

    if ( how_far > 0 )
    {
    
        // Just call the API 'step' function to do this.
        STEPPER_stepwt( BOTH_STEPPERS, STEPPER_FWD, how_far, 
                        BOT_MOTION_SPEED, STEPPER_BRK_OFF );
                                                            
    } // end if()
    
    else if ( how_far < 0 )
    {
    
        // Just call the API 'step' function to do this.
        STEPPER_stepwt( BOTH_STEPPERS, STEPPER_REV, -1*how_far, 
                        BOT_MOTION_SPEED, STEPPER_BRK_OFF );
    
    } // end else-if()
    
} // end move_forward()
// -------------------------------------------------------------------------- //
void turn_wt( signed short int how_much )
{

    STEPPER_NOTIFY tc;  // Flags for identifying the 'terminal count' for steps.
    
    if ( how_much != 0 )
    {
    
        if ( how_much > 0 )
        {
    
            // Move right stepper backward.
            STEPPER_stepnb( RIGHT_STEPPER, STEPPER_REV, how_much, 
                            BOT_MOTION_SPEED, STEPPER_BRK_OFF, &tc );
                                            
            // Move left stepper forward.
            STEPPER_stepnb( LEFT_STEPPER, STEPPER_FWD, how_much, 
                            BOT_MOTION_SPEED, STEPPER_BRK_OFF, &tc );
                                            
        } // end if()
    
        else if ( how_much < 0 )
        {
    
            // Move left stepper backward.
            STEPPER_stepnb( LEFT_STEPPER, STEPPER_REV, -1*how_much, 
                            BOT_MOTION_SPEED, STEPPER_BRK_OFF, &tc );
                                            
            // Move right stepper forward.
            STEPPER_stepnb( RIGHT_STEPPER, STEPPER_FWD, -1*how_much, 
                            BOT_MOTION_SPEED, STEPPER_BRK_OFF, &tc );    
                            
        } // end else-if()

        // Wait on the motion to complete.
        while( ( !tc.left ) || ( !tc.right ) );
    
    } // end if()
    
} // end turn()
// ============================ CBoT main =================================== //
void CBOT_main( void )
{

    // Initialize the 'BoT.
    init_bot();

    // Notify that all subsystems are ready.
    LCD_printf( "Subsystems ready.\n" );
    LCD_printf( "Starting...\n"  );
    
    // Wait 3 seconds before starting.
    TMRSRVC_delay( TMR_SECS( 3 ) );
    
    LCD_printf( "Exploring!\n" );
    
    // Start exploring!
    explore();
    
    LCD_printf( "Finished!" );

} // end CBOT_main()
