// Auth: Jose Santos
// Desc: Testing the 'speaker subsystem' which generates tones.

#include "capi324v221.h"

void CBOT_main( void )
{

    SUBSYS_OPENSTAT opstat;

    // Initialize the needed subsystems:
    opstat = LCD_open();        // Open the LCD subsystem.
    opstat = LED_open();        // Open the LED subsystem.
    opstat = SPKR_open();       // Open the SPEAKER subsystem.

    LCD_printf( "About to beep...\n" );

    // Wait before beeping.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Beep for a second.
    SPKR_tone( 523, TMR_SECS( 1 ) );
    // SPKR_tone( 440, TMR_SECS( 1 ) );

    while( 1 );                 // Don't leave.
    
} // end CBOT_main()
