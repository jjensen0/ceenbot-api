// Auth: Jose Santos
// Desc: Sample program to test I2C module.  The program tests operation of the
//       MASTER-mode commands by testing out communication of a COLOR SENSOR
//       that is custom-wired to interface with the CEENBoT.  This program
//       contains sample could that could be used to create a 'COLOR SENSOR'
//       module for the CEENBoT.

typedef unsigned short int USHORT;
typedef unsigned char      UBYTE;

#include "capi324v221.h"

// -----------------------  Prototypes:
void  SensorSampleTrigger( void );
void  SetSensorCaps( UBYTE red_cap, UBYTE green_cap, UBYTE blue_cap, UBYTE white_cap );
void  SetIntValues( USHORT red_int, USHORT green_int, USHORT blue_int, USHORT white_int );
UBYTE GetSensorData( UBYTE reg_addr );

// -----------------------  Functions:
void SensorSampleTrigger( void )
{

	I2C_STATUS status;

	// First we need to trigger a 'sensor sample'.
	status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

	// Send the register that we wish to write to.
	status = I2C_MSTR_send( 0x00 );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	// Send the value to be written to this register.
	status = I2C_MSTR_send( 0x01 );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

} // end SensorSampleTrigger()
// ----------------------------------- //
UBYTE GetSensorData( UBYTE reg_addr )
{

	I2C_STATUS status;

	UBYTE data = 0;	

	// Start a sequence for writing.
	status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

	
	// Send the Register Address we wish to read data from.
	status = I2C_MSTR_send( reg_addr );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	// Restart a sequence for reading.
	status = I2C_MSTR_start( 0x74, I2C_MODE_MR );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'RE-start' [ 0x%X ].\n", status );

	// Get the data byte, and DO NOT acknowledge.
	status = I2C_MSTR_get( &data, FALSE );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'get' [ 0x%X ].\n", status );

	// Stop the sequence.
	I2C_MSTR_stop();

	return data;

} // end GetSensorData()
// ----------------------------------- //
void  SetSensorCaps( UBYTE red_cap, UBYTE green_cap, UBYTE blue_cap, UBYTE white_cap )
{

	I2C_STATUS status;

	// *** Ensure limits:
	red_cap   &= 0x0F;
	green_cap &= 0x0F;
	blue_cap  &= 0x0F;
	white_cap &= 0x0F;

	// *** WRITE RED CAP VALUE...

	// Start register write sequence.
	status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

	// Send the register that we wish to write to.
	status = I2C_MSTR_send( 0x06 );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	// Send the value to be written to this register.
	status = I2C_MSTR_send( red_cap );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	I2C_MSTR_stop();




// *** WRITE GREEN CAP VALUE...

	// Start register write sequence.
	status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

	// Send the register that we wish to write to.
	status = I2C_MSTR_send( 0x07 );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	// Send the value to be written to this register.
	status = I2C_MSTR_send( green_cap );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	I2C_MSTR_stop();




// *** WRITE BLUE CAP VALUE...

	// Start register write sequence.
	status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

	// Send the register that we wish to write to.
	status = I2C_MSTR_send( 0x08 );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	// Send the value to be written to this register.
	status = I2C_MSTR_send( blue_cap );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	I2C_MSTR_stop();


	
	// *** WRITE WHITE CAP VALUE...

	// Start register write sequence.
	status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

	// Send the register that we wish to write to.
	status = I2C_MSTR_send( 0x09 );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	// Send the value to be written to this register.
	status = I2C_MSTR_send( white_cap );

	if ( status != I2C_STAT_OK )

		UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

	I2C_MSTR_stop();

} // end SetSensorCaps()
// ----------------------------------- //

void  SetIntValues( USHORT red_int, USHORT green_int, USHORT blue_int, USHORT white_int )
{

	USHORT data[ 4 ] = { red_int, green_int, blue_int, white_int };

	USHORT i = 0;

	UBYTE addr = 0x0A;

	I2C_STATUS status;

	for( i = 0; i < 4; ++i )
	{

		// ** Send LOW BYTE...

		// Start register write sequence.
		status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

		if ( status != I2C_STAT_OK )

			UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

		// Send the register that we wish to write to.
		status = I2C_MSTR_send( addr++ );

		if ( status != I2C_STAT_OK )

			UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

		// Send the value to be written to this register.
		status = I2C_MSTR_send( ( 0x00FF & data[ i ] ) );

		if ( status != I2C_STAT_OK )

			UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

		I2C_MSTR_stop();


		// ** Send HIGH BYTE...

		status = I2C_MSTR_start( 0x74, I2C_MODE_MT );

		if ( status != I2C_STAT_OK )

			UART_printf( UART_UART0, "I2C: Error with 'start' [ 0x%X ].\n", status );

		// Send the register that we wish to write to.
		status = I2C_MSTR_send( addr++ );

		if ( status != I2C_STAT_OK )

			UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

		// Send the value to be written to this register.
		status = I2C_MSTR_send( ( data[ i ] >> 8 ) & 0x00FF );

		if ( status != I2C_STAT_OK )

			UART_printf( UART_UART0, "I2C: Error with 'send' [ 0x%X ].\n", status );

		I2C_MSTR_stop();


	}// end for()

} // end SetIntValues()

// -----------------------  CBoT Main:

void CBOT_main( void )
{

	// For storing color data.	
	USHORT red   = 0;
	USHORT green = 0;
	USHORT blue  = 0;
	USHORT white = 0;

	// Open the needed modules:
	LED_open();
	LCD_open();
	UART_open( UART_UART0 );

	// Configure the UART device.
	UART_configure( UART_UART0, 
						UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 57600 );

	// Enable the transmitter portion of the UART device.
	UART_set_TX_state( UART_UART0, UART_ENABLE );

	// Configure PINS to be used with the color sensor:
	//  (aside from the I2C bus pins).  The SLEEP pin is pulled down
	//  by a resistor (on the evaluation board), so we'll keep this 
	//  as an input pin.
	SBD( A, 3, INPIN );		// PA3 out (SLEEP pin).
	SBD( A, 5, OUTPIN );	// PA5 out ( LED  pin).

	// Turn the color sensor's LED ON.
	SBV( 5, PORTA );


	// Open the I2C subsystem module.
	UART_printf( UART_UART0, "Opening I2C device...\n" );

	I2C_open();		// Open the I2C device (by default it is configured
	                // to operate at 100Khz.

	// Check if the device is busy.
	if ( I2C_IsBusy() )

		UART_printf( UART_UART0, "I2C is BUSY.\n" );

	else

		UART_printf( UART_UART0, "I2C is NOT busy.\n" );

	UART_printf( UART_UART0, "I2C should be ready.\n" );

	UART_printf( UART_UART0, "Setting sensor caps...\n" );

	// Configure the sensor.
	SetSensorCaps( 12, 12, 12, 12 );

	UART_printf( UART_UART0, "Done.\n" );

	// Configure the integration values.
	UART_printf( UART_UART0, "Setting Integration Values...\n" );

	SetIntValues( 500, 500, 500, 500 );

	UART_printf( UART_UART0, "Done.\n" );


	UART_printf( UART_UART0, "Starting Sampling loop...\n" );

	// Let's try to read color sensor data.

	while( 1 ) {

		// Trigger the color sensor to sample data.
		SensorSampleTrigger();

		// Read last sampled color data.
		red   = GetSensorData( 0x40 );	// Get the RED.
		green = GetSensorData( 0x42 );	// Get the GREEN.
		blue  = GetSensorData( 0x44 );  // Get the BLUE.
		white = GetSensorData( 0x46 );  // Get the WHITE.

		UART_printf( UART_UART0,
			"R = %5d, G = %5d, B = %5d, W = %5d\n", red, green, blue, white );

		TMRSRVC_delay( TMR_SECS( 1 ) ); // Wait 1 second before doing it again.
		
	} // end while()

} // end CBOT_main()
