// Auth: Jose Santos
// Desc: This sample program shows how to use the Ultrasonic module to
//       measure distances.  Because the ultrasonic module depends on the
//		 stopwatch module, the stopwatch module is started also.  In addition
//		 the sample program shows how to use some timer-service features.
//		 This source code is the 'easy-version' of using the Ultrasonic.


#include "capi324v221.h"

// ============================== globals =================================== //
TIMEROBJ  echo_time;                     // Timer object to trigger a 'PING'
                                         // with the Ultrasonic sensor.  We're
                                         // going to trigger this timer object
                                         // at least 8 times a second (every
                                         // 125ms or so).
// ============================ prototypes ================================== //

// *** Miscellaneous prototypes ***:
void init_bot( void );

void ping_timer_event( void );

// ============================ functions =================================== //
void init_bot( void )
{

    SUBSYS_OPENSTAT opstat;

    // Start by opening and initializing the needed subsystem modules:

    opstat = LCD_open();        // Open the LCD subsystem.
    opstat = LED_open();        // Open the LED subsystem.
    opstat = STOPWATCH_open();  // Open the STOPWATCH service.
	opstat = USONIC_open();		// Open the ULTRASONIC subsystem.

    


} // end init_bot()
// -------------------------------------------------------------------------- //
void ping_timer_event( void )
{

    unsigned long int time_us = 0;  // Holds time in micro-seconds.

    float dist_cm = 0;              // Holds distance in 'cm'.

    SWTIME stopwatch_ticks = 0;     // Holds stopwtach ticks in 10us/tick.

	// Turn LED ON so we know this is working.
	LED_state( LED_Green, LED_ON );

    // Trigger and wait for response.
    stopwatch_ticks = USONIC_ping();

	// Turn LED OFF.
	LED_state( LED_Green, LED_OFF );

    // Compute time in microseconds -- each stopwatch 'tick' = 10us.
    time_us = (( unsigned long int ) stopwatch_ticks ) * 10;

    // Compute the distance in 'cm'.
    dist_cm = (( float ) time_us ) * 0.01724;

    // Print results.
    LCD_printf_RC( 3, 0, "swtime = %d ticks\t", stopwatch_ticks );
    LCD_printf_RC( 2, 0, "time_us = %ld us\t", time_us );
    LCD_printf_RC( 1, 0, "dist_cm = %.2f cm\t", dist_cm );

} // end ping_timer_event()
// ============================ CBOT_main =================================== //
void CBOT_main( void )
{

    // Initialize.
    init_bot();

    // Notify we're about to start.
    LCD_printf( "Starting test...\n" );

#ifndef __DEBUG

    // Wait 3 seconds.
    TMRSRVC_delay( TMR_SECS( 3 ) );

#endif

    // Clear the screen.
    LCD_clear();

    // Start the timed event to occur 8-times per second.
    TMRSRVC_new( &echo_time, TMRFLG_NOTIFY_FLAG, TMRTCM_RESTART, 125 );

    // Enter the Oh-Eternal while loop...
    while( 1 )
    {

        // But only trigger a 'ping' every 125ms.
        TMRSRVC_on_TC( echo_time, ping_timer_event() );

    } // end while()   

} // end CBOT_main()
