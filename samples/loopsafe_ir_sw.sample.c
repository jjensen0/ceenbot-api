// Auth: Jose Santos
// Desc: Sample program, which tests the new 'LOOP-SAFE' 'Tiny sensor request
//       info for switch and IR data.

#include "capi324v221.h"

void CBOT_main( void )
{

    LCD_open();

	unsigned short int SW3 = 0;
	unsigned short int SW4 = 0;
	unsigned short int SW5 = 0;
	unsigned short int triggered_IR = 0;
    
	unsigned short int i = 0;


    while( 1 )
    {

		// Test the switches...

		if ( ATTINY_get_SW_state( ATTINY_SW3) )
		{

			LCD_printf_RC( 3, 0, "SW3: %d\t", ++SW3 );

		}

		if ( ATTINY_get_SW_state( ATTINY_SW4 ) )
		{

			LCD_printf_RC( 2, 0, "SW4: %d\t", ++SW4 );

		}

		if ( ATTINY_get_SW_state( ATTINY_SW5 ) )
		{

			LCD_printf_RC( 1, 0, "SW5: %d\t", ++SW5 )	

		}

		// Test the IR sensors.
		if ( ATTINY_get_IR_state( ATTINY_IR_RIGHT ) ||
		     ATTINY_get_IR_state( ATTINY_IR_LEFT ) )
		{

			LCD_printf_RC( 0, 0, "IR: %d\t", ++triggered_IR );

		} // end if()
		
	} // end while()
    
} // end CBOT_main()
