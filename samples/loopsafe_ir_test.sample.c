// Auth: Jose Santos
// Desc: Test program to show the loop-safe versions of the IR sensor
//       functions.

#include "capi324v221.h"

void CBOT_main( void )
{

    STEPPER_open();     // Open stepper services.

    // Enter the infinite loop...
    while( 1 )
    {

        if ( ATTINY_get_IR_state( ATTINY_IR_BOTH ) )
        {

            // Back up!
            STEPPER_move_stwt( STEPPER_BOTH,
                STEPPER_REV, 400, 200, 400, STEPPER_BRK_OFF, 
                STEPPER_REV, 400, 200, 400, STEPPER_BRK_OFF );

        } // end else-if()

        else if ( ATTINY_get_IR_state( ATTINY_IR_LEFT ) )
        {

            // Turn Left!
            STEPPER_move_stwt( STEPPER_BOTH,
                STEPPER_REV, 150, 250, 400, STEPPER_BRK_OFF,
                STEPPER_FWD, 150, 250, 400, STEPPER_BRK_OFF );

        } // end else-if()

        else if ( ATTINY_get_IR_state( ATTINY_IR_RIGHT ) )
        {

            // Turn Right!
            STEPPER_move_stwt( STEPPER_BOTH,
                STEPPER_FWD, 150, 250, 400, STEPPER_BRK_OFF,
                STEPPER_REV, 150, 250, 400, STEPPER_BRK_OFF );

        } // end else-if()

    } // end while()

} // end CBOT_main()

