// Desc: This sample program shows how to use the non-blocking versions of
//       the stepper functions and how to use the 'STEPPER_wait_and_then()' 
//	     functions to wait on motion completion of the steppers.  The
//		 'STEPPER_wait_and_then()' function can be used to trigger 'stepper
//	     events'.  These are basically functions that get triggered as a 
//		 result of either the left stepper or right stepper (or both) 
//		 completing their previously triggered motions.

#include "capi324v221.h"
// =========================== prototypes =============================== //
STEPPER_EVENT( left_stepper_event );
STEPPER_EVENT( right_stepper_event );
// ========================== Stepper Events ============================ //
STEPPER_EVENT( left_stepper_event )
{

	// Display left motor has completed motion.
	LCD_printf_RC( 3, 0, "Left motor stopped.\t" );

	// Wait 3 seconds.
	TMRSRVC_delay( TMR_SECS( 3 ) );

	// Disable the brake, which will be enagaged when the motion completes.
	STEPPER_stop( LEFT_STEPPER, STEPPER_BRK_OFF );

} // end STEPPER_EVENT( left_stepper_event )

STEPPER_EVENT( right_stepper_event )
{

	// Display right motor has completed motion.
	LCD_printf_RC( 2, 0, "Right motor stopped.\t" );

	// Wait 3 seconds.
	TMRSRVC_delay( TMR_SECS( 3 ) );

	// Disable the brake, which will be engaged when the motion completeds.
	STEPPER_stop( RIGHT_STEPPER, STEPPER_BRK_OFF );

} // end STEPPER_EVENT( right_stepper_event )

// ============================= CBOT main ============================== //
void CBOT_main( void )
{

	SUBSYS_OPENSTAT opstat;

	// Initialize needed modules.
	opstat = LED_open();
	opstat = LCD_open();
	opstat = STEPPER_open();

	// Set the stepper mode.
	STEPPER_set_mode( BOTH_STEPPERS, STEPPER_STEP_MODE );

	// Set the acceleration.
	STEPPER_set_accel( LEFT_STEPPER, 500 );
	STEPPER_set_accel( RIGHT_STEPPER, 100 );

	LCD_printf( "Starting test...\n" );

	// Wait 3 seconds.
	TMRSRVC_delay( TMR_SECS( 3 ) );

	

	// Clear the LCD.
	LCD_clear();

	// Set the motion parameters for left and right independently.  Since
	// we're invoking the 'non-blocking' versions of the step functions
	// both function calls below will occur pretty much simultaneously.
	STEPPER_stepnb( RIGHT_STEPPER, STEPPER_FWD, 600, 200, STEPPER_BRK_ON );
	STEPPER_stepnb( LEFT_STEPPER, STEPPER_REV, 400, 200, STEPPER_BRK_ON );

	// Turn the green led on to indicate parameters are set and that
	// the stepper motors should be running.
	LED_state( LED_Green, LED_ON );

	// Then, we wait for both motions to complete, and in addition, the
	// corresponding stepper events will be triggered upon completion of
	// each stepper.
	STEPPER_wait_and_then( BOTH_STEPPERS, 
							     left_stepper_event, right_stepper_event );

	// Turn the Green LED off to indicate motion is off and we have 
	// reached this point in the code.
	LED_state( LED_Green, LED_OFF );

	// Clear the LCD.
	LCD_clear();
	
	// Print message.
	LCD_printf( "Test complete.\n" );

	// Don't leave.
	while( 1 );

} // end CBOT_main()

