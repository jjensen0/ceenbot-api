// Desc: Sample program to test the 'GPI_move()' function, which implements
//	     the  'Move' tool in the GPI-GUI.  This particular example shows how 
//		 to invoke 'GPI_move()' using the NON-BLOCKING STEP mode (by 
//		 specifying 'GPI_STEPPER_NO_BLOCK' for the 'run_mode' in the 
//		 'GPI_move()' function.  It also shows how to implement the 
//		 'GPI_move_wait_on()' function to control further execution after
//		 'GPI_move()' is invoked in this mode (that is, 
//		 'GPI_STEPPER_NO_BLOCK').

#include "capi324v221.h"
#include "gpi324v221.h"

// ============================== prototypes ============================ //
GPI_STEPPER_EVENT( left_stepper  );
GPI_STEPPER_EVENT( right_stepper );
// ============================== functions ============================= //
GPI_STEPPER_EVENT( left_stepper )
{

	GPI_display( GPI_DISPLINE1, "Left stopped." );

} // end left_stepper()

GPI_STEPPER_EVENT( right_stepper )
{

	GPI_display( GPI_DISPLINE2, "Right stopped." );

} // end right_stepper()


// ============================== CBOT main ============================= //
void CBOT_main( void )
{

	// Start the GPI interface:
	GPI_MODULES gpi_modules;

	gpi_modules.GPI_step = TRUE;
	gpi_modules.GPI_lcd  = TRUE;
	gpi_modules.GPI_led  = TRUE;

	// Start the specified modules.
	GPI_open( &gpi_modules );

	// Display test is about to begin.
	GPI_display( GPI_DISPLINE0, "Starting..." );

	// Wait 3 seconds.
	GPI_delay( TMR_SECS( 3 ) );

	// Let's move both motors with independent settings.
	GPI_move( GPI_STEPPER_NO_BLOCK, GPI_STEPPER_BOTH,

		// Left motor params:
		GPI_STEP_DIR_FWD,	// Stepper direction.
		600,				// Step distance ( 600 = 3 revolutions ).
		250,				// Speed.
		400,				// Acceleration.
		GPI_STEP_BRK_OFF,	// Brakes off upon motion completion.
		NULL,				// Stepper event MUST be 'NULL' in this runmode.

		// Right motor params:
		GPI_STEP_DIR_REV,	// Stepper direction.
		600,				// Step distance ( 600 = 3 revolutions ).
		100,				// Speed.
		200,				// Acceleration.
		GPI_STEP_BRK_OFF,	// Brakes off upon motion completion.
		NULL		        // Stepper event MUST be 'NULL' in this runmode.

	); // end GPI_move().

	// After 'GPI_move()' is issued in 'GPI_STEPPER_NO_BLOCK' mode it will
	// IMMEDIATELY proceed to the next function or instruction.  So you 
	// could:
	//
	//		... call more [fast] functions here...
	//
	// For example, let us turn the Green LED ON to show this statement is
	// executed immediately after 'GPI_move()' issued above:
	//
				GPI_led( GPI_LED_ON, 0b01000000 );
	//
	//
	// But then, you'd want to WAIT since the motors are probably 
	// still moving before you do anything further.  In this mode, we do this
	// as follows:
	GPI_move_wait_and_then( GPI_STEPPER_BOTH, left_stepper, right_stepper );

	// ...that is, 'GPI_move_wait_and_then()' will HOLD or BUSY-WAIT until
	// both motors (as specified by 'GPI_STEPPER_BOTH') to complete moving
	// through their specified distances and upon completion of the left
	// stepper, it will trigger the stepper event 'left_stepper()', and
	// upon completion of the right stepper, it will trigger the stepper
	// event 'right_stepper()'.

	// To show this doesn't get executed until 'GPI_move_wait_and_then()'
	// lets go, let us turn the Green LED OFF after the fact -- you'll see
	// this won't happen immediately as 'GPI_move_wait_and_then()' is 
	// waiting for the previously issued motor motion to complete on BOTH
	// motors.
	GPI_led( GPI_LED_OFF, 0b01000000 );

	// Wait 2 seconds before clearing the display.
	GPI_delay( TMR_SECS( 2 ) );

	// Clear the display.
	GPI_clr_disp();

	// Denote that we're done.
	GPI_display( GPI_DISPLINE0, "Test complete." );

	while( 1 );	// Don't leave.

} // end CBOT_main()
