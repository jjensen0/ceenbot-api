// Auth: Jose Santos
// Desc: This is a sample program that shows good practices when reading data
//       from the PS2 controller.  In addition to reading data from the con-
//       troller it shows how you might go about sing the 'PSXC_get_center()'
//       function to determine the minimum 'dead-zone' readius for controller
//       data when doing value remapping via 'PSXC_plinear_map()'.

#include "cbot324v221.h"

// 'PADDING' adds additional 'buffer zone' between the idle 'center values'
// returned from the controllers and the end of the 'dead zone' when using
// pseudo-linear mapping via 'PSXC_plinear_map()' function.
#define PADDING 5

// ============================== globals =================================== //
TIMEROBJ timer0;
// ============================ prototypes ================================== //
TMR_NR( timer0_callback );
// ========================== Timer Routines ================================ //

TMR_NR( timer0_callback )
{

    PSXC_STDATA controller;     // For storing controller state data.
    PSXC_CENTER curr_center;    // For storing current 'center' data.

	static BOOL got_center = FALSE;

    // Left analog map.
    signed short int aleft_LR;  // Left/Right value.
    signed short int aleft_UD;  // Up/Down value.

    // Right analog map.
    signed short int aright_LR; // Left/Right value.
    signed short int aright_UD; // Up/Down value.

    // Toggle LED so we know this is running.
    LED_toggle( LED_Green );

    // Read controller and process only if data is valid.
    if ( PSXC_read( &controller ) == TRUE )
    {

        // Get center data if we don't have it, or if previously acquired
		// 'center values' have been INVALIDATED.
		if ( got_center == FALSE )
		{

			// Get new 'center values' and set the state variable 'got_center'
			// to indicate that we got it so we don't do this again the next
			// time around UNTIL an INVALIDATION occurs for whatever reason.
        	got_center = PSXC_get_center( &curr_center );			

		} // end if()

		// Then, do remapping ONLY if we have successfully received 
        // 'center values' AND if we're in ANALOG MODE.
		if ( ( got_center == TRUE ) && ( controller.data_type == PSXC_ANALOG ) )
		{

        	// Process the mapping.
        	aleft_LR = PSXC_plinear_map( controller.left_joy.left_right, 
                                 
                   ( curr_center.left_joy.left_right + PADDING ), 127, 1000 );

        	aleft_UD = PSXC_plinear_map( controller.left_joy.up_down, 

                   ( curr_center.left_joy.up_down + PADDING ), 127, 1000 );

        	aright_LR = PSXC_plinear_map( controller.right_joy.left_right,

                   ( curr_center.right_joy.left_right + PADDING ), 127, 1000 );

        	aright_UD = PSXC_plinear_map( controller.right_joy.up_down,

                   ( curr_center.right_joy.up_down + PADDING ), 127, 1000 );

		} // end if()

		// Otherwise, set to safe values, since it appears our data 
        // cannot be trusted.
		else
		{

			aleft_LR = 0;
			aleft_UD = 0;

			aright_LR = 0;
			aright_UD = 0;

		} // end else.

		// *** Display results on LCD ***

        // Set display position.
        LCD_set_next_PGC( 3, 0 );

        // Display the data.
        LCD_printf( "L-L/R: %d\t", aleft_LR );

        // Set display position.
        LCD_set_next_PGC( 2, 0 );

        // Display the data.
        LCD_printf( "L-U/D: %d\t", aleft_UD );

        // Set display position.
        LCD_set_next_PGC( 1, 0 );

        // Display the data.
        LCD_printf( "R-L/R: %d\t", aright_LR );

        // Set display position.
        LCD_set_next_PGC( 0, 0 );

        // Display the data.
        LCD_printf( "R-U/D: %d\t", aright_UD );

    } // end if()
    else
    {

		// Invalidate CENTER data -- this will force that we get new 
        // 'center values'
		// once VALID data does arrive -- whenver that happens, IF ever.
		got_center = FALSE;

        LCD_clear();

        LCD_printf( " *** INVALID *** " );

    } // end else.

} // end timer0_callback()

// ============================== main ====================================== //
void CBOT_main( void )
{

     SUBSYS_OPENSTAT opstat;

	 opstat = LCD_open();	// Open the LCD  subsystem module.
	 opstat = PSXC_open();	// Open the PSXC subsystem module.

     timer0.pNotifyFunc = timer0_callback;

     LCD_clear();
     LCD_printf( "Starting test." );

     // Wait two seconds before starting.
     TMRSRVC_delay( TMR_SECS( 2 ) );
    
     // Let the LED toggle so we know the system is running.
     TMRSRVC_new( &timer0, TMRFLG_FUNCNOTIFY, TMR_TCM_RESTART, 250 );

     // Don't leave.
     while( 1 );

} // end main()
