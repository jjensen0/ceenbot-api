/*
 * main.c
 *
 *  Created on: Aug 13, 2013
 *      Author: Kevin Dethlefs-Moreno
 */
#include "capi324v221.h"
#include <string.h>
#define DEBUG

void CBOT_main() {
	ADC_open();
	BATTERY_check();
	UART_open(UART_UART0);
	UART_configure(UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 256000);
	UART_set_TX_state(UART_UART0, UART_ENABLE);
	UART0_printf("UART0 enabled.");
#ifdef DEBUG
	UART0_printf(" DEBUG mode on.");
#endif
	UART0_printf("\n");
	I2C_open();
	RTC_open();
	ATTINY_open();

	STORE_open();
	TEMP_open();
	LCD_open();
	unsigned char sensors;
	char buf[30];
	STORE_LOCATION readLoc;
	while (1) {
		sensors = ATTINY_get_sensors();
		LCD_printf("Temp: %d\t\n", CtoF(TEMP_get()));
		RTC_update();
		LCD_printf("%02d/%02d/20%02d\t%02d:%02d:%02d %s\t", RTC_time.month,
				RTC_time.date, RTC_time.year, RTC_time.hour, RTC_time.min,
				RTC_time.sec, RTC_time.day);
		if (sensors & SNSR_SW3_EDGE) {
			sprintf(buf, "%s %d/%d/20%02d %02d:%02d:%02d - Temp: %d", RTC_time.day, RTC_time.month,
					RTC_time.date, RTC_time.year, RTC_time.hour, RTC_time.min, RTC_time.sec, CtoF(TEMP_get()));
			STORE_writes((unsigned char *) buf, strlen(buf));
			UART0_printf("Logged: %s\n", buf);
		} else if (sensors & SNSR_SW4_EDGE)
		{
		//	STORE_readAll();
			readLoc.high = 0x00;
			readLoc.low = 0x03;
			UART0_printf("Test print: %c\n", STORE_read(readLoc));
		} else if (sensors & SNSR_SW5_EDGE)
		{
			UART0_printf("RESETTING STORAGE.\n");
			STORE_reset();
		}
	}
}
