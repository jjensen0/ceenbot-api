// Auth: Jose Santos
// Desc: Sample to test the motors with acceleration enabled to determine
//       stepping count accuracy (or more precisely... lack thereof).

#include "capi324v221.h"

// =============================== CBOT main ================================ //
void CBOT_main( void )
{

    SUBSYS_OPENSTAT opstat;
    STEPPER_SPEED   speed;
    STEPPER_STEPS   steps;

    unsigned char   got_it = 0;

    // Initialize needed modules:
#ifndef __DEBUG
    opstat = LED_open();
    opstat = LCD_open();
#endif
    opstat = STEPPER_open();

    // Initialize the stepper subsystem.
    STEPPER_set_mode( BOTH_STEPPERS, STEPPER_STEP_MODE );
    STEPPER_set_pwr_mode( STEPPER_PWR_LOW );
    STEPPER_set_accel( BOTH_STEPPERS, 300 );

    LCD_printf( "Starting...\n" );

#ifndef __DEBUG
    TMRSRVC_delay( TMR_SECS( 3 ) );
#endif

    // Run it once.
    STEPPER_stepnb( BOTH_STEPPERS, STEPPER_FWD, STEPPER_REVS( 1 ), 300,
                                                STEPPER_BRK_OFF, NULL  );

#ifndef __DEBUG
    while( !got_it )
    {

        speed = STEPPER_get_curr_speed();

        if ( speed.right == 300 )
        {

            steps = STEPPER_get_nSteps();

            got_it = 1;

        } // end if()

    } // end while()

    LCD_printf( "Steps: %d\t", steps.right );

#endif

    while( 1 ); // Don't leave.



} // end CBOT_main()
