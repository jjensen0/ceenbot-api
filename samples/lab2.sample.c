// Auth: Jose Santos
// Desc: Sample program for Robotics Laboratory 2.

#include "capi324v221.h"

// ============================= defines ==================================== //
// Desc: This parameter constant controls the speed that the 'BoT "explores.".
//       NOTE: Try with '150', '250', '350', just for fun!
#define BOT_SPEED    300

// Desc: This parameter controls the number of steps needed for an 'approximate'
//       90-degree turn.  The values are slightly different for when non-zero
//       acceleration is in effect, or when it is disabled.  These values are
//       determined 'experimentally', but once found there's almost no need
//       to change it as result are fairly consistent.
//
// #define DEG_90  150      /* Good with no acceleration enabled. */
#define DEG_90  140         /* Good with acceleration enabled.    */
// =========================== declarations ================================= //
// Custom enumerated type declaration to specify the state of the 'bot.
typedef enum BOT_STATE_TYPE {

    BOT_IDLE = 0,       // The bot is in this state when it is doing nothing.
    BOT_EXPLORING,      // The bot is in this state when 'exploring'.
    BOT_OBJECT_FOUND    // The bot has collided with while 'exploring'.

} BOT_STATE;

// Custom enumerated type declaration to specify which IR sensors are triggered.
typedef enum IR_STATE_TYPE {

    IR_NONE = 0,        // NO IR sensor triggered.
    IR_LEFT,            // Left IR sensor triggered.
    IR_RIGHT,           // Right IR sensor triggered.
    IR_BOTH             // Both IR sensors triggered.

} IR_STATE;

// ============================== globals =================================== //

TIMEROBJ IR_time;       // This timer object controls how often the 'Tiny is
                        // polled for IR sensor status data.  We need to control
                        // the rate at which this happens to keep from over-
                        // whelming the ATtiny and keep SPI 'chatter' low.

volatile BOT_STATE bot_state = BOT_IDLE;  // This holds the 'bots current state.

// ============================ prototypes ================================== //

// *** Prototypes of 'Behavioral' Functions ***:
//
// Compound Behaviors:
void BHVR_explore( void );
void BHVR_avoid_obj( IR_STATE which_ir );

// Primitive Behaviors:
void PBHVR_move_wt( signed short int how_far );     
void PBHVR_turn_wt( signed short int how_much );
void PBHVR_move_nb( signed short int how_far );     // Non-blocking version.
void PBHVR_check_sensors( IR_STATE *pWhich_IR );

// *** Miscellaneous prototypes ***:
void init_bot( void );
// ============================ functions =================================== //
void init_bot( void )
{

    SUBSYS_OPENSTAT opstat;

    // Start by opening and initializing the needed subsystem modules:

    opstat = LCD_open();        // Open the LCD subsystem.
    opstat = LED_open();        // Open the LED subsystem.
    opstat = STEPPER_open();    // Open the STEPPER subsystem.

    // NOTE:  We also need IR-sensor status, which is provided by the ATtiny
    //        subsystem module.  This module is already open by default.

    // Set STEPPER parameters:
    STEPPER_set_mode( BOTH_STEPPERS, STEPPER_STEP_MODE );

    // Enable low-power mode for the steppers.
    STEPPER_set_pwr_mode( STEPPER_PWR_LOW );

    // Set stepper acceleration parameters.
    STEPPER_set_accel( BOTH_STEPPERS, 400 );    // 400 steps/sec^2.

} // end init_bot()
// -------------------------------------------------------------------------- //
void BHVR_explore( void )
{

    // Exploring means moving forward -- we invoke the 'primitive behavior'
    // to do so.  We invoke the non-blocking version so that 'BHVR_explore()'
    // doesn't BLOCK the state-machine looping engine in 'CBOT_main()'.
    PBHVR_move_nb( STEPPER_REVS( 20 ) );

} // end explore()
// -------------------------------------------------------------------------- //
void BHVR_avoid_obj( IR_STATE which_ir )
{

    // Turn the RED LED on to indicate we're avoiding an object.
    LED_state( LED_Red, LED_ON );

    // First we stop the stepper IMMEDIATELY as opposed to gradually
    // (especially with acceleration parameters being non-zero, which also
    // affect deceleration), so we engage the brakes.
    STEPPER_stop( BOTH_STEPPERS, STEPPER_BRK_ON );

    // Wait a bit 750ms.
    TMRSRVC_delay( 750 );

    // Then proceed...

    // We avoid an object according to the value given in 'which_ir'.  We
    // then invoke 'primitive' behaviors to perform 'avoidance'.

    switch( which_ir )
    {

        case IR_LEFT:

            // Back up.
            PBHVR_move_wt( STEPPER_REVS( -1 ) );

            // Turn left.
            PBHVR_turn_wt( -DEG_90 );

            // Move forward.
            PBHVR_move_wt( STEPPER_REVS( 1 ) );

            // Turn right.
            PBHVR_turn_wt( DEG_90 );

            // That's it.

            break;

        case IR_RIGHT:

            // Back up.
            PBHVR_move_wt( STEPPER_REVS( -1 ) );

            // Turn right.
            PBHVR_turn_wt( DEG_90 );

            // Move forward.
            PBHVR_move_wt( STEPPER_REVS( 1 ) );

            // Turn left.
            PBHVR_turn_wt( -DEG_90 );

            // That's it.
            
            break;

        case IR_BOTH:

            // Back up further.
            PBHVR_move_wt( STEPPER_REVS( -2 ) );

            // Turn right.
            PBHVR_turn_wt( DEG_90 );

            // Move forward.
            PBHVR_move_wt( STEPPER_REVS( 1 ) );

            // Turn left.
            PBHVR_turn_wt( -DEG_90 );

            // That's it.
            break;

        default:; // Do nothing.

    } // end switch()

    // Turn the Red LED off.
    LED_state( LED_Red, LED_OFF );

} // end BHVR_avoid_obj()
// -------------------------------------------------------------------------- //
void PBHVR_move_wt( signed short int how_far )
{

    if ( how_far > 0 )
    {
    
        // Just call the API 'step' function to do this.
        STEPPER_stepwt( BOTH_STEPPERS, STEPPER_FWD, how_far, 
                        BOT_SPEED, STEPPER_BRK_OFF );
                                                            
    } // end if()
    
    else if ( how_far < 0 )
    {
    
        // Just call the API 'step' function to do this.
        STEPPER_stepwt( BOTH_STEPPERS, STEPPER_REV, -1*how_far, 
                        BOT_SPEED, STEPPER_BRK_OFF );
    
    } // end else-if()
    
} // end move_forward()
// -------------------------------------------------------------------------- //
void PBHVR_turn_wt( signed short int how_much )
{

    STEPPER_NOTIFY tc;  // Flags for identifying the 'terminal count' for steps.
    
    if ( how_much != 0 )
    {
    
        if ( how_much > 0 )
        {
    
            // Move right stepper backward.
            STEPPER_stepnb( RIGHT_STEPPER, STEPPER_REV, how_much, 
                            BOT_SPEED, STEPPER_BRK_OFF, &tc );
                                            
            // Move left stepper forward.
            STEPPER_stepnb( LEFT_STEPPER, STEPPER_FWD, how_much, 
                            BOT_SPEED, STEPPER_BRK_OFF, &tc );
                                            
        } // end if()
    
        else if ( how_much < 0 )
        {
    
            // Move left stepper backward.
            STEPPER_stepnb( LEFT_STEPPER, STEPPER_REV, -1*how_much, 
                            BOT_SPEED, STEPPER_BRK_OFF, &tc );
                                            
            // Move right stepper forward.
            STEPPER_stepnb( RIGHT_STEPPER, STEPPER_FWD, -1*how_much, 
                            BOT_SPEED, STEPPER_BRK_OFF, &tc );    
                            
        } // end else-if()

        // Wait on the motion to complete.
        while( ( !tc.left ) || ( !tc.right ) );
    
    } // end if()
    
} // end turn()
// -------------------------------------------------------------------------- //
void PBHVR_move_nb( signed short int how_far )
{

    if ( how_far > 0 )
    {
    
        // Just call the API 'step' function to do this.
        STEPPER_stepnb( BOTH_STEPPERS, STEPPER_FWD, how_far, 
                        BOT_SPEED, STEPPER_BRK_OFF, NULL );
                                                            
    } // end if()
    
    else if ( how_far < 0 )
    {
    
        // Just call the API 'step' function to do this.
        STEPPER_stepnb( BOTH_STEPPERS, STEPPER_REV, -1*how_far, 
                        BOT_SPEED, STEPPER_BRK_OFF, NULL );
    
    } // end else-if()
    
} // end move_forward()
// -------------------------------------------------------------------------- //
void PBHVR_check_sensors( IR_STATE *pWhich_IR )
{

    unsigned char sensor_data;

    // Toggle the LED so we know this is being triggered.
    LED_toggle( LED_Green );

    // Get sensor data from the ATtiny.
    sensor_data = ATTINY_get_sensors();

    // First check if both IR's are triggered.
    if ( ( sensor_data & SNSR_IR_LEFT ) && ( sensor_data & SNSR_IR_RIGHT ) )
    {

        *pWhich_IR = IR_BOTH;

    } // end if()

    else if ( sensor_data & SNSR_IR_LEFT )
    {

        *pWhich_IR = IR_LEFT;

    } // end else-if()

    else if ( sensor_data & SNSR_IR_RIGHT )
    {

        *pWhich_IR = IR_RIGHT;

    } // end else-if()

    else
    {

        *pWhich_IR = IR_NONE;

    } // end else.

} // end PBHVR_check_sensors()
// ============================ CBOT_main =================================== //
void CBOT_main( void )
{

    IR_STATE which_ir;      // Variable to hold 'IR-state' info.

    // Initialize the 'bot as needed.
    init_bot();

    LCD_printf( "Starting bot...\n" );

    // Wait 3 seconds before starting.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Start the timing for the IR-sensor monitoring.  This will trigger
    // the internal timing flag to go off every 125ms.
    TMRSRVC_new( &IR_time, TMRFLG_NOTIFY_FLAG, TMR_TCM_RESTART, 125 );

    // Start the state machine.
    while( 1 )
    {

        // Process current state.
        switch( bot_state )
        {

            // If 'bot is IDLE, then have it explore.
            case BOT_IDLE:

                // Let the state variable reflect that the bot is exploring.
                bot_state = BOT_EXPLORING;

                LCD_printf_RC( 2, 0, "Exploring...\t" );

                BHVR_explore();

                break;

                // If the 'bot is EXPLORING...
            case BOT_EXPLORING:

                // Nothing to do here -- the 'bot is having fun.

                break;

                // If the 'bot has collided with an object, then it has to
                // avoid that object.
            case BOT_OBJECT_FOUND:

                LCD_printf_RC( 2, 0, "AARGH! Object found!\t" );

                BHVR_avoid_obj( which_ir );   
                                    // Unlike 'BHVR_explore()' which doesn't 
                                    // BLOCK, 'BHVR_avoid_obj()' will BLOCK 
                                    // until finished.  'BHVR_avoid_obj()' will
                                    // avoid depending on which IR sensor was
                                    // triggered.  This is given by 'which_ir'
                                    // variable.

                // Once we get to this point,
                // let the state variable reflect that the 'bot has completed
                // avoiding the object, so we let the state machine know that
                // the 'bot is now IDLE so that it can continue exploring once
                // again.
                bot_state = BOT_IDLE;

                break;

        } // end switch()

        // If it's time to do so, get sensor status and process that info.
        // Sensors are checked 8 times per second.
        TMRSRVC_on_TC( IR_time, PBHVR_check_sensors( &which_ir ));

        // We may need to change the state if a collision has occurred.
        if ( which_ir != IR_NONE )

            bot_state = BOT_OBJECT_FOUND;

    } // end while().
    
} // end CBOT_main()

