// Auth: Jose Santos
// Desc: Firmware implementation for the CEENBoT (TM) targeted for v2.21 board
//       which uses the ATMEL ATmega324 MCU.

#include "cbot324v221.h"

//------------------- CBOT_main:
void CBOT_main( void )
{

	SUBSYS_OPENSTAT ops;

	// Open the STEPPER, LCD, and LED subsystems.
	ops = STEPPER_open();
	ops = LCD_open();
	ops = LED_open();   // NOT USED.

	LCD_printf( "Starting...\n" );

	// Wait 3 seconds.
	TMRSRVC_delay_sec( 3 );

	// Move forward.
	STEPPER_move_stwt( STEPPER_BOTH, 
						STEPPER_FWD, 200, 200, 400, STEPPER_BRK_ON,   // Left
						STEPPER_FWD, 200, 200, 400, STEPPER_BRK_ON ); // Right

	// Wait 2 seconds.
	TMRSRVC_delay_sec( 2 );

	// Move back.
	STEPPER_move_stwt( STEPPER_BOTH, 
						STEPPER_REV, 200, 200, 400, STEPPER_BRK_OFF,   // Left
						STEPPER_REV, 200, 200, 400, STEPPER_BRK_OFF ); // Right

	LCD_printf( "Done.\n" );

	while( 1 );

} // end CBOT_main()

