// Desc: Sample program to test the 'GPI_move()' function, which implements
//	     the  'Move' tool in the GPI-GUI.  This particular sample shows how
//       use 'GPI_move()' in 'free-running' mode.

#include "capi324v221.h"
#include "gpi324v221.h"

// ============================== CBOT main ============================= //
void CBOT_main( void )
{

	// Start the GPI interface:
	GPI_MODULES gpi_modules;

	gpi_modules.GPI_step = TRUE;
	gpi_modules.GPI_lcd  = TRUE;
	gpi_modules.GPI_led  = TRUE;

	// Start the specified modules.
	GPI_open( &gpi_modules );

	// Display test is about to begin.
	GPI_display( GPI_DISPLINE0, "Starting..." );

	// Wait 3 seconds.
	GPI_delay( TMR_SECS( 3 ) );

	// Let's move both motors with independent settings.
	GPI_move( GPI_STEPPER_FREERUNNING, GPI_STEPPER_BOTH,

		// Left motor params:
		GPI_STEP_DIR_FWD,	// Stepper direction.
		0,					// Step distance not applicable in 'freerun'.
		200,				// Speed.
		400,				// Acceleration.
		GPI_STEP_BRK_OFF,	// Braking mode not applicable in 'freerun'.
		NULL,				// Stepper event not applicable in 'freerun'.

		// Right motor params:
		GPI_STEP_DIR_REV,	// Stepper direction.
		0,					// Step distance not applicable in 'freerun'.
		200,				// Speed.
		200,				// Acceleration.
		GPI_STEP_BRK_OFF,	// Braking mode not applicable in 'freerun'.
		NULL				// Stepper event not applicable in 'freerun'.

	); // end GPI_move().

	// The motors will run indefinitely until explicitly told to stop,
	// so let's wait 5 seconds -- then stop.
	GPI_delay( TMR_SECS( 5 ) );

	// Stop.
	GPI_stop( GPI_STEPPER_BOTH, GPI_STEP_BRK_OFF );

	// Clear the display.
	GPI_clrdisp();

	// Denote that we're done.
	GPI_display( GPI_DISPLINE0, "Test complete." );

	while( 1 );	// Don't leave.

} // end CBOT_main()
