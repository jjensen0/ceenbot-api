// Auth: Jose Santos
// Desc: Sample code using the CEENBoT (TM) API. It shows how to read the
//       sensor data consisting of 'IR (infra-red) sensors' and 'switch state'
//       by polling the ATtiny routines.

#include "capi324v221.h"
// ============================ globals ===================================== //
TIMEROBJ timer0;    // We need this here because both the 'timer0_callback()'
                    // and 'CBOT_main()' need access to this timer object.

// ============================ prototypes ================================== //
TMR_NR( timer0_callback );
// =========================== functions ==================================== //
TMR_NR( timer0_callback )
{

    unsigned char sensor_data;

    // Bit storage.
    unsigned char bit0 = 0;
    unsigned char bit1 = 0;
    unsigned char bit2 = 0;
    unsigned char bit3 = 0;
    unsigned char bit4 = 0;
    unsigned char bit5 = 0;
    unsigned char bit6 = 0;
    unsigned char bit7 = 0;

    // Toggle the LED to indicate the timer routine is running.
    LED_toggle( LED_Green );

    // Read sensor data.
    sensor_data = ATTINY_get_sensors();

    if ( sensor_data & 0x01 ) bit0 = 1;
    if ( sensor_data & 0x02 ) bit1 = 1;
    if ( sensor_data & 0x04 ) bit2 = 1;
    if ( sensor_data & 0x08 ) bit3 = 1;
    if ( sensor_data & 0x10 ) bit4 = 1;
    if ( sensor_data & 0x20 ) bit5 = 1;
    if ( sensor_data & 0x40 ) bit6 = 1;
    if ( sensor_data & 0x80 ) bit7 = 1;

    // Write always on second line of LCD.
    LCD_set_RC( 2, 0 );

    // Display state.
    LCD_printf( "Data: %d%d%d%d%d%d%d%d", bit7, bit6, bit5, bit4,
                                          bit3, bit2, bit1, bit0 );

} // end TMR_NR( timer0_callback )

// ============================ CBOT main =================================== //

void CBOT_main( void )
{

    ATTINY_FIRMREV rev;     // Hold ATtiny firmware revision.

    // Start up the needed modules:
    LED_open();         // Start the LED subsystem.

    LCD_open();         // Start the LCD subsystem.

    // Note to read sensor data the we do so by way of the ATtiny.
    // The ATtiny subsystem is already OPEN by default, so there's no
    // need to invoke 'ATTINY_open()'.

    // Attach the function to invoke upon timer TC (terminal count).
    timer0.pNotifyFunc = timer0_callback;

    // Let's make sure we can talk to the ATtiny first by reading
    // the tiny's firmware revision number.
    LCD_printf( "Getting tiny rev...\n" );

    TMRSRVC_delay( TMR_SECS( 1 ) );

    // Get the revision.
    ATTINY_get_firm_rev( &rev );

    // Display it.
    LCD_clear();
    LCD_printf( "Rev: %d.%d\n", rev.major, rev.minor );

    TMRSRVC_delay( TMR_SECS( 5 ) );

    LCD_clear();
    LCD_printf( "Starting test...\n" );

    TMRSRVC_delay( TMR_SECS( 3 ) );

    LCD_clear();
    LCD_printf( "Test started.\n" );

    // Trigger timer object function 8-times per second.
    TMRSRVC_new( &timer0, TMRFLG_NOTIFY_FUNC, TMR_TCM_RESTART, 125 );

    while( 1 );

} // end CBOT_main()
