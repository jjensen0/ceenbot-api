// Auth: Jose Santos
// Desc: This is the 'driver' program processing incoming commands from the
//       TI calculator.

#include "capi324v221.h"

#define DRIVER_REV_MAJ		1
#define DRIVER_REV_MIN		0
#define DRIVER_REV_BUILD	1
#define DRIVER_REV_STATUS	'b'

// ============================ prototypes ================================== //
TIMER_EVENT( lcd_toggle );
void TI_Get( void );
void TI_Send( int16_t *list, uint8_t len );
// ============================  functions ================================== //
TIMER_EVENT( lcd_toggle )
{
    
	/*
	static unsigned char toggle = 0;

	if ( ( toggle ^= 1 ) ) {

		LCD_putchar_XPG( 121, 3, '>' );
		LCD_printf( "\n" );

	}

	else {

		LCD_putchar_XPG( 121, 3, ' ' );
		LCD_printf( "\n" );

	}
	*/

	// TODO:  For the time being, let's just toggle the LED as 
	//        the code above is causing some 'minor' issues (they will be fixed).
	LED_toggle( LED_Green );

}

// ============================ CBOT main =================================== //
void CBOT_main( void )
{

	TIMEROBJ	lcd_timer;	// LCD timer object.
	//TIMEROBJ	ti_timer;	// TI timer for controlling how often 'TI_process_commands()'
						    // gets executed.

    SUBSYS_OPENSTAT ops;

    // Open needed modules.
    ops = LED_open();		// MUST be open, or TI_open() WILL fail!
    ops = LCD_open();		// MUST be open, or TI_open() WILL fail!
	ops = STEPPER_open();	// MUST be open, or TI_open() WILL fail!-
    // ops = UART_open( UART_UART0 );

	// Display the TI driver revision.
	LCD_printf( "\n" );
	LCD_printf( " TI Driver v%d.%02d.%03d%c", DRIVER_REV_MAJ,
											  DRIVER_REV_MIN,
											  DRIVER_REV_BUILD,
											  DRIVER_REV_STATUS );
	LCD_printf( "  (c) CEENBoT, Inc.  " );

	// Wait 4 seconds for splash-screen message.	
	TMRSRVC_delay( TMR_SECS( 2 ) );

	// Clear the display.
	LCD_clear();


	// Register timer event.
	TMRSRVC_REGISTER_EVENT( lcd_timer, lcd_toggle );

	// Start the timer event for the LED.
	TMRSRVC_new( &lcd_timer, TMRFLG_NOTIFY_FUNC, TMRTCM_RESTART, 
                                                                TMR_SECS( 1 ) );

    // Initialize the UART.
    // UART_configure( UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 
    //                                                                    57600 );

    // Enable the UART's transmitter.
    // UART_set_TX_state( UART_UART0, UART_ENABLE );

    // Print message on LCD.
    LCD_printf( "Starting Driver...\n" );

    // Notify we're about to open the TI subsystem module.
    // UART0_printf( "Opening TI subsystem module...\n" );

    // Set both parameters to 'NULL' to use the internal command set.
    ops = TI_open(NULL, NULL);

    if ( ops.state == SUBSYS_OPEN )
	{

		LCD_printf( "TI-mode OK.\n" );
        //UART0_printf( "TI module OK.\n" );
		//UART0_printf( "TI module open and ready!\n" );

	} //end if()

    else
	{

		LCD_printf( "TI-mode ERROR.\n" );
		//UART0_printf("Unable to open module -- Port Attachment Error.\n");
        //UART0_printf("**TI module ERROR!**\n");

	} // end else.

	// Finally, start the timer event for controlling the frequency of execution
	// for the 'TI_process_commands()' function.  We'll begin the 'test' by running
	// the timer every 10ms.  That means the command buffer will check for new commands
	// 100 times/sec, this should give us acceptable response while not using every
	// processor cycle available to run the while() loop unnecessarily.
	// TMRSRVC_new( &ti_timer, TMRFLG_NOTIFY_FLAG, TMRTCM_RESTART, 10 );

	if ( ops.state == SUBSYS_OPEN )

		LCD_printf( "Awaiting Commands...\n" );

    // Don't leave.
    while( 1 ) {
		
		TI_process_commands();

	} // end while()
        

} // end CBOT_main()

