// Auth: Jose Santos
// Desc: This is the 'driver' program processing incoming commands from the
//       TI calculator.

#include "capi324v221.h"

#define DRIVER_REV_MAJ		1
#define DRIVER_REV_MIN		0
#define DRIVER_REV_BUILD	0
#define DRIVER_REV_STATUS	'b'

// ============================ prototypes ================================== //
TIMER_EVENT( lcd_toggle );
void TI_Get( void );
void TI_Send( int16_t *list, uint8_t len );
// ============================  functions ================================== //
TIMER_EVENT( lcd_toggle )
{

	static unsigned char toggle = 0;

	if ( ( toggle ^= 1 ) )

		LCD_putchar_XPG( 121, 3, '>' );

	else

		LCD_putchar_XPG( 121, 3, ' ' );

}

// ============================ CBOT main =================================== //
void CBOT_main( void )
{

	TIMEROBJ	lcd_timer;	// LCD timer object.

    SUBSYS_OPENSTAT ops;

    // Open needed modules.
    ops = LED_open();		// MUST be open, or TI_open() WILL fail!
    ops = LCD_open();		// MUST be open, or TI_open() WILL fail!
	ops = STEPPER_open();	// MUST be open, or TI_open() WILL fail!-

	// Display the TI driver revision.
	LCD_printf( "\n" );
	LCD_printf( " TI Driver v%d.%02d.%03d%c", DRIVER_REV_MAJ,
											  DRIVER_REV_MIN,
											  DRIVER_REV_BUILD,
											  DRIVER_REV_STATUS );
	LCD_printf( "  (c) CEENBoT, Inc.  " );

	// Wait 4 seconds for splash-screen message.	
	TMRSRVC_delay( TMR_SECS( 4 ) );

	// Clear the display.
	LCD_clear();


	// Register timer event.
	TMRSRVC_REGISTER_EVENT( lcd_timer, lcd_toggle );

	// Start the timer event for the LED.
	TMRSRVC_new( &lcd_timer, TMRFLG_NOTIFY_FUNC, TMRTCM_RESTART, 
                                                                TMR_SECS( 1 ) );
   
    // Print message on LCD.
    LCD_printf( "Starting Driver...\n" );

    // Set both parameters to 'NULL' to use the internal command set.
    ops = TI_open( NULL, NULL );

    if ( ops.state == SUBSYS_OPEN )

		LCD_printf( "TI-mode OK.\n" );

    else

		LCD_printf( "TI-mode ERROR.\n" );


	if ( ops.state == SUBSYS_OPEN )

		LCD_printf( "Awaiting Commands...\n" );

    // Don't leave.
    while( 1 ) {
		
		TI_process_commands();

	} // end while()
        

} // end CBOT_main()

