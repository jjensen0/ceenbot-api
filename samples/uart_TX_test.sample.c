// Auth: Jose Santos
// Desc: Sample program on using the UART subsystem module.

#include "capi324v221.h"

void CBOT_main( void )
{

    unsigned int char_count = 0;

    SUBSYS_OPENSTAT ops;

    // Open the needed modules.
    ops = LED_open();
    ops = LCD_open();
    ops = UART_open( UART_UART0 );

    // Configure the UART:
    //
    // Data Size = 8-bits
    // Stop Bits = 1
    // Parity    = None
    // Baud      = 57600bps
    UART_configure( UART_UART0, UART_8DBITS, UART_1SBIT, UART_EVEN_PARITY, 
                                                                       57600 );

    // Enable the transmitter.
    UART_set_TX_state( UART_UART0, UART_ENABLE );

    // Let's see how that goes.
    // Start sending characters once every second.
    while( 1 ) 
    {

        // Transmit the character.
        UART_transmit( UART_UART0, 'X' );

        // Increment character count and print.
        LCD_printf_RC( 3, 0, "Sent #%d\t", ++char_count );
        

        // Wait 1 second.
        TMRSRVC_delay_sec( 1 );

    } // end while()

} // end CBOT_main()
