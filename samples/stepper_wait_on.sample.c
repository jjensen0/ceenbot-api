// Desc: This sample program shows how to use the non-blocking versions of
//       the stepper functions and how to use the 'STEPPER_wait_on()' 
//	     functions to wait on motion completion of the steppers.

#include "capi324v221.h"

// ============================= CBOT main ============================== //
void CBOT_main( void )
{

	SUBSYS_OPENSTAT opstat;

	// Initialize needed modules.
	opstat = LED_open();
	opstat = LCD_open();
	opstat = STEPPER_open();

	// Set the stepper mode.
	STEPPER_set_mode( BOTH_STEPPERS, STEPPER_STEP_MODE );

	// Set the acceleration.
	STEPPER_set_accel( LEFT_STEPPER, 500 );
	STEPPER_set_accel( RIGHT_STEPPER, 100 );

	LCD_printf( "Starting test...\n" );

	// Wait 3 seconds.
	TMRSRVC_delay( TMR_SECS( 3 ) );

	// Clear the LCD.
	LCD_clear();

	// Set the motions.
	STEPPER_stepnb( RIGHT_STEPPER, STEPPER_FWD, 600, 200, STEPPER_BRK_ON );
	STEPPER_stepnb( LEFT_STEPPER, STEPPER_REV, 400, 200, STEPPER_BRK_ON );

	// Turn the green led on to indicate parameters are set and that
	// the stepper motors should be running.
	LED_state( LED_Green, LED_ON );

	// Then, we wait for both motions to complete.
	STEPPER_wait_on( BOTH_STEPPERS );

	// Turn Red LED on to indicate the brakes are engaged.
	LED_state( LED_Red, LED_ON );

	// Turn the Green LED off to indicate motion is off and we have 
	// reached this point in the code.
	LED_state( LED_Green, LED_OFF );

	// Wait 3 seconds before 'disengaging' brakes.
	TMRSRVC_delay( TMR_SECS( 3 ) );

	// Disengage brakes.
	STEPPER_stop( BOTH_STEPPERS, STEPPER_BRK_OFF );

	// Turn the Red LED off to indcate brakes SHOULD be off.
	LED_state( LED_Red, LED_OFF );

	// Print message.
	LCD_printf( "Test complete.\n" );

	// Don't leave.
	while( 1 );

} // end CBOT_main()
