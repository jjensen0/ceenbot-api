// Desc: Sample program to test the I2C module.

#include "capi324v221.h"
#include "__csnsr324v221.h"

// ============================= prototypes ================================= //
void do_LED_test( void );
void sample_CSNSR( void );

// =============================== functions ================================ //
void do_LED_test( void )
{

    unsigned int i = 0;

    LCD_printf( "LED Test started.\n" );

    for ( i = 0; i < 6; i++ )
    {

        TBV( 5, PORTA );

        // Wait 1 second.
        TMRSRVC_delay( TMR_SECS( 1 ) );

    } // end for()

    LCD_printf( "LED Test complete.\n" );

    TMRSRVC_delay( TMR_SECS( 3 ) );

} // end do_LED_test()
// -------------------------------------------------------------------------- //
void sample_CSNSR( void )
{

    CSNSR_DATA csensor_data;
    CSNSR_STAT csensor_status;

    static unsigned short int r_max = 0;
    static unsigned short int b_max = 0;
    static unsigned short int g_max = 0;
    static unsigned short int w_max = 0;

    // Toggle LED so we know this is actively working.
    LED_toggle( LED_Green );

    csensor_status = CSNSR_read( &csensor_data );

    // Use RED LED to indicate an error.
    if ( csensor_status == CSNSR_ERROR )

        LED_state( LED_Red, LED_ON );

    else

        LED_state( LED_Red, LED_OFF );

    // Process maximums.
    if ( csensor_data.rgb.red     > r_max ) r_max = csensor_data.rgb.red;
    if ( csensor_data.rgb.green   > g_max ) g_max = csensor_data.rgb.green;
    if ( csensor_data.rgb.blue    > b_max ) b_max = csensor_data.rgb.blue;
    if ( csensor_data.clear.white > w_max ) w_max = csensor_data.clear.white;

    LCD_printf_RC( 3, 0, "R: %d : %d (max)\t", csensor_data.rgb.red,
                                                 r_max );

    LCD_printf_RC( 2, 0, "G: %d : %d (max)\t", csensor_data.rgb.green,
                                                 g_max );

    LCD_printf_RC( 1, 0, "B: %d : %d (max)\t", csensor_data.rgb.blue,
                                                 b_max );

    LCD_printf_RC( 0, 0, "W: %d : %d (max)\t", csensor_data.clear.white,
                                                 w_max );

} // end sample_CSNSR()
// =============================== CBOT_main() ============================== //
void CBOT_main( void )
{

    CSNSR_DATA      csensor_data;
    CSNSR_STAT      csensor_status;
    TIMEROBJ        sensor_time;

    SUBSYS_OPENSTAT opstat;
    I2C_STATUS      i2c_stat = 0;

    unsigned char   i2c_data = 0;
    unsigned char   data = 0;

    // Start the needed modules.
    opstat = LED_open();
    opstat = LCD_open();    // Open the LCD subsystem.
    opstat = I2C_open();    // Open the I2C subsystem.
    opstat = CSNSR_open();  // Open the COLOR SENSOR module.

    // Configure the pins.
    // NOTE: Pins configured by 'CSNSR_open()'.

    // Wait...
    LCD_printf( "About to start..." );

    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Clear the screen before starting.
    LCD_clear();

    // Perform LED_check.
    do_LED_test();

    // Clear the LCD.
    LCD_clear();

    LCD_printf( "I2C test...\n" );
    LCD_printf( "Setting gain...\n" );

    // Set the gain capacitors.
    csensor_status = CSNSR_set_gain_caps( 15, 15, 15, 15 );

    if ( csensor_status == CSNSR_ERROR )

        LED_state( LED_Red, LED_ON );

    LCD_printf( "Done.\n" );

    TMRSRVC_delay( TMR_SECS( 1 ) );        

    LCD_printf( "Setting int time...\n" );

    csensor_status = CSNSR_set_int_time( 1000, 1000, 1000, 1000 );

    if ( csensor_status == CSNSR_ERROR )

        LED_state( LED_Red, LED_ON );

    LCD_printf( "Done.\n" );

    // Wait 3 seconds.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    LCD_clear();
    TMRSRVC_new( &sensor_time, TMRFLG_NOTIFY_FLAG, TMRTCM_RESTART, 125 );

    // Turn the color sensor's LED.
    SBV( 5, PORTA );

    // Enter the while loop.
    while ( 1 )
    {

        // Only do this every 125ms (8-times/sec).
        TMRSRVC_on_TC( sensor_time, sample_CSNSR() );

    } // end while()

} // end CBOT_main()
