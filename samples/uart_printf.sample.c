// Auth: Jose Santos
// Desc: Sample program on using the UART subsystem module.

#include "capi324v221.h"

void CBOT_main( void )
{

    unsigned int  char_count = 0;

    SUBSYS_OPENSTAT   ops;

    // Open the needed modules.
    ops = LED_open();
    ops = LCD_open();
    ops = UART_open( UART_UART0 );

    // Configure the UART:
    //
    // Data Size = 8-bits
    // Stop Bits = 1
    // Parity    = None
    // Baud      = 9600bps
    UART_configure( UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 
                                                                       57600 );

    // Enable the transmitter receiver.
    UART_set_TX_state( UART_UART0, UART_ENABLE );

    // Let's see how that goes.
    // Start receiving characters.
    while( 1 ) 
    {

        LED_toggle( LED_Green );

        // Print via UART0.
        UART_printf( UART_UART0, "Hello, Dolly! #%d, 0x%X\n", char_count,
                                                              char_count );

        LCD_printf_RC( 3, 0, "Hello, Dolly! #%d\t",     char_count );

        // Increment.
        char_count++;

        // Wait 1 second.
        TMRSRVC_delay( TMR_SECS( 1 ) );

    } // end while()

} // end CBOT_main()
