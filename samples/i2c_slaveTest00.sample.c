// Auth: Jose Santos
// Desc: Sample program to test I2C module.  This programs starts the MASTER
//       as transmitter, and the SLAVE as receiver.

typedef unsigned short int USHORT;
typedef unsigned char      UBYTE;

#include "capi324v221.h"

// -----------------------  Defines:
//#define __I2C_MASTER
#define __I2C_SLAVE_ADDR	0x74	/* I2C device address when in SLAVE mode. */

#define __I2C_STATUS()		( ( TWSR ) & 0xF8 )
// -----------------------  Prototypes:
void I2C_master_test( void );
void I2C_slave_test( void );

// -----------------------  ISRs:
ISR( TWI_vect )
{
	// For receiving data.
	UBYTE data = 0;

	// Print debugging info via UART.
	UBYTE __raw_status = __I2C_STATUS();

	UART_printf( UART_UART0,
		"TWI_vect: Triggered with status = 0x%X.\n", __raw_status );

	// Print it on the LCD display.
	LCD_printf_RC( 2, 0, "Trig. Stat = 0x%X.\t", __raw_status );

	// Reply back with a 'get'.
	I2C_SLVE_get( &data, TRUE );

	// Read the status again.
	__raw_status = __I2C_STATUS();

	// Print it via UART.
	UART_printf( UART_UART0, 
		"Slave: Received Data = 0x%X with status = 0x%X.\n", data, __raw_status );

	// Print it on the LCD display.
	LCD_printf_RC( 1, 0, "Dt = 0x%X, St = 0x%X.\t", data, __raw_status );

	if ( __raw_status == 0xA0 )
	{

		I2C_SLVE_finished( TRUE );

		UART_printf( UART_UART0, "Slave: STOP.\n" );

	} // end if()

	
	// Wait 1 second before leaving to avoid a high
	// interrupt rate.
	// TMRSRVC_delay( TMR_SECS( 250 ) );

} // end ISR( TWI_vect )

// -----------------------  Functions:
void I2C_master_test( void )
{

	I2C_STATUS status;

	// Enable the pullups for the MASTER device.
	I2C_pullup_enable();

	LCD_printf_RC( 3,0, "Started as MASTER.\n" );	

	// Wait one second before starting.
	TMRSRVC_delay( TMR_SECS( 1 ) );

	// Let's start with a sequence.
	status = I2C_MSTR_start( __I2C_SLAVE_ADDR, I2C_MODE_MT );

	if ( status != I2C_STAT_OK )
	{

		UART_printf( UART_UART0, "Master: ERROR: Starting.\n" );
		LCD_printf_RC( 2, 0, "ERROR: Starting.\t" );

	} // end if()
	else
	{

		// Send it data.
		I2C_MSTR_send( 0xA5 );

		// Then STOP.
		I2C_MSTR_stop();

	}
	

} // end I2C_master_test()
// ----------------------- //
void I2C_slave_test( void )
{

	LCD_printf_RC( 3,0, "Started as SLAVE.\n" );

	TMRSRVC_delay( 250 );

	// If NOT busy, then configure the SLAVE device.
	if ( I2C_IsBusy() == FALSE )
	{

		// Configure the I2C slave and enable it.
		I2C_SLVE_enable( __I2C_SLAVE_ADDR );

	} // end if()

} // end I2C_slave_test()

// -----------------------  CBoT Main:

void CBOT_main( void )
{

	// Open the needed modules:
	LED_open();
	LCD_open();
	UART_open( UART_UART0 );

	// Configure the UART device.
	UART_configure( UART_UART0, 
						UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 57600 );

	// Enable the transmitter portion of the UART device.
	UART_set_TX_state( UART_UART0, UART_ENABLE );

	// Open the I2C subsystem module.
	UART_printf( UART_UART0, "Opening I2C device...\n" );

	I2C_open();		// Open the I2C device (by default it is configured
	                // to operate at 100Khz.

	// Check if the device is busy.
	if ( I2C_IsBusy() )

		UART_printf( UART_UART0, "I2C is BUSY.\n" );

	else

		UART_printf( UART_UART0, "I2C is NOT busy.\n" );

	UART_printf( UART_UART0, "I2C should be ready.\n" );

#ifdef __I2C_MASTER

	I2C_master_test();

#else

	// Wait 1 second before starting to let the MASTER be fully
	// configured.
	I2C_slave_test();

#endif

	while( 1 );


} // end CBOT_main()
