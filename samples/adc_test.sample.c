// Auth: Jose Santos
// Desc: Sample on using the ADC module.

#include "capi324v221.h"
// ------------------- prototypes:
TIMER_EVENT( adc_sample_event );

// ------------------- functions:
TIMER_EVENT( adc_sample_event )
{

    ADC_SAMPLE sample_vbatt;
    ADC_SAMPLE sample_vchrg;

    float VBatt    = 0;
    float VCharger = 0;

    // Toggle the LED so we know this is working.
    LED_toggle( LED_Green );

    // Switch to channel 0 to read Charger Voltage.
    ADC_set_channel( ADC_CHAN0 );

    // Read the sample from the ADC.
    sample_vchrg = ADC_sample();

    // Compute the corresponding voltage.
    VCharger = (( ( float ) sample_vchrg ) / 330.0f ) * 5.0f;

    // Switch to channel 1 to read Battery Voltage.
    ADC_set_channel( ADC_CHAN1 );

    // Read the sample from the ADC.
    sample_vbatt = ADC_sample();

    // Compute the corresponding voltage.
    VBatt = (( ( float ) sample_vbatt ) / 330.0f ) * 5.0f;

    LCD_printf_RC( 3, 0, "Sample = %d, %.2fV\t", 
                                            sample_vbatt, ( double ) VBatt );

    LCD_printf_RC( 2, 0, "Sample = %d, %.2fV\t", 
                                            sample_vchrg, ( double ) VCharger );

} // end adc_sample_timer()

// ------------------- CBOT-main:
void CBOT_main( void )
{

    SUBSYS_OPENSTAT ops; 
    TIMEROBJ        adc_sample_timer;

    // Open the ADC.
    ops = LED_open();
    ops = LCD_open();
    ops = ADC_open();

    // *** Setup the ADC ***:

    // Set the 'VREF' for the ADC.
    ADC_set_VREF( ADC_VREF_AVCC );

    // Set the ADC Channel to monitor the Battery Voltage.
    ADC_set_channel( ADC_CHAN1 );

    // Notify we're about to start.
    LCD_printf( "Starting..." );

    // Wait 3 seconds.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Clear the display.
    LCD_clear();

    // Attach our timer event to the timer object.
    TMRSRVC_REGISTER_EVENT( adc_sample_timer, adc_sample_event );

    // Start the timer to tick every second.
    TMRSRVC_new( &adc_sample_timer, TMRFLG_NOTIFY_FUNC, TMRTCM_RESTART, 1000 ); 

    // Don't leave.
    while( 1 )

        /* NULL */ ;


} // end CBOT_main()
