// Auth: Jose Santos
// Desc: Sample code using the CEENBoT (TM) API.  This particlar sample shows
//       how to interface with the PSX controller to read analog data, map the
//       controller values to a useful range and achieve all of this while 
//       taking advantage of the timer service facility.  This controller
//       data is then used to control the stepper motors.  In this version, 
//       the timer facility invokes the timer routine (which does the actual
//       reading of the controller) eight times per second.  It blinks the 
//       Green LED on and then off on each 'read'.  If the controller doesn't 
//       provide valid analog data, the Red LED goes ON.  Once the controller
//       is placed in 'Analog Mode', the Red LED should go OFF (provided 
//       incoming data is validated).  Press the 'start' button on the PSX
//       controller to stop the test sequence.

#include "capi324v221.h"
// ============================ globals ===================================== //
TIMEROBJ timer0;    // We need this here because both the 'timer0_callback()'
                    // and 'CBOT_main()' need access to this timer object.

// ============================ prototypes ================================== //
TMR_NR( timer0_callback );
// =========================== functions ==================================== //
TMR_NR( timer0_callback )
{

    signed short int aleft_UD;      // Analog left stick  UP/DOWN data.
    signed short int aright_UD;     // Analog right stick UP/DOWN data.

    PSXC_STDATA controller;

    // Toggle LED to show we're processing.
    LED_toggle( LED_Green );

    // Read controller data AND make sure it constitutes analog data.
    if ( ( PSXC_read( &controller ) == TRUE ) && 
         ( controller.data_type == PSXC_ANALOG )  )
    {

        // Turn RED LED OFF to indicate there is NO problem.
        LED_state( LED_Red, LED_OFF );

        // Map to suitable speed range.  The PSXC routines output analog
        // range from -128 to 127, while stepper functions can take values
        // between 0 to 300 or so, so we need to map the 30-120 range to 0-300.
        // We start from 30 instead of 0 because we need that 'buffer zone' that
        // acts as the 'dead zone'.
        aleft_UD = PSXC_plinear_map( controller.left_joy.up_down,
                                     30, 120, 300 );

        aright_UD = PSXC_plinear_map( controller.right_joy.up_down,
                                      30, 120, 300 );

        // Stop the test if the start button is pushed.
        if ( ( ~controller.buttons0 ) & STRT_BIT )
        {

            // Clear the LCD.
            LCD_clear();

            // Notify the test has stopped.
            LCD_printf( "Test stopped." );

            // Turn off the LED for good.
            LED_state( LED_Green, LED_OFF );

            // Stop this service from ticking.
            TMRSRVC_stop_timer( &timer0 );

        } // end if()

    } // end if()

    // Otherwise, if controller data is not valid...
    else
    {

        // Turn Red LED ON to indicate we have a problem.
        LED_state( LED_Red, LED_ON );

        // Set to safe values, so our 'bot doesn't go crazy.
        aleft_UD  = 0;
        aright_UD = 0;

    } // end else.

    // Right motor process.
    if ( aright_UD > 0 )

        STEPPER_run( RIGHT_STEPPER, STEPPER_FWD, aright_UD );

    else if ( aright_UD < 0 )

        STEPPER_run( RIGHT_STEPPER, STEPPER_REV, -1*aright_UD );

    else

        STEPPER_stop( RIGHT_STEPPER, STEPPER_BRK_OFF );


    // Left motor process.

    if ( aleft_UD > 0 )

        STEPPER_run( LEFT_STEPPER, STEPPER_FWD, aleft_UD );

    else if ( aleft_UD < 0 )

        STEPPER_run( LEFT_STEPPER, STEPPER_REV, -1*aleft_UD );

    else

        STEPPER_stop( LEFT_STEPPER, STEPPER_BRK_OFF );

} // end TMR_NR( timer0_callback )

// ============================ CBOT main =================================== //

void CBOT_main( void )
{

    // Start up the needed modules:
    LED_open();         // Start the LED subsystem.

    LCD_open();         // Start the LCD subsystem.

    STEPPER_open();     // Start the STEPPER subsystem.

    PSXC_open();        // Start the PSX Controller subsystem.

    // Attach the function to invoke upon timer TC (terminal count).
    timer0.pNotifyFunc = timer0_callback;

    // Set up the steppers.
    STEPPER_set_mode( BOTH_STEPPERS, STEPPER_NORMAL_MODE );     // Set op. mode.
    STEPPER_set_pwr_mode( STEPPER_PWR_LOW );    // Enable power-saving features.
    STEPPER_set_accel( BOTH_STEPPERS, 500 );    // Set suitable acceleration.

    // Tell user we're about to start.
    LCD_printf( "Starting test...\n" );

    // Wait 3 seconds.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Start it -- have the function trigger 8 times/sec (that's every 125ms).
    TMRSRVC_new( &timer0, TMRFLG_NOTIFY_FUNC, TMR_TCM_RESTART, 125 );

    // Tell the user the test is running.
    LCD_clear();
    LCD_printf( "Running...\n" );

    while( 1 );

} // end CBOT_main()
