// Auth: Jose Santos
// Desc: Sample program for testing out the 'stopwatch' service.

#include "capi324v221.h"

// ============================= defines ==================================== //
// =========================== declarations ================================= //
// ============================== globals =================================== //
// ============================ prototypes ================================== //

// *** Miscellaneous prototypes ***:
void init_bot( void );
// ============================ functions =================================== //
void init_bot( void )
{

    SUBSYS_OPENSTAT opstat;

    // Start by opening and initializing the needed subsystem modules:

    opstat = LCD_open();        // Open the LCD subsystem.
    opstat = LED_open();        // Open the LED subsystem.
    opstat = STOPWATCH_open();  // Open the STOPWATCH service.

} // end init_bot()
// ============================ CBOT_main =================================== //
void CBOT_main( void )
{

    SWTIME sw_time; // Stop stopwatch value.

    unsigned long int time_us; // Used to store number of microseconds.

    // Initialize.
    init_bot();

    // Reset the stopwatch.
    STOPWATCH_reset();


    // Notify we're about to start the test.
    LCD_printf( "Starting test.\n" );
    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Turn the Green LED on so we know we're measuring time.
    LED_state( LED_Green, LED_ON );

    // Start the stopwatch.
    STOPWATCH_start();

    // Wait 100ms.

#ifndef __DEBUG    
    TMRSRVC_delay( 100 );
#endif

    sw_time = STOPWATCH_stop();

    // Convert to microseconds.
    time_us = (( unsigned long int )( sw_time )) * 10;

    // Turn the Green LED off so we know we're done measuring time.
    LED_state( LED_Green, LED_OFF );

    // Print the results.
    LCD_clear();
    LCD_printf( "sw_time = %d\n",  sw_time );
    LCD_printf( "t = %ldus\n",     time_us );

    // ***Do it again***
    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Reset the timer.
    STOPWATCH_reset();

    // Turn the LED ON so we know we're counting.
    LED_state( LED_Green, LED_ON );

    // Start the stopwatch.
    STOPWATCH_start();

    // Wait 1ms.
#ifndef __DEBUG
    TMRSRVC_delay( 1 );
#endif

    sw_time = STOPWATCH_stop();

    // Convert to microseconds.
    time_us = (( unsigned long int )( sw_time )) * 10;

    // Turn the Green LED off so we know we're done measuring time.
    LED_state( LED_Green, LED_OFF );

    // Print the results.
    LCD_printf( "sw_time = %d\n",  sw_time );
    LCD_printf( "t = %ldus\n",     time_us );

    while( 1 );  // Don't leave.

} // end CBOT_main()

