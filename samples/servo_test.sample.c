// Auth: Jose Santos
// Desc: Sample code using the CEENBoT (TM) API.

#include "capi324v221.h"
// ============================ globals ===================================== //
TIMEROBJ timer0;    // We need this here because both the 'timer0_callback()'
                    // and 'CBOT_main()' need access to this timer object.

// ============================ prototypes ================================== //
TMR_NR( timer0_callback );
// =========================== functions ==================================== //
TMR_NR( timer0_callback )
{

    static unsigned int RCS0_pos = SERVO_POS_MIN_LIMIT;

    // Toggle the GREEN LED so we know this is running.
    LED_toggle( LED_Green );

    // Display current position.
    LCD_printf_RC( 0, 0, "Pos = %d\t", RCS0_pos );

    // Set RC position.
    ATTINY_set_RC_servos( RCS0_pos, 0, 0, 0, 0 );

    // Get sensor data.
    ATTINY_get_sensors();

    // If we still have range, then move to the next position.
    if ( RCS0_pos < SERVO_POS_MAX_LIMIT )

        RCS0_pos++;

    else
    {

        LCD_clear();
        LCD_printf( " Test stopped.\n" );
        TMRSRVC_stop_timer( &timer0 );

    } // end if()

} // end TMR_NR( timer0_callback )

// ============================ CBOT main =================================== //

void CBOT_main( void )
{

    ATTINY_FIRMREV revision;

    // Attach notify function.
    timer0.pNotifyFunc = timer0_callback;

    // Start up the needed modules:
    LED_open();         // Start the LED subsystem.

    LCD_open();         // Start the LCD subsystem.

    // Note to read sensor data the we do so by way of the ATtiny.
    // The ATtiny subsystem is already OPEN by default, so there's no
    // need to invoke 'ATTINY_open()'.

    LCD_printf( "Getting revision...\n" );

    // Give it time.
    TMRSRVC_delay( TMR_SECS( 1 ) );

    // Just get the revision.
    ATTINY_get_firm_rev( &revision );

    // Display the revision.
    LCD_clear();

    LCD_printf( "Firm. Rev: %d.%.2d\n", revision.major,
                                        revision.minor );

    // Wait.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    LCD_printf( "Starting RCS test...\n" );

    // Wait again.
    TMRSRVC_delay( TMR_SECS( 3 ) );

    // Start the test routine.
    TMRSRVC_new( &timer0, TMRFLG_NOTIFY_FUNC, TMR_TCM_RESTART, 125 );

    while( 1 );

} // end CBOT_main()
