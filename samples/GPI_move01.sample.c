// Desc: Sample program to test the 'GPI_move()' function, which implements
//	     the  'Move' tool in the GPI-GUI.  This particular example shows how
//	     to invoke 'GPI_move()' using the BLOCKING STEP mode (by specifying
//		 'GPI_STEPPER_BLOCK' for the 'run_mode' in the 'GPI_move()' function
//	     as shown below.

#include "capi324v221.h"
#include "gpi324v221.h"

// ============================== CBOT main ============================= //
void CBOT_main( void )
{

	// Start the GPI interface:
	GPI_MODULES gpi_modules;

	gpi_modules.GPI_step = TRUE;
	gpi_modules.GPI_lcd  = TRUE;
	gpi_modules.GPI_led  = TRUE;

	// Start the specified modules.
	GPI_open( &gpi_modules );

	// Display test is about to begin.
	GPI_display( GPI_DISPLINE0, "Starting..." );

	// Wait 3 seconds.
	GPI_delay( TMR_SECS( 3 ) );

	// Let's move both motors with independent settings.
	GPI_move( GPI_STEPPER_BLOCK, GPI_STEPPER_BOTH,

		// Left motor params:
		GPI_STEP_DIR_FWD,	// Stepper direction.
		600,				// Step distance ( 600 = 3 revolutions ).
		250,				// Speed.
		400,				// Acceleration.
		GPI_STEP_BRK_OFF,	// Brakes off upon motion completion.
		NULL,				// No 'stepper event' upon completion.

		// Right motor params:
		GPI_STEP_DIR_REV,	// Stepper direction.
		600,				// Step distance ( 600 = 3 revolutions ).
		100,				// Speed.
		200,				// Acceleration.
		GPI_STEP_BRK_OFF,	// Brakes off upon motion completion.
		NULL				// No 'stepper event' upon completion.

	); // end GPI_move().

	// Clear the display.
	GPI_clr_disp();

	// Denote that we're done.
	GPI_display( GPI_DISPLINE0, "Test complete." );

	while( 1 );	// Don't leave.

} // end CBOT_main()
