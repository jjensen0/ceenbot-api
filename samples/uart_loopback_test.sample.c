// Auth: Jose Santos
// Desc: Sample program on using the UART subsystem module.

#include "capi324v221.h"

void CBOT_main( void )
{

    unsigned int  char_count = 0;
    unsigned char uart_data  = 0;

    SUBSYS_OPENSTAT   ops;
    UART_COMM_RESULT  ucs;

    // Open the needed modules.
    ops = LED_open();
    ops = LCD_open();
    ops = UART_open( UART_UART0 );

    // Configure the UART:
    //
    // Data Size = 8-bits
    // Stop Bits = 1
    // Parity    = None
    // Baud      = 9600bps
    UART_configure( UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 
                                                                       57600 );

    // Enable the transmitter receiver.
    UART_set_RX_state( UART_UART0, UART_ENABLE );
    UART_set_TX_state( UART_UART0, UART_ENABLE );

    // Let's see how that goes.
    // Start receiving characters.
    while( 1 ) 
    {

        // Check if there's incoming data to read.
        if ( UART_has_data( UART_UART0 ) )
        {

            // Read the data and read the status.
            ucs = UART_receive( UART_UART0, &uart_data );

            // If a timeout...
            if ( ucs == UART_COMM_TIMEOUT )
            {

                LCD_clear();
                LCD_printf( "TIMEOUT ERROR!" );

            } // end if()

            // Otherwise...
            else
            {

                // Print the data.
                LCD_printf( "%c", uart_data );

                // Send it back!  (LOOPBACK!)
                UART_transmit( UART_UART0, uart_data );

            }

        } // end if()

    } // end while()

} // end CBOT_main()
