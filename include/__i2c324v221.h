/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private header file for the I2C (TWI) API subsystem module.

#ifndef ____I2C324V221_H__
#define ____I2C324V221_H__

// ========================= private defines ================================ //
#define __I2C_SCL_OUT_PIN   0
#define __I2C_SDA_OUT_PIN   1
#define __I2C_OUT_PORT  PORTC

// Desc: Status codes used in Master-Transmit (MT) Mode.
#define __I2C_STAT_START_OK             0x08
#define __I2C_STAT_RESTART_OK           0x10
#define __I2C_STAT_MT_SLAW_ACK_OK       0x18
#define __I2C_STAT_MT_SLAW_NO_ACK       0x20
#define __I2C_STAT_MT_DATA_ACK_OK       0x28
#define __I2C_STAT_MT_DATA_NO_ACK       0x30
#define __I2C_STAT_ARB_ERROR            0x38

// Desc: Status codes used in Master-Receive (MR) Mode.
#define __I2C_STAT_MR_SLAR_ACK_OK       0x40
#define __I2C_STAT_MR_SLAR_NO_ACK       0x48
#define __I2C_STAT_MR_DATA_ACK_OK       0x50
#define __I2C_STAT_MR_DATA_NO_ACK       0x58

// Desc: Status codes used in Slave-Receive (SR) Mode.
#define __I2C_STAT_SR_SLAW_ACK_OK       0x60
#define __I2C_STAT_SR_DATA_ACK_OK       0x80
#define __I2C_STAT_SR_DATA_NO_ACK       0x88
#define __I2C_STAT_SR_START_STOP_REQ    0xA0

// Desc: Status codes used in Slave-Transmit (ST) Mode.
#define __I2C_STAT_ST_SLAR_ACK_OK       0xA8
#define __I2C_STAT_ST_DATA_ACK_OK       0xB8
#define __I2C_STAT_ST_DATA_NO_ACK       0xC0
#define __I2C_STAT_ST_LAST_DATA_ACK_OK  0xC8


// Desc: The following helper macro is used to extract the status code
//       from the TWSR register.
#define __I2C_STATUS( reg )     (( reg ) & 0xF8 )

// ========================= private prototypes ============================= //
// Desc: Do MCU-specific device initialization.
extern void __I2C_init( void );
// -------------------------------------------------------------------------- //
// Desc: Do MCU-specific device closure and resource release.
extern void __I2C_close( void );

#endif /* ____I2C324V221_H__ */
