/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private header file for PSXC module. It contains private declarations.

#ifndef ____PSXC324V221_H__
#define ____PSXC324V221_H__

// ============================ private defines ============================= //
#define MAX_PSXC_BYTES_TO_READ  9
#define PSXC_POLL_SEQ_BYTES     9
#define PSXC_RESP_BYTES         9

#define DIGITAL_MODE_BYTE  0x41 /* Byte indicater for 'analog-mode' stream  */
#define ANALOG_MODE_BYTE   0x73 /* Byte indicator for 'digital-mode' stream */
#define REPLY_BYTE         0x5A /* First byte reply from controller.        */

#define PSXC_TEST_INTERVAL  250 /* in milliseconds */

// ============================ private prototypes ========================== //
// Desc: Timer callback prototype used with the 'Timer Service' subsystem.
//       This routine starts a timing service that prints PSX controller data
//       on the LCD screen.  It is started via 'PSXC_run_test()' function, and
//       stopped with 'PSXC_stop_test()' function.
extern TMR_NR( PSXC_test_callback );

#endif /* ____PSXC324v221_H__ */
