/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Header for the CEENBoT (TM) module.

#ifndef __CBOT324V221_H__
#define __CBOT324V221_H__

// ============================ includes ==================================== //
#include<avr/io.h>
#include<avr/interrupt.h>

#include "capi324v221.h"

// ============================= prototypes ================================= //
// TODO
// Desc: This function is the user's version of 'main()'.  This is where all
//       of the user's code will reside in, and thus it MUST be defined by the
//       user.  The standard 'main()' function is used internally by the API
//       to initialize necessary subsystems.  Note that although this PROTOTYPE
//       is designated as 'private', it's definition is made publicly.
extern void CBOT_main( void );
// -------------------------------------------------------------------------- //
// TODO
// Desc: Initializes the CEENBoT's API.
extern void CBOT_init( void );
// -------------------------------------------------------------------------- //
// Desc: This function is needed here to force the linker to bring in the 
//       'CBOT_isr' object module to force undefined references to interrupt
//       service routines to be resolved.

extern void CBOT_ISR_init( void );

#endif /* __CBOT324V221_H__ */
