/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Header file for 'lcd' module.  It contains portions of the module/int-
//       erface that should not be exposed to the user (for public use).  These
//       are 'private' entitites.

#include "lcd324v221.h"

#ifndef ____LCD324V221_H__
#define ____LCD324V221_H__


// =========================== private prototypes =========================== //
// Input Args: 'pageNum' - The page number.  Allowed values are 0-3, with 3 
//                         corresponding to the top-most page, and 0 with the
//                         bottom-most page.  (This is the way it is imple-
//                         mented in the 324 board).
//             'colAddr' - Column address.  Must be between 0 and 
//                         'LCD_PIX_WIDTH - 1'.
// Output Args: None.
// Globals write: None.
// Globals read:  None.
// Returns: Nothing.
//
// Desc: Function sets the PaGe number and Column (PGC) address for all sub-
//       sequent display writes.  
extern void LCD_set_PGC_addr( unsigned char pageNum, unsigned char colAddr );
// -------------------------------------------------------------------------- //
// Input Args:
// Output Args:
// Globals Write: 'LCD_params'.
// Globals  Read: 'LCD_params'.
// Returns: 0 if output was successful, non-zero otherwise.
//
// Desc:  This function is a special 'handler function' internally called
//        by the standard output stream used in redirection of the output
//        stream.  It is eventually called indirectly when 'printf()' is
//        used.  It shold NOT be called by the user.  Users should use
//        'LCD_putchar' (no preceeding underscores) to achieve the same
//        task.
extern int __LCD_putchar( char c, FILE *stream );
// -------------------------------------------------------------------------- //
// Desc: Function does MCU-specific initialization.
extern void __LCD_init( void );
// -------------------------------------------------------------------------- //
// Desc: Function does MCU-specific resource release.
extern void __LCD_close( void );

#endif /* ____LCD324V221_H__ */
