/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private header file for the SPIFLASH subsystem module.

#ifndef ____SPIFLASH324V221_H__
#define ____SPIFLASH324V221_H__

// ========================= private defines ================================ //
// Desc: SPIFLASH Opcode Commands for the Atmel - AT25DF041A (see datasheet).
// 
// Read Commands
#define __SPIF_OPC_CMD_READ_ARRAY           0x0B
#define __SPIF_OPC_CMD_READ_ARRAY_LOW_FREQ  0x03

// Program Erase Commands
#define __SPIF_OPC_CMD_BLOCK_ERASE_4K       0x20
#define __SPIF_OPC_CMD_BLOCK_ERASE_32K      0x52
#define __SPIF_OPC_CMD_BLOCK_ERASE_64K      0xD8
#define __SPIF_OPC_CMD_CHIP_ERASE           0x60
#define __SPIF_OPC_CMD_BYTE_PAGE_PROGRAM    0x02
#define __SPIF_OPC_CMD_SEQUENTIAL_PROGRAM   0xAD

// Protection Commands
#define __SPIF_OPC_CMD_WRITE_ENABLE         0x06
#define __SPIF_OPC_CMD_WRITE_DISABLE        0x04
#define __SPIF_OPC_CMD_PROTECT_SECTOR       0x36
#define __SPIF_OPC_CMD_UNPROTECT_SECTOR     0x39
#define __SPIF_OPC_CMD_READ_SECTOR_PROTECTION_REGS  0x3C

// Status Register Commands
#define __SPIF_OPC_CMD_READ_STATUS_REG      0x05
#define __SPIF_OPC_CMD_WRITE_STATUS_REG     0x01

// Misc. Commands
#define __SPIF_OPC_CMD_READ_DEVICE_ID       0x9F
#define __SPIF_OPC_CMD_DEEP_POWER_DOWN      0xB9
#define __SPIF_OPC_CMD_RESUME               0xAB

// Global Protect/Unprotect Byte Patterns
#define __SPIF_GLOBAL_PROTECT_BYTE          0b00111100
#define __SPIF_GLOBAL_UNPROTECT_BYTE        0b00000000

// Desc: Chip parameters.
#define __SPIF_MAX_32K_BLOCKS               16
#define __SPIF_32K_BYTES                    ( 32UL * 1024UL )


// ======================= private prototypes =============================== //
// Input Args:  'start_addr' - The starting address to begin reading data from
//                             in the SPI Flash device.  This is limited to a
//                             19-bit address (512K total): 0x00000 to 0x7FFFF.
//
//              'pDest' - The **ADDRESS OF** the destination buffer where data
//                        will be written to, must be passed to this argument.
//
//              'size' - The number of consecutive bytes to read from the SPI
//                       Flash starting at 'start_addr'.  You can read a maximum
//                       of 64K bytes at a time.
//
// Desc: Function will read 'size' bytes starting at address specified by
//       'start_addr' and stash these 'size' bytes in the specified destination
//       buffer indicated by 'pDest'.  User **MUST** make sure that the data
//       destination buffer is large enough to accomodate 'size' bytes.  Failure
//       to observe this rule will result in 'buffer overrun', yielding
//       unpredictable behavior.
extern void SPIFLASH_read_bytes( unsigned long int start_addr, 
                                unsigned char *pDest, unsigned short int size );
// -------------------------------------------------------------------------- //
// Input Args: 'start_addr' - The starting address to begin writing data to in
//                            the SPI-FLASH device.  Note that this is limited
//                            to a 19-bit address (512K total): 0x00000 to 
//                            0x7FFFF.  It is recommended you always write on 
//                            a 256-byte boundary (i.e., clear the lower 
//                            8-bits).
//
//              'pSource' - The buffer containing the data to be written. You
//                          must pass **THE ADDRESS** of this buffer.
//
//              'size' - The number of bytes to take from the source buffer.
//                       The maximum value is limited to 256 bytes.
//                          
// Desc: Function will write 'size' bytes starting at address specified by
//       'start_addr' taken from 'pSource' buffer.  You can write up to 256
//       bytes which is the minimum write granularity allowed for the currently
//       installed SPI-FLASH device.  Although arbitrary addresses are possible
//       keep in mind the 'size' must be properly chosen so that it fits on a
//       given 256 boundary.
//
//       For example, you can't specify the start address of 128, and specify
//       that you wish to write 256 bytes.  The SPI-FLASH will NOT be written
//       from location 128 to 384.  Instead, it will write from 128 to 255, and
//       recycle the rest down to 0 (within the same 256-byte page).  In other
//       words, you can't write across 256-byte page BOUNDARIES.
//
//       The function does NOT return until it has written all bytes, so it
//       is safe to call the function back-to-back for writing larger chunks
//       of data 256-bytes at a time.
extern void SPIFLASH_write_bytes( unsigned long int start_addr,
                             unsigned char *pSource, unsigned short int size );
// -------------------------------------------------------------------------- //
// Input Args: 'sect_addr' - The address within the 256-byte sector that is to
//                           be unprotected.  Note that this is limited to a 
//                           19-bit address (512K total): 0x00000 to 0x7FFFF.
//
// Desc: Function is used to unprotect a sector that the user wishes to write
//       to.  Specify the address within the sector that is to be unprotected.
//       This address should preferrably be on a 256-byte boundary.
extern void SPIFLASH_unprotect_sector( unsigned long int sect_addr );
// -------------------------------------------------------------------------- //
// Input Args: 'sect_addr' - The address within the 256-byte sector that is to
//                           be unprotected.  Note that this is limited to a 
//                           19-bit address (512K total): 0x00000 to 0x7FFFF.
//
// Desc: Function is used to protect a sector that the user wishes to protect.
//       Specify the address within the sector that is to be protected.  This
//       address should preferrably be on a 256-byte boundary.
extern void SPIFLASH_protect_sector( unsigned long int sect_addr );
// -------------------------------------------------------------------------- //
// Desc: Function removes the 'sector protection' mechanism for the entire
//       SPI-FLASH. ***USE WITH CAUTION***
extern void SPIFLASH_global_unprotect( void );
// -------------------------------------------------------------------------- //
// Desc: Function enables the global 'sector protection' mechanism for the
//       entire SPI-FLASH.
extern void SPIFLASH_global_protect( void );
// -------------------------------------------------------------------------- //
// Desc: Function waits and blocks until SPI-FLASH is no longer busy.
extern void SPIFLASH_IsBusy( void );
// -------------------------------------------------------------------------- //
// Input Args: 'which_block' - The 'page number' of the 32K block to erase.  The
//                            value must be between 0 and 15, with 0 represen-
//                            ting the first 32K block in the SPI-FLASH, and
//                            15 representing the LAST 32K block in the SPI-
//                            FLASH.
//
// Desc: Function is used to erase a 32K block of memory in the SPI-FLASH. You
//       specify which 32K block to erase by passing an integer representing
//       the 32K block of memory to erase.
//
//       For example, if you wish to erase the 32K page starting at 0x8000
//       which corresponds to the second 32K, you specify:
//
//          SPIFLASH_block_erase_32K( 1 );
//
extern void SPIFLASH_block_erase_32K( unsigned short int which_block );
// -------------------------------------------------------------------------- //
// Desc: Function erases the SPI-FLASH completely.
extern void SPIFLASH_chip_erase( void );
// -------------------------------------------------------------------------- //
// Desc: Function is used to send address bytes while an SPI communication 
//       transaction is in effect with the SPIFLASH.  Various functions use this
//       function internally.  Note that the function assumes a transaction has
//       been properly initiated (by a previous function) and is presently
//       'active' or 'in effect'.
extern void SPIFLASH_send_addr( unsigned long int memory_addr );
// -------------------------------------------------------------------------- //
// Desc: Function enables the SPI-FLASH for a 'write/erase/program' transaction.
extern void SPIFLASH_write_enable( void );
// -------------------------------------------------------------------------- //
// Desc: Do any MCU-specific initialization.
extern void __SPIFLASH_init( void );
// -------------------------------------------------------------------------- //
// Desc: Do any MCU-specific release.
extern void __SPIFLASH_close( void );

#endif /* ____SPIFLASH324V221_H__ */


