/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Header file for the SPIFLASH subsystem module.  This module contains
//       functions for reading/writing to the on-board SPI FLASH device on the
//       v2.21 '324 platform.

#ifndef __SPIFLASH324V221_H__
#define __SPIFLASH324V221_H__

#include<avr/io.h>

#include "utils324v221.h"
#include "sys324v221.h"
#include "spi324v221.h"

// =============================== defines ================================== //
// ============================ type declarations =========================== //
// ============================== prototypes ================================ //
// Desc: Initializes and opens services for manipulating the on-board SPI 
//       flash.
extern SUBSYS_OPENSTAT SPIFLASH_open( void );
// -------------------------------------------------------------------------- //
// Desc: Function deallocates and releases SPIFLASH resources.
extern void SPIFLASH_close( void );
// ============================ external globals ============================ //

#endif /* __SPIFLASH324V221_H__ */
