/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Nicholas Wertzberger
// E-mail: wertnick@gmail.com
//
// Desc: Private header file for the TI subsystem module.

/* Programmer: Nicholas Wertzberger
 *      email: wertnick@gmail.com
 *
 */

#ifndef ____TI324V221_H__
#define ____TI324V221_H__

#include <inttypes.h>

#include "ti324v221.h"

/***************************************************************************
 * Transport Layer
 */

#define MAX_LIST_LEN 6
#define TI_MAX_PACKET_DATA  ( 2 + (( MAX_LIST_LEN ) * 10 )) 
#define TI82_CBR    0x12
#define TI85_CBR    0x05 /* 0x15 */
#define TI86_CBR    0x06 /* 0x16 */
#define TI89_CBR    0x19

// Desc: Custom structure type declaration for 'packets' which are used
//       in the communication sequence bewteen the calculator and CEEBoT.
typedef struct {

    uint8_t  machine_id;
    uint8_t  command;
    uint16_t len;
    uint8_t  data[ TI_MAX_PACKET_DATA ];

} packet_t;

BOOL ti_process_packet(packet_t *packet);

void ti_transport_abort( void );

void ti_mkvar(packet_t *pk, uint8_t *name, uint8_t len, int16_t var);
void ti_mkdata(packet_t *pk, int16_t s_rval);
uint8_t ti_getlist(packet_t *pk, int16_t *list, uint8_t len);

void ti_legacy_mkvar(packet_t *pk, uint8_t *name, uint8_t len, 
                     uint8_t ss_machine_id );
void ti_legacy_mkdata(packet_t *pk, int16_t s_rval, uint8_t ss_machine_id );
uint8_t ti_legacy_getlist(packet_t *pk, int16_t *list, uint8_t len, 
                          uint8_t ss_machine_id );

/***************************************************************************
 * Data Link Layer
 */

/* Command ID's */
#define TI_VAR     0x06     /* Variable Header              */
#define TI_CTS     0x09     /* Clear to Send                */
#define TI_DATA    0x15     /* Data Packet                  */
#define TI_ACK     0x56     /* Acknowledge Packet           */
#define TI_ERR     0x5a     /* Error Packet                 */
#define TI_SKIP    0x36     /* Skip Packet                  */
#define TI_EOT     0x92     /* End of Transmission Packet   */
#define TI_RTS     0xc9     /* Request to send              */
#define TI_REQ     0xa2     /* Variable Request.            */
#define TI_RDY     0x68

/* not used, but cool! */
#define TI_SCR     0x6d     /* Screen Capture request       */

/* declared, but shoudl be used sparingly for readability. pass as pointer */
extern packet_t g_active_packet;

/* Process the received character.
 */
BOOL ti_process_char(uint8_t ch);

/* input:   the command ID, and packet data.
 * output:  none
 * notes:   none
 */
void ti_send_packet(const packet_t *data);

/* Aborts the current transfer
 */
void ti_datalink_abort(void);

/***************************************************************************
 * Physical Layer
 */

/* inputs:  the character to send
 * outputs: the character if successful, otherwise error code.
 * notes:   The TI protocol says these must be sent LSB first.
 */
void ti_putc(uint8_t ch);

#endif /* ____TI324V221_H__ */

