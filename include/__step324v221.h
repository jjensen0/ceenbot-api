/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private header for the stepper module.

#ifndef ____STEP324V221_H__
#define ____STEP324V221_H__

// =========================== private defines ============================== //
#define __DDS_DIVISOR     1000    /* DDS divisor constant */
#define __STEPPER_PORT    PORTC
#define __MAX_STEP_SPEED  400     /* Maximum stepper speed allowed. */
                                  // Anything higher is simply not reliable
                                  // as it causes the stepper to either 
                                  // rattle or miss/skip steps.

#define __MAX_ACCEL_RATE  1000    /* Maximum acceleration/deceleration rate */
#define __PWM_PROP_RATE   96      /* PWM proportion rate. */

// =========================== private declarations ========================= //
// =========================== private prototypes =========================== //
// Desc: Do MCU-specific initialization.
extern void __STEPPER_init( void );
// -------------------------------------------------------------------------- //
// Desc: Do MCU-specific deallocation and release.
extern void __STEPPER_close( void );
// -------------------------------------------------------------------------- //
// Input  Args: 'process_left' - A non-zero value indicates to this function
//                               that a DDS trigger (timing) event has occurred
//                               for the left motor.
//              'process_right' - A non-zero value indicates to this function
//                                that a DDS trigger (timing) event has occurred
//                                for the right motor.
//                                
// Output Args: None.
// Globals  Read:
// Globals Write:
// Returns: TODO
//
// Desc:  This function is invoked internally by 'STEPPER_clk()'.  It's a 
//        private function.
extern void STEPPER_process_run_mode( unsigned char process_left, 
                                      unsigned char process_right );
// -------------------------------------------------------------------------- //
// TODO
// Desc: This function is invoked internally by 'STEPPER_process_run_mode()'
//       when further processing is needed because a stepper is running in 
//       STEP mode.
extern void STEPPER_process_step_mode( STEPPER_ID which );

// -------------------------------------------------------------------------- //
// TODO
// Desc: This function is internally invoked by 'STEPPER_clk()' to process
//       acceleration parameters.
extern void STEPPER_process_accel( unsigned char process_left,
                                   unsigned char process_right );
// -------------------------------------------------------------------------- //
// TODO
// Desc: This function is internally invoked.  It performs pulse-width modu-
//       lation of the stepper signals in order to achieve power reduction.
extern void STEPPER_process_pwm( unsigned char *pPattern, 
                                 unsigned char process_left,
                                 unsigned char process_right );
// -------------------------------------------------------------------------- //
// TODO
// Desc: This function computes the step value at which deceleration should
//       occur when operating in STEP mode.
extern void STEPPER_set_decel_step_values( STEPPER_ID which,
                                           unsigned short int nSteps,
                                           unsigned short int nStepsPerSec );
// -------------------------------------------------------------------------- //
// TODO
// Desc: THis function performs the process of initiating the computation
//       of step values at which deceleration should begin depending on the
//       motor being engaged.  It accomplishes this via calls to 
//       'STEPPER_set_decel_step_values()'.
extern void STEPPER_setup_decels( STEPPER_ID which,
                                  unsigned short int nSteps,
                                  unsigned short int nStepsPerSec );


#endif /* ____STEP324V221_H__ */
