/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private header file for the 'led' module.

#ifndef ____LED324V221_H__
#define ____LED324V221_H__

// ========================= private defines ================================ //
// LED bit patterns.
#define LED_RED_BIT 0x20
#define LED_GRN_BIT 0x40

#define LED_PORT    PORTD

// Desc: The 'LED_state()' macro is used to turn the onboard LED's ON or OFF.
//       Currently, there are only two user-controllable LED's if they're not
//       in use by other subsystems.
//       
//       Example:
//       -------
//
//       LED_state( LED_Red, LED_ON );    // Turns Red LED ON.
//       LED_state( LED_Green, LED_OFF ); // Turns Green LED OFF.
//
#define __LED_state( which, state )  SCBV( state, which, LED_PORT )

// Desc: The 'LED_toggle()' macro can be used to 'toggle' the state of the
//       specified LED from one state to the other and vice versa.
//
//       Example:
//       -------
//
//       LED_toggle( LED_Green );   // Toggle Green LED state.
//
#define __LED_toggle( which ) TBV( which, LED_PORT )


// ========================= private prototypes ============================= //
// Desc:  This is an MCU-specific implementation to set up the hardware for
//        LED access.
extern void __LED_init( void );
// -------------------------------------------------------------------------- //
// Desc:  This is an MCU-specific implementation to release any hardware in
//        use by the LED subsystem.
extern void __LED_close( void );
// -------------------------------------------------------------------------- //
// Desc:  This is an MCU-specific implementation to set the desired LEDs.
extern void __LED_set_pattern( unsigned char LED_pattern );
// -------------------------------------------------------------------------- //
// Desc:  This is the MCU-specific implementation to clear the desired LEDs.
extern void __LED_clr_pattern( unsigned char LED_pattern );
// -------------------------------------------------------------------------- //
// Desc:  This is the MCU-specific implementation to toggle the desired LEDs.
extern void __LED_tog_pattern( unsigned char LED_pattern );

#endif /* ____LED324V221_H__ */
