/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private header file for the SPKR (speaker) subsystem module.

#ifndef ___SPKR324V221_H__
#define ___SPKR324V221_H__

#include "spkr324v221.h"

// ========================== private defines =============================== //
#define __SPKR_OUT_PIN      7       /* Pin 7 on Port... */
#define __SPKR_OUT_PORT     PORTD   /* PORTD!           */
#define __SPKR_DIR_PORT     D       /* Direction Port.  */

#define __SPKR_DDS_DIVIDER_VAL  63000
#define __BEEP_DDS_DIVIDER_VAL  1000

#define __MIN_OCTAVE    0   /* Lowest Octave    */
#define __MAX_OCTAVE    5   /* Highest Octave   */
#define __REF_OCTAVE    3   /* Reference Octave */
#define __MAX_NOTE      11  /* Maximum chromatic note (starting from 0) */

// ========================== private prototypes ============================ //
// Desc: Do MCU-specific resource acquisition.
extern void __SPKR_init( SPKR_MODE spkr_mode );
// -------------------------------------------------------------------------- //
// Desc: Do MCU-specific resource release.
extern void __SPKR_close( SPKR_MODE spkr_mode );
// -------------------------------------------------------------------------- //
// Desc: Let the timer generate interrupts.
extern void __SPKR_start( void );
// -------------------------------------------------------------------------- //
// Desc: Stop the timer from generating interrupts.
extern void __SPKR_stop( void );
// -------------------------------------------------------------------------- //
// Desc: Function takes care of the 'timing' aspects needed to generate tones
//       using fundamental direct digital synthesis techniques.
extern void SPKR_clk( void );


// ====================== CBOT-ISRs prototypes ============================== //
extern CBOT_ISR( __SPKR_TIMER1_COMPA_vect );

#endif /* ___SPKR324V221_H__ */
