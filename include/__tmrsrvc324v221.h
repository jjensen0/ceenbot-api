/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
*/
// Auth: Jose Santos
// Desc: Private header file for the timer service module.

#ifndef ____TMRSRVC324V221_H__
#define ____TMRSRVC324V221_H__

// ========================= private declarations =========================== //
// TODO: The 'system' variable 'CBOT_system.TMRSRVC_subsys' should be used
//       in place of the 'TMR_RDY_STAT' paramater to accomplish the task of
//       this variable (system availability indicator).
//
// Private enumerated type to hold the status of the timer service subsystem.
typedef enum TMR_RDY_STATUS_TYPE { TMR_NOT_RDY = 0, TMR_RDY } TMR_RDY_STAT;

// Private enumerated type to hold insertion status (i.e., the timing service
// is busy modifying the list of timer objects).
typedef enum TMR_BSY_STATUS_TYPE { TMR_NOT_BSY = 0, TMR_BSY } TMR_BSY_STAT;

// Private enumerated type to hold 'run' status of the timer service.
typedef enum TMR_RUN_STATUS_TYPE { TMR_STOPPED = 0, TMR_RUNNING } TMR_RUN_STAT;

// Private enumerated type to denote whether there's another time object
// with 0 terminal count that needs to be processed.
typedef enum TMRPROC_RESULT_TYPE 
                        { TMRPROC_DONE, TMRPROC_NOT_DONE } TMRPROC_RESULT;


// Structure type declaration for encapsulating the timing object in
// a linked-list.

typedef struct TMRNODE_TYPE
{

    TIMEROBJ             *pTimerObj;    // Pointer to 'timer object'.
    struct TMRNODE_TYPE  *pNextNode;    // Pointer to NEXT node.

} TMRNODE;

// Structure for holding the linked list of timer object nodes.
typedef struct TMRNODE_LIST_TYPE
{

    unsigned char nNodes;   // Contains number of 'nodes' in the list.  In 
                            // this case, the number of timer objects spec-
                            // ficially.

    TMRNODE *pNodeList;   // Points to the linked-list of timer objects.

} TMRNODE_LIST;
// ========================= private prototypes ============================= //
// Input  Args: 'pNode' - A populated timer node to insert in the linked list.
// Output Args: None.
// Globals  Read: 'tmrNodes' - This is the linked list of timer objects.
// Globals Write: 'tmrNodes' - This is the linked list of timer objects.
// Returns: Nothing.
//
// Desc: Performs insertion of the timer node into the timer service internal
//       linked list of timer objects.  This is an internal function invoked
//       by 'TMRSRVC_new()'.
extern void TMRSRVC_insert( TMRNODE *pNode );

// -------------------------------------------------------------------------- //
// Input  Args: None.
// Output Args: None.
// Globals  Read:
// Globals Write:
// Returns: TODO
//
// Desc:  Internal function that does the processing of timer events.
extern TMRPROC_RESULT TMRSRVC_process( void );
// -------------------------------------------------------------------------- //
// Desc: Function used to destroy all nodes and release memory resources back
//       to the heap.
extern void TMRSRVC_clear_list( void );
// -------------------------------------------------------------------------- //
// Input  Args: None.
// Output Args: None.
// Globals  Read: None.
// Globals Write: None.
// Returns: Nothing.
//
// Desc:  This is an internal function where the MCU-specific initialization
//        sequence (e.g., ports, registers, etc) are set up accordingly.  It 
//        is called internally during initialization by 'TMRSRVC_init()' func-
//        tion.
extern void __TMRSRVC_init( void );
// -------------------------------------------------------------------------- //
// Input  Args: None.
// Output Args: None.
// Globals  Read: None.
// Globals Write: None.
// Returns: Nothing.
//
// Desc:  This is an internal function and is MCU-specific.  It starts up the
//        hardware timer of the MCU, which drives the 'TMRSRVC_tick()' function.
//        It is called internally during initialization by 'TMRSRVC_init()'.
extern void __TMRSRVC_start( void );
// -------------------------------------------------------------------------- //
// Input  Args: None.
// Output Args: None.
// Globals  Read: None.
// Globals Write: None.
// Returns: Nothing.
//
// Desc:  This is an internal function and is MCU-specific.  It stops the 
//        hardware timer of the MCU, which drives the 'TMRSRVC_tick()' function.
//        It is called internally by the timer service subsystem.  However,
//        this function is typically never invoked.  It is invoked by the user
//        function 'TMRSRVC_close()' function.
// TODO: Not implemented.
// extern void __TMRSRVC_stop( void );
// -------------------------------------------------------------------------- //
// Desc:  This is an internal function and is MCU-specific.  It will stop the
//        timer in software and hardware, release any memory resources and
//        MCU hardware resources as well.  It is invoked internally when the
//        user calls 'TMRSRVC_close()' function.
// TODO: Not implemented.
// extern void __TMRSRVC_close( void );

// ========================= CBOT-ISRs prototypes =========================== //
extern CBOT_ISR( __TIMER0_COMPA_vect );

// ========================= external declarations ========================== //
extern volatile TMR_BSY_STAT timer_busy_status;
extern volatile TMR_RUN_STAT timer_run_status;
extern volatile TMRNODE_LIST tmrNodes;

#endif /* ____TMRSRVC324V221_H__ */
