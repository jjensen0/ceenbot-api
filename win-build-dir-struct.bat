@echo on
echo This is a script to create the directory
echo structure needed for building the API.
echo You only have to do this once!
@echo off

mkdir objects\modules\adc
mkdir objects\modules\capi
mkdir objects\modules\cbot
mkdir objects\modules\csnsr
mkdir objects\modules\gpi
mkdir objects\modules\i2c
mkdir objects\modules\isr
mkdir objects\modules\lcd
mkdir objects\modules\led
mkdir objects\modules\mega
mkdir objects\modules\psxc
mkdir objects\modules\pwrmgr
mkdir objects\modules\spi
mkdir objects\modules\spkr
mkdir objects\modules\step
mkdir objects\modules\swatch
mkdir objects\modules\system
mkdir objects\modules\task
mkdir objects\modules\ticalc
mkdir objects\modules\tiny
mkdir objects\modules\tmrsrvc
mkdir objects\modules\uart
mkdir objects\modules\usonic
mkdir objects\modules\utils
mkdir objects\modules\pwm
mkdir objects\modules\spiflash
mkdir objects\modules\bat
mkdir output
mkdir lib\lib-includes
mkdir objects\main
mkdir source\main