:# SRCDIR -- Root directory where sources are contained.
# HDRDIR -- Directory where headers are contained.
# OBJDIR -- Root directory to place the 'objects'.  The directory structure for
#			the objects MUST match the directory structure under 'SRCDIR'.
# LIBDIR -- Directory to place static library files upon generation.
# LIBINCDIR -- Directory to place header files associated with static library.
# OUTDIR -- Directory to place the output files.
# RELDIR -- 'Release' directory. (TODO)
# DBGDIR -- 'Debug' directory. (TODO)
# MSCDIR -- Directory to place miscellaneous files.
# PRJNAME -- Name of the project.  This will determine the name of the output.
# LIBNAME -- Name to give the library when generated.  If not given, the
#            'PRJNAME' will be assigned -- otherwise 'noname' will be given.
# BLDTYPE -- The 'Build Type'  Must be 'DEBUG' or 'RELEASE'. [RELEASE].
# DBGSFIX -- The 'Debug Suffix'.  It will appended to the intermediate and 
#			 output files.
# OUTSFIX - The 'Output Suffix'.  It will be appended to the project name once
#			given by 'PRJNAME' once generated.
# SRCEXT -- Extension used for source files. (e.g., '.c').
# ASMEXT -- Extension used for assembly files. (e.g., '.S').
# HDREXT -- Extension used for header files. (e.g., '.h').
# OBJEXT -- Extension used for object files. (e.g., '.o').
# LIBEXT -- Extension used for static libraries. (e.g., '.a').

# Options:
# WITHPRINTF_FLT    -- Enable linking with floating point 'printf' variants.

# ============================= VARIABLES =====================================
# Set the include directory.
AVR_INC := /home/joscci/LinuxAVR/avr/include

# Set the library directory.
AVR_LIB := /home/joscci/LinuxAVR/avr/lib

# Set the compiler.
AVR_CC := avr-gcc

# Set the needed binary tools.
AVR_OBJDUMP = avr-objdump
AVR_OBJCOPY = avr-objcopy
AVR_AR      = avr-ar
AVR_DUDE    = avrdude

# Set the MCU type.
MMCU   := -mmcu=$(MCU)

# Set archiver options.
AR_OPTS := -rcs

# Set needed compiler flags regardless.
CC_WFLAGS := -Wstrict-prototypes -Wmissing-prototypes -Werror

# Set needed compiler options regardless.
CC_FFLAGS := -funsigned-char -funsigned-bitfields -fpack-struct
CC_FFLAGS += -fshort-enums -fno-exceptions -std=gnu99

# Set the linker flags regardless.
LD_FLAGS  = -Wl,-Map



# ========================== PRELIMINARY ======================================
# We must first process all the parameters to ensure they have valid values.
# If they don't then we assign default values.


# First check if 'BLDTYPE' is empty so we can assign a default value.
ifeq ($(BLDTYPE),)
	
    BLDTYPE=RELEASE	

endif

# Check if 'SRCEXT' is empty so we can assign a default value.
ifeq ($(SRCEXT),)

    SRCEXT=.c

endif

# Check if 'ASMEXT' is empty so we can assign a default value.
ifeq ($(ASMEXT),)

    ASMEXT=.S

endif

# Check if 'HDREXT' is empty so we can assign a default value.
ifeq ($(HDREXT),)

    HDREXT=.h

endif

# Check if 'OBJEXT' is empty so we can assign a default value.
ifeq ($(OBJEXT),)

    OBJEXT=.o

endif

# Check if 'LIBEXT' is empty so we can assign a default value.
ifeq ($(LIBEXT),)

    LIBEXT=.a

endif

# Check if 'SRCDIR' is empty so we can assign a default value.
ifeq ($(SRCDIR),)

    SRCDIR=./

endif

# Check if 'HDRDIR' is empty so we can assign a default value.
ifeq ($(HDRDIR),)

	#Default to 'source directory'.
    HDRDIR:=$(SRCDIR)

endif

# Check if 'OBJDIR' is empty so we can assign a default value.
ifeq ($(OBJDIR),)

    OBJDIR=./

endif

# Check if 'LIBDIR' is empty so we can assign a default value.
ifeq ($(LIBDIR),)

    LIBDIR=./

endif

# Check if 'LIBINCDIR' is empty so we can assign a default value.
ifeq ($(LIBINCDIR),)

     ifeq ($(LIBDIR),)

	 	# Otherwise you'll have to put it in a default place.
		LIBINCDIR:=./

     else

	 	# If 'LIBDIR' is specified then put the files there also.
        LIBINCDIR:=$(LIBDIR)

     endif

endif

# Check if 'OUTDIR' is empty so we can assign a default value.
ifeq ($(OUTDIR),)

    OUTDIR=./

endif

# Check if 'MSCDIR' is empty so we can assign a default value.
ifeq ($(MSCDIR),)

    MSCDIR=./

endif

# Check if 'PRJNAME' is empty so we can assign a default value.
ifeq ($(PRJNAME),)

    PRJNAME=no_name

endif

# Check if 'LIBNAME'is empty so we can assign a default value.
ifeq ($(LIBNAME),)

    ifeq ($(PRJNAME),)
        
        LIB_NAME:=no_name$(LIBEXT)

    else

        LIB_NAME:=$(PRJNAME)$(LIBEXT)
    endif

else

	LIB_NAME:=$(LIBNAME)$(LIBEXT)

endif

# Check if 'DBGSFIX' is empty so we can assign a default value.
ifeq ($(DBGSFX),)

    DBGSFX=.debug

endif

# Check if 'RELDIR' is empty so we can assign a default value.
ifeq ($(RELDIR),)

    RELDIR=./

endif

# Check if 'DBGDIR' is empty so we can assign a default value.
ifeq ($(DBGDIR),)

    DBGDIR=./

endif

# =========================== DEFINED PARAMETERS PROCESS ======================
ifeq ($(WITHPRINTF_FLT),TRUE)

    LD_FLAGS   := -Wl,-u,vfprintf $(LD_FLAGS)
    PRINTF_LIB := -lprintf_flt

endif

    
# =========================== CLEAN PARAMETERS ================================
# Get rid of all trailing and following white spaces.
BLDTYPE := $(strip $(BLDTYPE))
SRCEXT  := $(strip $(SRCEXT))
ASMEXT  := $(strip $(ASMEXT))
HDREXT  := $(strip $(HDREXT))
OBJEXT  := $(strip $(OBJEXT))
LIBEXT  := $(strip $(LIBEXT))
SRCDIR  := $(strip $(SRCDIR))
HDRDIR  := $(strip $(HDRDIR))
OBJDIR  := $(strip $(OBJDIR))
LIBDIR  := $(strip $(LIBDIR))
OUTDIR  := $(strip $(OUTDIR))
MSCDIR  := $(strip $(MSCDIR))
PRJNAME := $(strip $(PRJNAME))
DBGSFX  := $(strip $(DBGSFX))
OUTSFX  := $(strip $(OUTSFX))
DBGDIR	:= $(strip $(DBGDIR))
RELDIR	:= $(strip $(RELDIR))

LD_FLAGS   := $(strip $(LD_FLAGS))
#PRINTF_LIB := $(strip $(PRINTF_LIB ))

# TODO: Clean up trailing slashes.  If there is none, it should be appended;
#       if there is more than one, the superfluous ones should be removed; 
#       if it already has one -- leave it aloneear
#SRCDIR  :=
#OBJDIR  :=
#MSCDIR  :=
#OUTDIR  :=




# =========================== PARAM PROCESS ===================================
# If 'RELEASE'...
ifeq ($(BLDTYPE),RELEASE)

    CC_OPTS  = -Os
    DBG_OPTS =
    PRJ_NAME := $(PRJNAME)


# If 'DEBUG'...
else ifeq ($(BLDTYPE),DEBUG)

    CC_OPTS  = -O0
    DBG_OPTS = -gdwarf-2 -D__NO_DELAYS -D__DEBUG -D__NO_LCD_PRINTF -D__NO_UART_PRINTF
    PRJ_NAME := $(PRJNAME)$(DBGSFX)

endif

# Continue constructing the command line.
CC_FLAGS := $(CC_OPTS) $(DBG_OPTS) $(CC_WFLAGS) $(CC_FFLAGS) 

# ========================== GENERATE FILE NAMES ==============================

# Set the 'hex' target name.
FLASH      = $(PRJ_NAME).flash.hex

# Set the 'elf' target name.
ELF        = $(PRJ_NAME).elf

# Set the 'EEPROM' target name.
EEPROM     = $(PRJ_NAME).eep.hex

# Set the .MAP' target name.
MAP        = $(PRJ_NAME).map

# Set the list target name.
LIST       = $(PRJ_NAME).lst

# Collect all subdirectories where sources are located from the source root.
# This is the FIRST PASS.
SRC_DIRS    := $(foreach directory, $(wildcard $(SRCDIR)*), $(dir $(wildcard $(directory)/*/)))

# Generate the source files along with path info.
SRCS       := $(foreach directory, $(SRC_DIRS), $(notdir $(wildcard $(directory)/*$(SRCEXT))))

# Generate the assembly source files along with path info.
A_SRCS     := $(foreach directory, $(SRC_DIRS), $(notdir $(wildcard $(directory)/*$(ASMEXT))))

# Generate the object files.
OBJS       := $(SRCS:%$(SRCEXT)=%$(OBJEXT))

# Generate the object files from assembly.
A_OBJS     := $(A_SRCS:%$(ASMEXT)=%$(OBJEXT))

# Generate teh header files.
HFILES     := $(notdir $(wildcard $(HDRDIR)*$(HDREXT)))

# Construct files list along with PATH directories.
A_SRCS_WP  := $(foreach directory, $(SRC_DIRS), $(wildcard $(directory)*$(ASMEXT)))
SRCS_WP    := $(foreach directory, $(SRC_DIRS), $(wildcard $(directory)*$(SRCEXT)))
OBJS_WP    := $(foreach source_wp, $(SRCS_WP), $(patsubst $(SRCDIR)%$(SRCEXT),$(OBJDIR)%$(OBJEXT),$(source_wp)))
A_OBJS_WP  := $(foreach a_source_wp, $(A_SRCS_WP), $(patsubst $(SRCDIR)%$(ASMEXT),$(OBJDIR)%$(OBJEXT),$(a_source_wp)))
HFILES_WP  := $(foreach header,     $(HFILES), $(HDRDIR)$(header) )

FLASH_WP   := $(OUTDIR)$(FLASH)
ELF_WP	   := $(OUTDIR)$(ELF)
EEPROM_WP  := $(OUTDIR)$(EEPROM)
MAP_WP     := $(OUTDIR)$(MAP)
LIST_WP    := $(OUTDIR)$(LIST)
LIB_WP	   := $(LIBDIR)$(LIB_NAME)

# ============================ RULES ==========================================


# Declare Phonies
.PHONY: default, compile, link, create-hex-eep, build-all, clean, \
		upload_flash, verify_flash, test, create-lib

default:
	@printf "ERROR: Make target NOT specified!\n"
	@printf "Make targets:\n\n"
	@printf "\tcompile - Requires PRJNAME, BLDTYPE, OBJDIR, SRCDIR, OUTDIR, F_CPU, MCU\n"
	@printf "\tlink - Requires PRJNAME, BLDTYPE, SRCDIR, OBJDIR, OUTDIR, F_CPU, MCU\n"
	@printf "\tcreate-hex - Requires PRJNAME, BLDTYPE, OUTDIR\n"
	@printf "\tbuild-all - Requires PRJNAME, BLDTYPE, OBJDIR, SRCDIR, OUTDIR, F_CPU, MCU\n"
	@printf "\tclean - Requires SRCDIR, OBJDIR\n"
	@printf "\tupload_flash - Requires PRJNAME, OUTDIR, FMCU\n"
	@printf "\tverify_flash - Requires PRJNAME, OUTDIR, FMCU\n"

# Rule to link and generate .ELF file from the object files.
$(ELF_WP): $(OBJS_WP)
	$(check_prerequisites)
	@printf "Linking AVR C: Generating [ $(ELF) ] from:\n\n\t $(OBJS)\n\n"
	$(AVR_CC) $(CC_FLAGS) $(MMCU) $(LD_FLAGS),$(MAP_WP) \
	-o $@ $(OBJS_WP) $(A_OBJS_WP) $(PRINTF_LIB) -lm
	@printf "\nDone!\n\n"
	$(create_listing)

# Rule to generate object files (from '*$(SRCEXT)').
$(OBJS_WP): $(SRCS_WP) $(HFILES_WP)
	$(check_prerequisites)
	@printf "\nCompiling AVR C: [ $(addsuffix $(SRCEXT),$(basename $(notdir $@ ))) ==> $(notdir $@) ]\n"
	@printf "Target MCU = $(MCU), BUILD-TYPE = $(BLDTYPE)\n"
	$(AVR_CC) -c $(CC_FLAGS) -I$(HDRDIR) $(MMCU) -DF_CPU=$(F_CPU) \
	$(patsubst $(OBJDIR)%$(OBJEXT),$(SRCDIR)%$(SRCEXT),$@) \
	-o $@
	@printf "\nDone!\n\n"

# Rule to generate object files (from '*$(ASMEXT)').
$(A_OBJS_WP): $(A_SRCS_WP) $(HFILES_WP)
	$(check_prerequisites)
	@printf "\nCompiling AVR C: [ $(addsuffix $(ASMEXT),$(basename $(notdir $@ ))) ==> $(notdir $@) ]\n"
	@printf "Target MCU = $(MCU), BUILD-TYPE = $(BLDTYPE)\n"
	$(AVR_CC) -c $(CC_FLAGS) -I$(HDRDIR) $(MMCU) -DF_CPU=$(F_CPU) \
	$(patsubst $(OBJDIR)%$(OBJEXT),$(SRCDIR)%$(ASMEXT),$@) \
	-o $@
	@printf "\nDone!\n\n"

# Rule to generate the .hex and .eep files if any.
$(FLASH_WP) $(EEPROM_WP) : $(ELF_WP)
	@printf "\nCreating FLASH Hex file: [ $(FLASH) ]\n"
	$(AVR_OBJCOPY) -j .text -j .data -O ihex \
		$(ELF_WP) $(FLASH_WP)
	@printf "\nDone!\n"
	@printf "\nCreating EEPROM Hex file: [ $(EEPROM) ]\n"
	$(AVR_OBJCOPY) -j .eeprom --change-section-lma \
		.eeprom=0 -O ihex $(ELF_WP) $(EEPROM_WP)
	@printf "\nDone!\n"

# Rule to generate the static library from the objects.
$(LIB_WP): $(OBJS_WP) $(A_OBJS_WP)
	$(check_ar_prereq)
	@printf "\nCreating library...\n"
	$(AVR_AR) $(AR_OPTS) $(LIB_WP) $(OBJS_WP) $(A_OBJS_WP)
	@printf "\nDone!\n"
	@printf "\nCopying related headers...\n"
	@cp $(HDRDIR)*$(HDREXT) $(LIBINCDIR)
	@rm -f $(LIBINCDIR)__*$(HDREXT)
	@printf "\nDone!\n"

# Rule to 'compile' only.
compile: $(OBJS_WP)

# Rule to 'assemble' only.
assemble: $(A_OBJS_WP)

# Rule to 'link' only.
link: $(ELF_WP)

# Rule to 'buld' the entire project.
build-all: compile assemble link

# Rule to create the 'hex'files.
create-hex: $(ELF_WP) $(EEPROM_WP)

# Rule to generate the static library.
create-lib: $(LIB_WP)


# Rule to clean up.
clean:
	@printf "\nDeleting objects...\n"
	@rm -rf $(OBJS_WP) $(A_OBJS_WP)
	@printf "\nDeleting libs...\n"
	@rm -rf $(LIB_WP)
	@printf "\nDone!\n"

# Rule to flash hex files via 'avrdude'.
upload_flash:
	$(check_avrdude_prereq)
	@printf "Programming flash with [ $(FLASH) ]...\n"
	$(AVR_DUDE) -p $(FMCU) -c avrispmkII -P usb -e -U flash:w:$(FLASH_WP)
	@printf "\nDone!\n"

verify_flash:
	$(check_avrdude_prereq)
	@printf "Verifying flash with [ $(FLASH) ]...\n"
	$(AVR_DUDE) -p $(FMCU) -c avrispmkII -P usb -e -U flash:v:$(FLASH_WP)
	@printf "\nDone!\n"

avrdude_terminal:
	$(AVR_DUDE) -p $(FMCU) -c avrispmkII -P usb -t

# Default rule.
test:
	@printf "Main Default values:\n\n"
	@printf "\tBLDTYPE = $(BLDTYPE)\n"
	@printf "\tDBGSFX = $(DBGSFX)\n"
	@printf "\tOUTSFX = $(OUTSFX)\n"
	@printf "\tSRCEXT = $(SRCEXT)\n"
	@printf "\tASMEXT = $(ASMEXT)\n"
	@printf "\tHDREXT = $(HDREXT)\n"
	@printf "\tOBJEXT = $(OBJEXT)\n"
	@printf "\tLIBEXT = $(LIBEXT)\n"
	@printf "\tPRJ_NAME (final) = $(PRJ_NAME)\n"
	@printf "\tLIB_NAME (final) = $(LIB_NAME)\n"
	@printf "\n"
	@printf "Directories:\n\n"
	@printf "\tSRCDIR = $(SRCDIR)\n"
	@printf "\tHDRDIR = $(HDRDIR)\n"
	@printf "\tOBJDIR = $(OBJDIR)\n"
	@printf "\tMSCDIR = $(MSCDIR)\n"
	@printf "\tDBGDIR = $(DBGDIR)\n"
	@printf "\tRELDIR = $(RELDIR)\n"
	@printf "\tLIBDIR = $(LIBDIR)\n"
	@printf "\tLIBINCDIR = $(LIBINCDIR)\n"
	@printf "\tSRC_DIRS = $(SRC_DIRS)\n"
	@printf "\n"
	@printf "Compiler/Linker Flags:\n\n"
	@printf "\tCC_FLAGS = $(CC_FLAGS)\n"
	@printf "\tLD_FLAGS = $(LD_FLAGS)\n"
	@printf "\n"
	@printf "Output files:\n\n"
	@printf "\tFLASH = $(FLASH)\n"
	@printf "\tELF = $(ELF)\n"
	@printf "\tEEPROM = $(EEPROM)\n"
	@printf "\tMAP = $(MAP)\n"
	@printf "\tLIST = $(LIST)\n"
	@printf "\n"
	@printf "Sources:\n\n"
	@printf "\tSRCS = $(SRCS)\n"
	@printf "\tA_SRCS = $(A_SRCS)\n"
	@printf "\n"
	@printf "Corresponding Objects:\n\n"
	@printf "\tOBJS = $(OBJS)\n"
	@printf "\tA_OBJS = $(A_OBJS)\n"
	@printf "\n"
	@printf "Project Header files:\n\n"
	@printf "\tHFILES = $(HFILES)\n"
	@printf "\n"
	@printf "All files with Path info:\n\n"
	@printf "\tSRCS_WP = $(SRCS_WP)\n"
	@printf "\tA_SRCS_WP = $(A_SRCS_WP)\n"
	@printf "\tOBJS_WP = $(OBJS_WP)\n"
	@printf "\tA_OBJS_WP = $(A_OBJS_WP)\n"
	@printf "\tHFILES_WP = $(HFILES_WP)\n"
	@printf "\n"
	@printf "Variables that indicate static libraries:\n"
	@printf "\tPRINTF_LIB = $(PRINTF_LIB)\n"

# ============================ helper macros ==================================
# Command function to do some important checking.
define check_prerequisites

	$(if $(MCU),,$(error ERROR: MCU not defined! (e.g., MCU=atmega32)))
	$(if $(F_CPU),,$(error ERROR: F_CPU not defined! (e.g., F_CPU=20000000L)))

endef

# Command function do important checking when dealing with 'avrdude'.
define check_avrdude_prereq

	$(if $(FMCU),,$(error ERROR: FMCU not defined! (e.g., FMCU=m324p))) 

endef

# Command function to check prerequisites when creating a static library with
# the archiver utility.
define check_ar_prereq

    $(if $(LIBDIR),,$(error ERROR: LIBDIR not defined! (e.g., LIBDIR=lib/)))

endef

# Command function to generate the file listing.
define create_listing
@printf "\nGenerating $(LIST) file...\n"
@$(AVR_OBJDUMP) -h -S $(ELF_WP) > $(LIST_WP)
@printf "\nDone!\n\n"
endef
